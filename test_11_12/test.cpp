#define _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
#include "test1.h"
#include "test2.h"


using namespace std;

//int add(int a, int b)
//{
//	return a + b;
//}

//int add(int a = 10, int b = 10)
//{
//	return a + b;
//}

//double add(double a, double b)
//{
//	return a + b;
//}
//
//void swap(int& a, int& b)
//{
//	int tmp = a;
//	a = b;
//	b = tmp;
//}
//
//int main()
//{
//	const int a = 10;
//	//不能
//	//int& ra = a;		//属于权限放大，
//	const int& ra = a;
//
//	int b = 10;
//	const int& rb = b;	//属于权限缩小
//	const int& rbb = b;	//权限可以缩小不能放大
//
//	int c = 10;
//	double d = 1.1;
//	d = c;				//隐式类型转换		c先传给一个double类型的临时变量，临时变量在传给d
//
//	//double& rc = c;		//
//	const double& rc = c;	//临时变量具有常属性		rc是临时变量的别名
//
//	int e = 10, f = 20;
//	swap(e, f);
//	cout << e << endl << f << endl;
//
//	return 0;
//}

//int add(int a, int b)
//{
//	int c = a + b;
//	return c;		//先把结果给临时变量，然后临时变量在传给ret
//}
//
//int main()
//{
//	//int& ret = add(1, 2);	//程序没问题，ret就是c的别名,所以这里接受的是c的临时变量
//	const int& ret = add(1, 2);
//	
//	cout << ret << endl;
//	return 0;
//}

//int& add(int a, int b)
//{
//	int c = a + b;
//	return c;
//}
//
//int main()
//{
//	int& ret = add(1, 2);
//	cout << "hello world" << endl;
//
//	add(5, 7);
//	cout << ret << endl;
//	return 0;
//}

namespace N1
{
	int a = 10;
	int b = 20;
}

namespace N2
{
	int a = 10;
}


//int main()
//{
//	cout << N1::a << endl;
//	cout << N1::a2 << endl;
//	cout << N1::b2 << endl;
//	return 0;
//}

//int main()
//{
//	printf("%d\n", N1::a);
//	return 0;
//}


//using N1::a;
//
//int main()
//{
//	printf("%d\n", a);
//	return 0;
//}

//using namespace N1;
//int main()
//{
//	printf("%d\n", a);
//	printf("%d\n", b);
//
//	
//	return 0;
//}

//int main()
//{
//	cout << "hello c++" << endl;
//	int a;
//	cin >> a;
//	cout << a << endl;
//	return 0;
//}

//void Add(int a, int b)
//{
//	int c = a + b;
//	cout << c << endl;
//}
//
//int main()
//{
//	Add();
//	Add(1, 2);
//	return 0;
//}

//int Add(int a, int b)
//{
//	return a + b;
//}
//
//short Add(int a, int b)
//{
//	return a + b;
//}
//int main()
//{
//
//	return 0;
//}

//int main()
//{
//	int a = 10;
//	int& ra = a;
//	int& rb = a;
//	int& rc = a;
//	//int& rr;			//这条会报错，必须要初始化
//	int b = 20;
//	rb = b;				//这里是把b的值赋给rb，也是a
//	cout << ra << endl;
//	return 0;
//}

int main()
{
	int a = 10;
	const int& ra = a;	//不会报错，这里是权限的缩小
	
	const int b = 20;
	//int& rb = b;		//会报错，这里是权限的放大
	const int& rb = b;	//权限可以缩小，但是不能放大

	double c = 3.14;
	const int& rc = c;	//这里发生了隐式类型转换，c先把值传给一个int类型的临时变量
						//临时变量在传给rc，临时变量具有常属性，所以要用const修饰
						//这里rc存的应该是3，rc改变了，它的本体也应该改变
	cout << c << endl;	//但是c没变，说明了，c是把值传给了临时变量
	return 0;
}