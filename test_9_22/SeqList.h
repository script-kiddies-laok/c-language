#pragma once

#include <stdio.h>
#include <assert.h>
#include <stdlib.h>


typedef int SLDataType;
typedef struct SeqList
{
	SLDataType* array;		//指向动态开辟的数组
	int size;				//有效个数
	int capacity;			//容量空间的大小
}SeqList;

//顺序表的初始化
void SeqListInit(SeqList* pg);

//顺序表销毁
void SeqListDestory(SeqList* pg);

//顺序表的打印
void SeqListPrint(SeqList* pg);

//顺序表扩容
void SeqCheckCapacity(SeqList* pg);

//顺序表尾插
void SeqListPushBack(SeqList* pg,SLDataType x);

//顺序表尾删
void SeqListPopBack(SeqList* pg);

//顺序表头插
void SeqListPushFront(SeqList* pg,SLDataType x);

//顺序表头删
void SeqListPopFront(SeqList* pg);

//顺序表查找
int SeqListFind(SeqList* pg, SLDataType x);

//顺序表在pos位置插入数据
void SeqListInsert(SeqList* pg, int pos, SLDataType x);

//顺序表删除pos位置的值
void SeqListErase(SeqList* pg, int pos);