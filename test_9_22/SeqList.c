#define _CRT_SECURE_NO_WARNINGS 1

#include "SeqList.h"


void SeqListInit(SeqList* pg)
{
	assert(pg);
	
	pg->array = NULL;
	pg->size = 0;
	pg->capacity = 0;
}


void SeqListDestory(SeqList* pg)
{
	free(pg->array);
	pg->array = NULL;
	pg->capacity = pg->size = 0;
}


void SeqListPrint(SeqList* pg)
{
	assert(pg);
	for (int i = 0; i < pg->size; i++)
	{
		printf("%d ", pg->array[i]);
	}

	printf("\n");
}

void SeqCheckCapacity(SeqList* pg)
{
	assert(pg);

	if (pg->size == pg->capacity)
	{
		int NewCapacity = pg->capacity == 0 ? 4 : pg->capacity * 2;
		SLDataType* New = (SLDataType*)realloc(pg->array, NewCapacity);
		if (New == NULL)
		{
			perror("CheckCapacity realloc");
			exit(-1);
		}

		pg->array = New;
		pg->capacity = NewCapacity;
	}

}


void SeqListPushBack(SeqList* pg, SLDataType x)
{	
	assert(pg);
	SeqCheckCapacity(pg);
	
	pg->array[pg->size] = x;
	pg->size++;
}


void SeqListPopBack(SeqList* pg)
{
	assert(pg);
	assert(pg->size > 0);

	pg->size--;
}

void SeqListPushFront(SeqList* pg,SLDataType x)
{
	assert(pg);
	assert(pg->size > 0);
	SeqCheckCapacity(pg);

	int end = pg->size - 1;

	while (end >= 0)
	{
		pg->array[end + 1] = pg->array[end];
		end--;
	}

	pg->array[0] = x;
	pg->size++;
}


void SeqListPopFront(SeqList* pg)
{
	assert(pg);
	assert(pg->size > 0);
	int start = 0;
	while (start < pg->size - 1)
	{
		pg->array[start] = pg->array[start + 1];
		start++;
	}
	pg->size--;
}

int SeqListFind(SeqList* pg, SLDataType x)
{
	assert(pg);
	int count = 0;
	for (int i = 0; i < pg->size; i++)
	{
		if (pg->array[i] != x)
		{
			count++;
		}
	}
	if (count >= pg->size)
	{
		return -1;
	}
	else
		return count;
}


void SeqListInsert(SeqList* pg, int pos, SLDataType x)
{
	assert(pg);
	assert(pos <= pg->size);

	SeqCheckCapacity(pg);

	//pos=2
	int end = pg->size - 1;
	while (end >= pos)
	{
		pg->array[end + 1] = pg->array[end];
		end--;
	}
	pg->array[pos] = x;
	pg->size++;
}


void SeqListErase(SeqList* pg, int pos)
{
	assert(pg);
	assert(pos <= pg->size);
	int start = pos;
	while (start <= pg->size)
	{
		pg->array[start] = pg->array[start + 1];
		start++;
	}
	pg->size--;
}