#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>

//int main()
//{
//	int arr[3][4] = { 1,2,3,4,2,3,4,5,3,4,5,6 };
//	int* p = &arr[0][0];
//	for (int i = 0; i < 12; i++)
//	{
//		printf("%d ", *(p + i));
//	}
//	return 0;
//}

//void bubble_sort(int* arr, int temp,int sz)
//{
//	int i = 0;
//	for ( i = 0; i < sz-1; i++)
//	{
//		int j = 0;
//		int flag = 1;
//		for ( j = 0; j < sz-1 - i; j++)
//		{
//			if (arr[j] > arr[j + 1])
//			{
//				temp = arr[j];
//				arr[j] = arr[j + 1];
//				arr[j + 1] = temp;
//				flag = 0;
//			}
//		}
//		if (flag == 1)
//			break;
//	}
//}
//
//int main()
//{
//	int arr[10] = { 9,8,7,6,5,4,3,2,1,0 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	printf("%d\n", sz);
//	int temp = 0;
//	bubble_sort(arr,temp,sz);
//	//arr=&arr[0]
//	for (int i = 0; i < 10; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	return 0;
//}

//int main()
//{
//	int arr[5] = { 1,2,3,4,5 };
//	printf("%p\n", arr);
//	printf("%p\n", &arr[0]);
//	//数组名就是首元素的地址
//	printf("%d\n", *arr);
//	return 0;
//}

//int main()
//{
//	int arr[2][2] = { 0 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	printf("%d\n", sz);
//	return 0;
//}

//void bubble_sort(int arr[], int sz)
//{
//	int temp = 0;
//	int flag = 1;
//	for (int i = 0; i < sz - 1; i++)
//	{
//		for (int j = 0; j < sz - 1 - i; j++)
//		{
//			if (arr[j] > arr[j + 1])
//			{
//				temp = arr[j];
//				arr[j] = arr[j + 1];
//				arr[j + 1] = temp;
//				flag = 0;
//			}
//		}
//		if (flag == 1)
//			break;
//	}
//}
//
//int main()
//{
//	int arr[10] = { 9,8,7,6,5,4,3,2,1,0 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	bubble_sort(arr,sz);
//	for (int i = 0; i < sz; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	return 0;
//}

//void init(int arr[],int sz)
//{
//	for (int i = 0; i < sz; i++)
//	{
//		arr[i] = 0;
//	}
//}
//
//void print(int arr[], int sz)
//{
//	for (int i = 0; i < sz; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	printf("\n");
//}
//
//void swap(int* p1, int* p2)
//{
//	int temp = *p1;
//	*p1 = *p2;
//	*p2 = temp;
//}
//
//void reverse(int* arr, int sz)
//{
//	for (int i = 0; i <= sz / 2; i++)
//	{
//		swap(arr + i, arr + sz - 1 - i);
//	}
//}
//
//int main()
//{
//	int arr[10] = { 0,1,2,3,4,5,6,7,8,9 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	//打印数组
//	print(arr, sz);
//	//交换数组的逆置
//	reverse(arr,sz);
//	//打印数组逆置后的元素
//	print(arr, sz);
//	//初始化数组
//	init(arr, sz);
//
//	print(arr, sz);
//	return 0;
//}

//void swap(int* p1, int* p2)
//{
//	int temp = *p1;
//	*p1 = *p2;
//	*p2 = temp;
//}
//
//void reverse(int* arr1, int* arr2, int sz1)
//{
//	for (int i = 0; i < sz1; i++)
//	{
//		swap(arr1+i, arr2+i);
//	}
//}
//
//void print(int arr1[], int sz1)
//{
//	for (int i = 0; i < sz1; i++)
//	{
//		printf("%d ", arr1[i]);
//	}
//	printf("\n");
//}
//
//int main()
//{
//	int arr1[10] = { 1,2,3,4,5,6,7,8,9,0 };
//	int arr2[10] = { 9,8,7,6,5,4,3,2,1,0 };
//
//	int sz1 = sizeof(arr1) / sizeof(arr1[0]);
//	print(arr1, sz1);
//	print(arr2, sz1);
//	printf("交换之后\n");
//	reverse(arr1, arr2, sz1);
//	print(arr1,sz1);
//	print(arr2, sz1);
//	return 0;
//}

//void swap(int* p1, int* p2)
//{
//	int temp = *p1;
//	*p1 = *p2;
//	*p2 = temp;
//}

//void reverse_string(char* arr)
//{
//	int len = strlen(arr);
//	char tmp = *arr;
//	*arr = *(arr + len - 1);
//
//	*(arr + len - 1) = '\0';
//	if (strlen(arr + 1) >= 2)
//		reverse_string(arr + 1);
//
//	* (arr + len - 1) = tmp;
//}
//
//int main()
//{
//	char arr[] = "abcdef";
//
//	printf("%s\n", arr);
//	reverse_string(arr);
//	printf("%s\n", arr);
//	return 0;
//}

//int main()
//{
//	int a = -1;
//	int b = a << 1;
//	printf("%d\n", b);
//	//正整数的原码，补码，反码都相同
//	//负整数的原码
//	//10000000000000000000000000000001
//	//反码，原码按位取反
//	//11111111111111111111111111111110
//	//补码，反码加一
//	//11111111111111111111111111111111
//	//整数在内存中存放是以补码存放的
//
//	//左移后
//	//11111111111111111111111111111110
//	//反码
//	//11111111111111111111111111111101
//	//原码
//	//10000000000000000000000000000010
//	//-2
//	return 0;
//}

//int main()
//{
//	int a = -10;
//	int b = a >> 1;
//	//右移操作符
//	//1.算术
//	//左边补原来的符号位
//	//2.逻辑
//	//左边补0
//	printf("%d\n", b);
//	return 0;
//}

//int main()
//{
//	int a = 3;
//	int b = 5;
//	//int c = a & b;//按（2进制）位与
//	//00000000000000000000000000000011
//	//00000000000000000000000000000101
//	//00000000000000000000000000000001
//	//对位同为1，则为1
//
//	//int c = a | b;//按（2进制）位或
//	//00000000000000000000000000000011
//	//00000000000000000000000000000101
//	//00000000000000000000000000000111
//	//对应位上有1，则为1
//
//	int c = a ^ b;//按（2进制）位异或
//	//相同为0，相异为1
//	//00000000000000000000000000000011
//	//00000000000000000000000000000101
//	//00000000000000000000000000000110
//
//	printf("%d\n", c);
//	return 0;
//}

//int main()
//{
//	int a = 3;
//	int b = 5;
//	a = a + b;
//	b = a - b;
//	a = a - b;
//	//整形溢出
//	printf("%d %d", a, b);
//	return 0;
//}

//int main()
//{
//	int a = 31;
//	int b = 5;
//	a = a ^ b;
//	b = a ^ b;
//	a = a ^ b;
//	printf("%d %d\n", a, b);
//	return 0;
//}

//int main()
//{
//	int a = 15;
//	int count = 0;
//	for (int i = 1; i <= 31; i++)
//	{
//		if ((a & 1) == 1)
//			count++;
//		a >>= 1;
//	}
//	printf("%d\n", count);
//	return 0;
//}
//#include <string.h>
//int main()
//{
//	char arr[10] = "asdasd";
//	printf("%d\n", strlen(arr));
//	return 0;
//}

