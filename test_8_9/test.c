#define _CRT_SECURE_NO_WARNINGS 1



//int my_strcmp(const char* dest, const char* src)
//{
//	assert(dest && src);
//	while (*dest == *src)
//	{
//		if (*src == '\0')
//			return 0;
//		dest++;
//		src++;
//	}
//	return *dest - *src;
//}

//int main()
//{
//	int ret = my_strcmp("aza", "azb");
//	printf("%d\n", ret);
//	return 0;
//}

#include <string.h>
#include <stdio.h>
//
//int main()
//{
//	char arr1[20] = { "hello " };
//	char arr2[] = "world";
//	strncat(arr1, arr2, 5);
//	printf("%s\n",arr1);
//	return 0;
//}



//int main()
//{
//	char arr1[20] = { 0 };
//	char arr2[] = "hello world";
//	strncpy(arr1, arr2, 5);
//
//	printf("%s\n", arr1);
//	return 0;
//}

void left_move(char* src, int k)
{
	int i = 0;
	int len = strlen(src);

	for (i = 0; i < k; i++)
	{
		char tmp = *src;
		int j = 0;
		for (j = 0; j < len - 1; j++)
		{
			*(src + j) = *(src + j + 1);
		}
		*(src+len-1) = tmp;
	}
}

int main()
{
	char arr1[] = "hello";
	char arr2[] = "elloh";
	int i = 0;
	int len = strlen(arr1);

	//gets_s(arr2, 5);

	for (i = 0; i < len; i++)
	{
		left_move(arr1, 1);
		if (strcmp(arr1, arr2) == 0)
		{
			printf("YES!\n");
			break;
		}
	}
	if (i == len)
	{
		printf("No\n");
	}
	return 0;
}