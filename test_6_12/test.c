#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>

int main()
{
    int n, temp;
    int a[40] = { 0 };
    scanf("%d", &n);
    for (int i = 0; i < n; i++)
    {
        scanf("%d", &a[i]);
    }
    for (int j = 0; j < n - 1; j++)
    {
        for (int k = 0; k < n - 1 - j; k++) {
            if (a[k] < a[k + 1])
            {
                temp = a[k];
                a[k] = a[k + 1];
                a[k + 1] = temp;
            }
        }
    }
    for (int m = 0; m < n; m++)
    {
        printf("%d ", a[m]);
    }
    return 0;
}