#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

//
//struct s
//{
//	int i;
//	char arr[];
//};
//
//int main()
//{
//	struct s* p = (struct s*)malloc(sizeof(struct s) + 10);
//	if (p == NULL)
//	{
//		perror("malloc");
//		return 0;
//	}
//
//	int i = 0;
//	p->i = 100;
//
//	for (i = 0; i < 10; i++)
//	{
//		p->arr[i] = 'a' + i;
//	}
//
//	for (i = 0; i < 10; i++)
//	{
//		printf("%c ", p->arr[i]);
//	}
//
//	struct s* ptr = realloc(p,24);
//	if (ptr != NULL)
//	{
//		p = ptr;
//	}
//	p->i = 100;
//	for (i = 0; i < 20; i++)
//	{
//		p->arr[i] = 'x';
//	}
//	free(p);
//	p = NULL;
//	return 0;
//}

//
//struct s
//{
//	int num;
//	char* arr;
//};
//
//int main()
//{
//	struct s* p = (struct s*)malloc(sizeof(struct s));
//	if (p == NULL)
//	{
//		perror("malloc");
//		return 0;
//	}
//	p->num = 100;
//	p->arr = (char*)malloc(6 * sizeof(char));
//	if (p->arr == NULL)
//	{
//		perror("malloc");
//		return 0;
//	}
//	int i = 0;
//	for (i = 0; i < 6; i++)
//	{
//		p->arr[i] = 'a' + i;
//	}
//
//	for (i = 0; i < 6; i++)
//	{
//		printf("%c ", p->arr[i]);
//	}
//	char* ptr = realloc(p->arr, 10);
//	if (ptr != NULL)
//	{
//		p->arr = ptr;
//	}
//	for (i = 0; i < 10; i++)
//	{
//		p->arr[i] = 'x';
//	}
//
//	for (i = 0; i < 10; i++)
//	{
//		printf("%c ", p->arr[i]);
//	}
//	free(p->arr);
//	p->arr = NULL;
//
//	free(p);
//	p = NULL;
//	return 0;
//}

//void sort_int(int arr[], int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz - 1; i++)
//	{
//		int j = 0;
//		for (j = 0; j < sz - 1 - i; j++)
//		{
//			if (arr[j] > arr[j + 1])
//			{
//				int tmp = arr[j];
//				arr[j] = arr[j + 1];
//				arr[j + 1] = tmp;
//			}
//		}
//	}
//}
//
//
//void check_number(int arr[],int sz)
//{
//	//int count = 0;
//	int	i = 0;
//	for (i = 0; i < sz - 1; i++)
//	{
//		int j = 0;
//		for (j = 0; j < sz - 1 - i; j++)
//		{
//			if (arr[j] > arr[j + 1])
//			{
//				int tmp = arr[j];
//				arr[j] = arr[j + 1];
//				arr[j + 1] = tmp;
//			}
//		}
//	}
//	i = 0;
//	while (i<sz)
//	{
//		if (arr[i] == arr[i + 1])
//		{
//			i += 2;
//		}
//		else
//		{
//			printf("%d\n", arr[i]);
//			i++;
//		}
//	}
//	
//}
//
//int main()
//{
//	//int arr[50] = { 1,1,2,2,3,4,4,5,6,6,7,7 };
//	int arr[] = { 1,2,3,4,5,7,8,9,9,8,6,5,4,3,2,1 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	
//	check_number(arr,sz);
//
//	return 0;
//}


//int main()
//{
//	char* str = "   -12345";
//	int ret = atoi(str);
//
//	printf("%d\n", ret);
//	return 0;
//}


//int my_atoi(const char* str)
//{
//	int count = 0;
//	double n = 0;
//	const char* p = str;
//	int i = 0;
//
//	int arr[100] = { 0 };
//
//
//	while (*str && *str != '.' && *str > 47 && *str < 57)
//	{
//		count++;
//		str++;
//	}
//	for (i = 0; i < count; i++)
//	{
//		arr[i] = p[i] - 48;
//		n += arr[i] * pow((double)10, (double)count - (double)i - (double)1);
//	}
//
//	return (int)n;
//}
//
//int main()
//{
//	char str[] = "113.45";
//	//char str[] = "h";
//	int n = my_atoi(str);
//
//	//printf("%d\n", *str);
//	printf("%d\n", n);
//	return 0;
//}

int my_atoi(const char* str)
{
	assert(str);
	int ret = 0;
	while (*str)
	{
		ret = ret * 10 + *str - '0';
		str++;
	}

	return ret;
}

int main()
{
	char* p = "12345";
	int ret = my_atoi(p);

	printf("%d\n", ret);
	return 0;
}