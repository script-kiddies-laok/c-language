#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>
#include <string.h>


int cmp(const void* e1,const void * e2)
{
	return *((int*)e1) - *((int*)e2);
}

//int cmp_str(const void* e1, const void* e2)
//{
//	return strcmp();
//}

void _swap(char* arr1, char* arr2, size_t width)
{
	for (size_t i = 0; i < width; i++)
	{
		int tmp = *arr1;
		*arr1 = *arr2;
		*arr2 = tmp;
		arr1++;
		arr2++;
	}
}

void bubble_sort(void* arr,size_t sz,size_t width, int(*cmp)(const void* e1, const void* e2))
{
	for (size_t i = 0; i < sz - 1; i++)
	{
		for (size_t j = 0; j < sz - 1 - i; j++)
		{
			//判断返回值，大于0，交换次序
			if (cmp((char*)arr + j * width, (char*)arr + (j + 1) * width)>0)
			{
				_swap((char*)arr + j * width, (char*)arr + (j + 1) * width, width);
			}
		}
	}
}

void print_arr(int arr[], int sz)
{
	for (int i = 0; i < sz; i++)
	{
		printf("%d ", arr[i]);
	}
}

void test()
{
	int arr[] = { 1,2,3,4,5,6,7,8,9,0 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	bubble_sort(arr,sz,sizeof(arr[0]),cmp);
	print_arr(arr, sz);
}


int main()
{
	test();
	return 0;
}