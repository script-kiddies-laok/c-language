#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>

//int main()
//{
//	printf("[%d]\n", 123);
//	printf("[%.4d]\n", 123);
//	printf("[%.6d]\n", 12345);
//	printf("[%05d]\n", 123);
//	return 0;
//}

//int main()
//{
//	int n;
//
//	do
//	{
//		int no=0;
//
//		printf("请输入一个整数：");
//		scanf("%d", &no);
//
//		if (no % 2 == 0)
//			printf("偶数\n");
//		else
//			printf("不是偶数\n");
//
//		printf("要再来一次吗?【yes--0/no--9】\n");
//		scanf("%d", &n);
//	} while (n==0);
//}

//int main()
//{
//	int num = 0;
//
//	do {
//		printf("请输入一个非负整数:");
//		scanf("%d", &num);
//		if (num <= 0)
//			printf("请输入正整数\n");
//	} while (num<=0);
//
//	printf("该整数逆向输出:");
//	do {
//		printf("%d", num % 10);
//		num /= 10;
//	} while (num>0);
//
//	return 0;
//}

//int main()
//{
//	int num = 0;
//
//	do {
//		printf("请输入一个非负整数:");
//		scanf("%d", &num);
//		if (num <= 0)
//			printf("请输入正整数\n");
//	} while (num <= 0);
//
//	//printf("该整数逆向输出:");
//	int count = 0;
//	do {
//		//printf("%d", num % 10);
//		num /= 10;
//		count++;
//	} while (num > 0);
//	printf("该整数的位数是%d\n", count);
//
//	return 0;
//}

//int main()
//{
//	int sum = 0;
//	int n = 28;
//	do {
//		sum += n;
//		n += 1;
//	} while (n<=37);
//	printf("%d\n", sum);
//	return 0;
//}


//int main()
//{
//	int n;
//
//	printf("请输入一个整数:");
//	scanf("%d", &n);
//
//	while (n >= 0) {
//		printf("%d ", n--);
//		if (n == 0)
//			break;
//	}
//	//if(n>=0)
//		putchar('\n');
//
//	return 0;
//}

//int main()
//{
//	int i, n, m;
//
//	printf("请输入一个正整数：");
//	scanf("%d", &n);
//
//	i = 1;
//	m = n;
//	while (i <= n)
//		printf("%d ", i++);
//	if(m>=0)
//		putchar('\n');
//
//	return 0;
//}

//int main()
//{
//	int n, i;
//
//	printf("请输入一个正整数:");
//	scanf("%d", &n);
//
//	i = 2;
//	while (i<=n)
//	{
//		printf("%d ", i );
//		i += 2;
//	}
//	putchar('\n');
//
//	return 0;
//}

//int main()
//{
//	int i, n,tmp;
//
//	printf("请输入一个正整数:");
//	scanf("%d", &n);
//
//	i = 4;
//	printf("2 ");
//	while (i <= n)
//	{
//		tmp = i;
//		if (tmp % 2 == 0)
//		{
//			tmp = tmp / 2;
//			if (tmp % 2 == 0)
//				printf("%d ", i);
//		}
//		i++;
//	}
//
//	return 0;
//}

//int main()
//{
//	int n, i;
//
//	printf("请输入一个整数:");
//	scanf("%d", &n);
//
//	i = 2;
//	while (i <= n)
//	{
//		printf("%d ", i);
//		i *= 2;
//	}
//
//	return 0;
//}

//int main()
//{
//	int n;
//
//	printf("请输入一个整数:");
//	scanf("%d", &n);
//
//	while (n-- > 0)
//	{
//		putchar('*');
//		putchar('\n');
//	}
//	return 0;
//}

//int main()
//{
//	int i = 0;
//	int sum = 0;
//	int num, tmp;
//
//	printf("要输入多少个整数:");
//	scanf("%d", &num);
//
//	while (i < num) {
//		printf("No.%d:", ++i);
//		scanf("%d", &tmp);
//		sum += tmp;
//	}
//
//	printf("合计值:%d\n", sum);
//	printf("平均值:%.2f\n", (double)sum / num);
//
//	return 0;
//}

//int main()
//{
//	int n, i;
//
//	scanf("%d", &n);
//
//	i = 1;
//	while (i <= n) {
//		printf("%d", i % 10);
//		i++;
//	}
//
//	return 0;
//}

//int main()
//{
//	int i, j;
//
//	for (i = 1; i <= 9; i++)
//	{
//		for (j = 1; j <= 9; j++)
//			printf("%4d", i * j);
//		printf("\n");
//	}
//
//	return 0;
//}

//int main()
//{
//	int i, j;
//	int a;	//横标题
//
//	printf("  |");
//	for (a = 1; a <= 9; a++)
//		printf("  %d", a);
//
//	printf("\n");
//	printf("-------------------------------\n");
//
//	for (i = 1; i <= 9; i++)
//	{ 
//		printf("%d ", i);
//		printf("|");
//		for (j = 1; j <= 9; j++)
//		{
//			printf("%3d", i * j);
//		}
//		printf("\n");
//	}
//
//	return 0;
//}

//int main()
//{
//	int count;
//
//	do 
//	{
//		int num, i;
//		do
//		{
//			printf("请输入一个非负整数:");
//			scanf("%d", &num);
//		} while (num<0);
//
//		for (i = 1; i <= num; i++)
//			printf("* ");
//		
//		putchar('\n');
//		printf("继续1，退出0:");
//		scanf("%d", &count);
//
//	} while (count);
//
//	return 0;
//}

//int main()
//{
//	int i, j;
//	int high, weight;
//
//	printf("描述长方形\n");
//	printf("长:");			
//	scanf("%d", &high);
//
//	printf("宽:");			
//	scanf("%d", &weight);
//
//	for (i = 1; i <= high; i++)
//	{
//		for (j = 1; j <= weight; j++)
//		{
//			printf("* ");
//		}
//		printf("\n");
//	}
//	return 0;
//}

//int main()
//{	
//	int n;
//	int i, j;
//
//	printf("三角形有几层:");
//	scanf("%d", &n);
//
//	for (i = 1; i <= n; i++)
//	{
//		for (j = 5; j >=i; j--)
//		{
//			printf("* ");
//		}
//		printf("\n");
//	}
// 左上角
//
//	return 0;
//}

//int main()
//{
//	int n;
//	int i, j, k;
//
//	printf("三角形有几层:");
//	scanf("%d", &n);
//
//	for (i = 1; i <= n; i++)
//	{
//		for (k = 1; k <= i - 1; k++)
//		{
//			printf("  ");
//		}
//		for (j = 5; j >= i; j--)
//		{
//			printf("* ");
//		}
//
//		printf("\n");
//	}
//
//	return 0;
//}

//int main()
//{
//	int i, j, n;
//
//	printf("三角形有几层:");
//	scanf("%d", &n);
//
//	for (i = 1; i <= n; i++)
//	{
//		for (j = 1; j <= n - i; j++)
//			printf(" ");
//		for (j = 1; j <= i; j++)
//			printf("*");
//		printf("\n");
//	}	
//
//	return 0;
//}

//int main()
//{
//	int i, j, n;
//
//	scanf("%d", &n);
//
//	for (i = 1; i <= n; i++)
//	{
//		for (j = n-1; j >= i ; j--)
//			printf(" ");
//		for (j = 1; j <= 2 * i - 1; j++)
//			printf("*");
//		printf("\n");
//	}
//
//	return 0;
//}

//int main()
//{
//	puts("asdasdasd" "\n"
//				"zxczxczxc"
//					);
//
//
//
//	return 0;
//}\

//int main()
//{
//	int i;
//	int vx[8];
//
//	for (i = 0; i < 8; i++)
//		scanf("%d", &vx[i]);
//
//	int sz = sizeof(vx) / sizeof(vx[0]);
//
//	for (i = 0; i < sz/2 ; i++)
//	{
//		int tmp=0;
//		tmp = vx[i];
//		vx[i] = vx[sz-1-i];
//		vx[sz-1 - i] = tmp;
//	}
//
//	for (i = 0; i < 8; i++)
//		printf("%d ", vx[i]);
//
//	return 0;
//}

//#define NUMBER 5
//
//int main()
//{
//	int i;
//	int score[NUMBER];
//	int max, min;
//
//	printf("请输入学生的分数\n");
//	for (i = 0; i < NUMBER; i++)
//	{
//		printf("学生%d:", i + 1);
//		scanf("%d", &score[i]);
//	}
//
//	max = min = score[0];
//	for (i = 1; i < NUMBER; i++)
//	{
//		if (score[i] > max)
//			max = score[i];
//		if (score[i] < min)
//			min = score[i];
//	}
//
//	printf("最大值%d,最小值%d\n", max, min);
//
//	return 0;
//}

//#define NUMBER 5
//
//int main()
//{
//	int i;
//	int snum=0;
//	int score[NUMBER];
//	int succe[NUMBER];
//
//	puts("请输入学生的分数");
//	for (i = 0; i < NUMBER; i++)
//	{
//		printf("%d号:",i+1);
//		scanf("%d", &score[i]);
//		if (score[i] >= 60)
//			succe[snum++] = i+1;
//	}
//
//	puts("及格的学生");
//	for (i = 0; i < snum; i++)
//	{
//		printf("%d号:%d\n", succe[i],score[succe[i]-1]);
//	}
//
//	return 0;
//}

//#define NUMBER 80
//
//int main()
//{
//	int n;
//	int i, j;
//	int score[NUMBER];
//	int fenbu[11] = {0};
//
//	printf("请输入学生人数:");
//	do {
//		scanf("%d", &n);
//		if (n<1 || n>NUMBER)
//			printf("请输入[1-%d]的数字\n", NUMBER);
//	} while (n<1||n>NUMBER);
//
//	printf("请输入学生的分数\n");
//	for (i = 0; i < n; i++)
//	{
//		printf("%d号:", i + 1);
//		do {
//			scanf("%d", &score[i]);
//			if (score[i] < 0 || score[i]>100)
//				printf("请输入[1-100]的数字\n");
//		} while (score[i] < 0 || score[i]>100);
//
//		fenbu[score[i] / 10]++;
//	}
//
//	printf("\n 分布图	\n");
//	printf("  100:");
//	for (j = 0; j < fenbu[10]; j++)
//		printf("*");
//	printf("\n");
//
//	for (i = 9; i >= 0; i--)
//	{
//		printf("%3d-%3d:", i * 10, i * 10 + 9);
//		for (j = 0; j < fenbu[i]; j++)
//			printf("*");
//		printf("\n");
//	}
//
//	return 0;
//}

//int main()
//{
//	int i, j;
//	int ma[2][3] = { {1,2,3},{4,5,6} };
//	int mb[2][3] = { {4,5,6},{7,8,9} };
//	int mc[2][3];
//
//	for (i = 0; i < 2; i++)
//	{
//		for (j = 0; j < 3; j++)
//		{
//			mc[i][j] = ma[i][j] + mb[i][j];
//		}
//	}
//
//	for (i = 0; i < 2; i++)
//	{
//		for (j = 0; j < 3; j++)
//		{
//			printf("%3d ", mc[i][j]);
//		}
//
//		printf("\n");
//	}
//
//	return 0;
//}

//int main()
//{
//	int i, j;
//	int ma[2][3] = { {1,2,3},{4,5,6} };
//	int mb[3][2] = { {1,5},{5,3},{8,1} };
//	int mc[5][5];
//
//	for (i = 0; i < 2; i++)
//	{
//		for (j = 0; j < 3; j++)
//		{
//			mc[i][j] = ma[i][j] * mb[j][i];
//		}
//	}
//
//	for (i = 0; i < 2; i++)
//	{
//		for (j = 0; j < 3; j++)
//		{
//			printf("%d  ", mc[i][j]);
//		}
//	}
//
//	return 0;
//}


//#include <math.h>
//int main()
//{
//	int no,i;
//	unsigned int count=0;
//	printf("2\n");
//	for (no = 3; no <= 1000; no+=2) {
//		for ( i = 3; i < no; i+=2)
//		{
//			count++;
//			if (no % i == 0)
//				break;
//		}
//
//		if (no==i)
//			printf("%d\n",no);
//	}
//
//	printf("运算次数%d", count);
//	return 0;
//}

//int _min(int x, int y, int z)
//{
//	int min = 0;
//	min=(x < y) ? x : y;
//	return (min < z) ? min : z;
//}
//
//int main()
//{
//	int x, y, z;
//	scanf("%d%d%d", &x, &y, &z);
//	printf("%d\n", _min(x, y, z));
//	return 0;
//}

//int _lf(int n)
//{
//	return n * n * n;
//}
//
//int main()
//{
//	int n;
//	scanf("%d", &n);
//	printf("%d", _lf(n));
//	return 0;
//}

//double mi(double n, int k)
//{
//	double tmp=1;
//	while (k-- > 0)
//		tmp *= n;
//	return tmp;
//}
//
//int main()
//{
//	double n;
//	int	k;
//	scanf("%lf%d", &n, &k);
//	printf("%.2lf\n", mi(n, k));
//
//	return 0;
//}

//void put_stars(int i)
//{
//	while (i-- > 0)
//		printf("*");
//}
//
//int main()
//{
//	int i, n;
//
//	printf("三角形有几层:");
//	scanf("%d", &n);
//
//	for (i = 1; i <= n; i++)
//	{
//		put_stars(i);
//		printf("\n");
//	}
//
//	return 0;
//}

//void hello(void)
//{
//	printf("hello\n");
//}
//
//int main()
//{
//	hello();
//	return 0;   
//}

//#define NUMBER 5
//
//int score[NUMBER];
//
////int top(void);
//
//int top(void)
//{
//	extern int score[];
//	int i;
//	int max = score[0];
//	for (i = 1; i < NUMBER; i++)
//	{
//		if (score[i] > max)
//			max = score[i];
//	}
//	return max;
//}
//
//int main()
//{
//	extern int score[];
//	int i;
//
//	printf("请输入5名学生的分数;\n");
//	for (i = 0; i < NUMBER; i++)
//	{
//		printf("%d:", i + 1);
//		scanf("%d", &score[i]);
//	}
//	printf("最高分=%d\n", top());
//
//	return 0;
//}

//int maxof(int, int);

//#define NUMBER 5
//
//int max_of(int arr[], int n)
//{
//	int i;
//	int max = arr[0];
//	for (i = 1; i <= n; i++)
//	{
//		if (arr[i] > max)
//			max = arr[i];
//	}
//	return max;
//}
//
//int main()
//{
//	int i;
//	int eng[NUMBER];
//	int mat[NUMBER];
//	int max_e, max_m;
//
//	printf("请输入[%d]的分数\n", NUMBER);
//	for (i = 0; i < NUMBER; i++)
//	{
//		printf("[%d]英语:",i+1);
//		scanf("%d", &eng[i]);
//		printf("   数学:");
//		scanf("%d", &mat[i]);
//	}
//	max_e = max_of(eng, NUMBER);
//	max_m = max_of(mat, NUMBER);
//
//	printf("英语的最高分:%d\n", max_e);
//	printf("数学的最高分:%d\n", max_m);
//
//	return 0;
//}

//int min_of(const int arr[], int sz)
//{
//	int min = arr[0];
//	for (int i = 1; i < sz; i++)
//	{
//		if (arr[i] < min)
//			min = arr[i];
//	}
//	return min;
//}
//
//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8,9,0 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//
//	printf("最小值为%d\n", min_of(arr, sz));
//
//	return 0;
//}

//void rev_intary(int arr[], int sz)
//{
//	for (int i = 0; i < sz / 2; i++)
//	{
//		int tmp = arr[i];
//		arr[i] = arr[sz - 1 - i];
//		arr[sz - 1 - i] = tmp;
//	}
//}
//
//void print(int arr[], int sz)
//{
//	for (int i = 0; i < sz; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	printf("\n");
//}
//
//void exchange(int arr[], int arr1[], int sz)
//{
//	for (int i = 0; i < sz; i++)
//	{
//		arr1[i] = arr[i];
//	}
//}
//
//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8,9,0 };
//	int arr1[40];
//	int sz = sizeof(arr) / sizeof(arr[0]);
//
//	rev_intary(arr,sz);
//	exchange(arr, arr1, sz);
//	print(arr, sz);
//	print(arr1, sz);
//	return 0;
//}
// 


//#define NUMBER 5
//#define FAULT -1
//
//int search(int arr[], int key, int sz)
//{
//	int i = 0;
//	arr[sz] = key;
//	while (1)
//	{
//		if (arr[i] == key)
//			break;
//		i++;
//	}
//	return ((i == sz) ? -1 : i);
//}
//
//int main()
//{
//	int i, key, re;
//	int arr[NUMBER] ;
//
//	for (i = 0; i < 5; i++)
//	{
//		printf("vx[%d]:", i);
//		scanf("%d", &arr[i]);
//	}
//
//	printf("请输入要查找的值:");
//	scanf("%d", &key);
//
//	re = search(arr,key,NUMBER);
//
//	if (re == -1)
//		printf("没找到\n");
//	else 
//		printf("找到了，是vx[%d]\n", re);
//
//	return 0;
//}

int max_of(int arr[][3],int i,int j)
{
	int max = arr[j][i];
	if (arr[j][i] > max)
		max = arr[j][i];
	return max;
}

int main()
{
	int i, j,re[10];
	int arr[5][3];
	//int score[20];

	for (i = 0; i < 5; i++)
	{
		for (j = 0; j < 3; j++)
		{
			scanf("%d", &arr[i][j]);
		}
	}

	for (i = 0; i < 3; i++)
	{
		for (j = 0; j < 5; j++)
		{
			re[i] = max_of(arr,j,i);
		}
	}

	for (i = 0; i < 3; i++)
	{
		printf("%d\n", re[i]);
	}

	return 0;
}