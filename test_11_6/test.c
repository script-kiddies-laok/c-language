#define _CRT_SECURE_NO_WARNINGS 1

#include "Stack.h"

void Print(int* a, int n)
{
	for (int i = 0; i < n; i++)
	{
		printf("%d ", a[i]);
	}
	printf("\n");
}

void Swap(int* a, int* b)
{
	int tmp = *a;
	*a = *b;
	*b = tmp;
}

void InsertSort(int* a, int n)
{
	for (int i = 0; i < n - 1; i++)
	{
		int end = i;
		int tmp = a[end + 1];
		while (end >= 0)
		{
			//找到比tmp小的值，跳出循环
			if (tmp < a[end])
			{
				a[end + 1] = a[end];
				end--;
			}
			else
			{
				break;
			}
		}
		//然后再tmp插入到，end的下一个位置
		a[end + 1] = tmp;
	}
}

void ShellSort(int* a, int n)
{
	int gap = n;
	while (gap > 1)
	{
		gap = gap / 3 + 1;
		for (int i = 0; i < n - gap; i++)
		{
			int end = i;
			int tmp = a[end + gap];
			while (end >= 0)
			{
				//找到比tmp小的值，跳出循环
				if (tmp < a[end])
				{
					a[end + gap] = a[end];
					end -= gap;
				}
				else
				{
					break;
				}
			}
			//然后再tmp插入到，end的下一个位置
			a[end + gap] = tmp;
		}
	}
}

void SelectSort(int* a, int n)
{
	int left = 0;
	int right = n - 1;
	while (left < right)
	{
		int min = left, max = right;
		for (int i = left; i <= right; i++)
		{
			if (a[i] < a[min])
				min = i;
			if (a[i] > a[max])
				max = i;
		}
		//找出最大的和最小的，然后交换
		Swap(&a[left], &a[min]);
		//如果最大的元素，在left位置上,要换一下
		if (left == max)
		{
			max = min;
		}
		Swap(&a[max], &a[right]);
		right--;
		left++;
	}
}

void AdjustDown(int* a, int parent, int n)
{
	int child = 2 * parent + 1;
	while (child < n)
	{
		if (child + 1 < n && a[child + 1] > a[child])
			child++;
		if (a[parent] < a[child])
			Swap(&a[parent], &a[child]);
		parent = child;
		child = parent * 2 + 1;
	}
}

void HeapSort(int* a, int n)
{
	//从最小的孩子开始建堆,建大堆
	for (int i = (n - 1 - 1) / 2; i >= 0; i--)
	{
		AdjustDown(a, i, n);
	}

	int end = n-1;
	while (end >= 0)
	{
		Swap(&a[0], &a[end]);
		AdjustDown(a, 0, end);
		end--;
	}
}

void BubbleSort(int* a, int n)
{
	for (int i = 0; i < n - 1; i++)
	{
		int flag = 0;
		for (int j = 0; j < n - 1 - i; j++)
		{
			if (a[j + 1] > a[j])
			{
				Swap(&a[j], &a[j + 1]);
				flag = 1;
			}
		}
		if (flag == 0)
			break;
	}
}


int GetMidIndex(int* a, int left, int right)
{
	int keyi = (left + right) >> 1;
	if (a[left] > a[keyi])
	{
		if (a[keyi] > a[right])
			return keyi;
		else if (a[right] > a[left])
			return left;
		else
			return right;
	}	//a[keyi]>a[left]
	else
	{
		if (a[left] > a[right])
			return left;
		else if (a[right] > a[keyi])
			return keyi;
		else
			return right;
	}
}


int PartSort1(int* a, int left, int right)
{
	int mid = GetMidIndex(a, left, right);
	Swap(&a[mid], &a[left]);

	int keyi = left;
	while (left < right)
	{
		while (left < right && a[right] >= a[keyi])
			right--;
		
		while (left < right && a[left] <= a[keyi])
			left++;
		Swap(&a[left], &a[right]);
	}
	Swap(&a[keyi], &a[left]);
	return left;
}

int PartSort2(int* a, int left, int right)
{
	int keyi = a[left];
	while (left < right)
	{
		while (left < right && a[right] >= a[keyi])
			right--;
		a[left] = a[right];
		while (left < right && a[left] <= a[keyi])
			left++;
		a[right] = a[left];
	}
	a[left] = keyi;
	return left;
}

int PartSort3(int* a, int left, int right)
{
	int prev = left, cur = left + 1;
	int keyi = left;
	while (cur <= right)
	{
		if (a[cur] < a[keyi] && ++prev != cur)
			Swap(&a[cur], &a[prev]);

		cur++;
	}
	Swap(&a[keyi], &a[prev]);
	return prev;
}

void QuickSort(int* a, int left,int right)
{
	if (left >= right)
		return;

	int keyi = PartSort1(a, left, right);
	if (right - left > 20)
	{
		QuickSort(a, left, keyi - 1);
		QuickSort(a, keyi + 1, right);
	}
	else
		InsertSort(a + left, right - left + 1);
}

void QuickSortNonR(int* a, int left, int right)
{
	Stack st;
	StackInit(&st);
	StackPush(&st, left);
	StackPush(&st, right);
	while (!StackEmpty(&st))
	{
		int right1 = StackTop(&st);
		StackPop(&st);

		int left1 = StackTop(&st);
		StackPop(&st);

		int keyi = PartSort1(a, left1, right1);
		if (left1 < keyi - 1)
		{
			StackPush(&st, left1);
			StackPush(&st, keyi - 1);
		}
		if (keyi + 1 < right1)
		{
			StackPush(&st, keyi + 1);
			StackPush(&st, right1);
		}
	}
	StackDestroy(&st);
}

void _Merge(int* a, int begin1, int end1, int begin2, int end2, int* tmp)
{
	int i = begin1;
	int j = begin1;
	while (begin1 <= end1 && begin2 <= end2)
	{
		if (a[begin1] < a[begin2])
			tmp[i++] = a[begin1++];
		else
			tmp[i++] = a[begin2++];
	}

	while (begin1 <= end1)
		tmp[i++] = a[begin1++];
	while (begin2 <= end2)
		tmp[i++] = a[begin2++];

	for (; j <= end2; j++)
	{
		a[j] = tmp[j];
	}
}

void _MergeSort(int* a, int left, int right, int* tmp)
{
	if (left >= right)
		return;

	int mid = (left + right) >> 1;
	_MergeSort(a, left, mid, tmp);
	_MergeSort(a, mid + 1, right, tmp);

	_Merge(a, left, mid, mid + 1, right, tmp);
}

void MergeSort(int* a, int n)
{
	int* tmp = (int*)malloc(sizeof(int) * n);
	if (tmp == NULL)
	{
		perror("malloc fail");
		exit(-1);
	}

	_MergeSort(a, 0, n - 1, tmp);

	free(tmp);
}

void MergeSortNonR(int* a, int n)
{
	int* tmp = (int*)malloc(sizeof(int) * n);
	if (tmp == NULL)
	{
		perror("malloc fail");
		exit(-1);
	}
	int gap = 1;
	while (gap < n)
	{
		for (int i = 0; i < n; i += 2 * gap)
		{
			int begin1 = i, end1 = i + gap - 1;
			int begin2 = i + gap, end2 = i + 2 * gap - 1;

			//第二个小区间不存在
			if (begin2 >= n)
				break;
			
			//第二个小区间存在，但是不够gap个，结束位置越界了
			if (end2 >= n)
				end2 = n - 1;

			_Merge(a, begin1, end1, begin2, end2, tmp);
		}
		gap *= 2;
	}
}

void CountSort(int* a, int n)
{
	//找到最大的和最小的，确定区间
	int max = a[0], min = a[0];
	for (int i = 0; i < n; i++)
	{
		if (a[i] > max)
			max = a[i];
		if (a[i] < min)
			min = a[i];
	}

	int range = max - min + 1;
	int* tmp = (int*)malloc(sizeof(int) * range);
	if (tmp == NULL)
	{
		perror("malloc fail");
		exit(-1);
	}
	memset(tmp, 0, sizeof(int) * range);

	for (int i = 0; i < n; i++)
	{
		tmp[a[i] - min]++;			 //对应位上的个数有几个
	}

	int j = 0;
	for (int i = 0; i < range; i++)
	{
		while (tmp[i]--)
		{
			a[j++] = i + min;
		}
	}

	free(tmp);
}

int main()
{
	int a[] = { 2,4,5,1,6,-1 };
	//InsertSort(a, sizeof(a) / sizeof(a[0]));
	//ShellSort(a, sizeof(a) / sizeof(a[0]));
	//SelectSort(a, sizeof(a) / sizeof(a[0]));
	//HeapSort(a, sizeof(a) / sizeof(a[0]));
	//QuickSort(a, 0, sizeof(a) / sizeof(a[0]) - 1);
	//QuickSortNonR(a, 0, sizeof(a) / sizeof(a[0]) - 1);
	//MergeSortNonR(a, sizeof(a) / sizeof(a[0]));
	CountSort(a, sizeof(a) / sizeof(a[0]));
	Print(a, sizeof(a) / sizeof(a[0]));
	return 0;
}