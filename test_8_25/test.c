#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>
#include <stddef.h>


//struct Point
//{
//	int x;
//	int y;
//};
//
//struct Node
//{
//	struct Point p;
//	struct Node* next;
//};
//
//int main()
//{
//	struct Point p = { 3,5 };
//	printf("%d %d\n", p.x, p.y);
//
//	struct Node n = { {5,9},NULL };
//	printf("%d %d\n", n.p.x, n.p.y);
//
//	return 0;
//}

//
//struct S
//{
//	char c;
//	int i;
//};
//
//int main()
//{
//	struct S s = { 0 };
//	printf("%d\n", sizeof(s));
//	//offsetof计算结构体变量的偏移量
//	printf("%d\n", offsetof(struct S, c));
//	printf("%d\n", offsetof(struct S, i));
//
//	return 0;
//}

//struct S
//{
//	char c1;
//	int i;
//	char c2;
//};
////vs默认对齐数是8
////对齐数：自身大小和默认对齐数的较小值
////结构体的总大小是变量最大对齐数的整数倍
//int main()
//{
//	struct S s = { 0 };
//	printf("%d\n", sizeof(s));
//	printf("%d\n", offsetof(struct S, i));
//	printf("%d\n", offsetof(struct S, c2));
//
//	return 0;
//}

struct S
{
	double c1;
	char c2;
	int i;
};
//把小的放在一起

int main()
{
	struct S s = { 0 };
	printf("%d\n", sizeof(struct S));
	return 0;
}