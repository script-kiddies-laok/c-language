#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>
#include <math.h>

//int is_prime(int a)
//{
//	for (int i = 2; i < sqrt(a); i++)
//	{
//		if (a % i == 0)
//		{
//			return 0;
//			break;
//		}
//	}
//	return 1;
//}
//
//int main()
//{
//	int i = 0;
//	for (i = 100; i <= 200; i++)
//	{
//		if (is_prime(i) == 1)
//		{
//			printf("%d ", i);
//		}
//	}
//	return 0;
//}

//int is_leap_year(int a)
//{
//	if (((a % 4 == 0) && a % 100 != 0) || a % 400 == 0)
//	{
//		return 1;
//	}
//}	
//int main()
//{
//	int year = 0;
//	for (year = 1000; year <= 2000; year++)
//	{
//		if (is_leap_year(year) == 1)
//		{
//			printf("%d ", year);
//		}
//	}
//	return 0;
//}

//int binary_search(int arr[], int k,int r)
//{
//	//int arr[] 传递的是首元素的地址
//	int left = 0;
//	int right = r -1;
//	int mid = 0;
//	while (left <= right)
//	{
//		mid = left + (right - left) / 2;
//		if (k < arr[mid])
//		{
//			right = mid - 1;
//		}
//		else if (k > arr[left])
//		{
//			left = mid + 1;
//		}
//		else
//			return mid;
//	}
//	if (left >= right)
//	{
//		return -1;
//	}
//}
//
//int main()
//{
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	int k = 7;
//	int r = sizeof(arr) / sizeof(arr[0]);
//	//找到了，返回下标
//	//找不到，返回-1
//	int ret = binary_search(arr, k,r);
//	if (ret == -1)
//	{
//		printf("没找到\n");
//	}
//	else
//	{
//		printf("找到了,下标是%d\n",ret);
//	}
//	return 0;
//}
//int add(int* p)
//{
//	(*p)++;
//}
//int main()
//{
//	int num = 0;
//	add(&num);
//	printf("%d\n", num);
//	add(&num);
//	printf("%d\n", num);
//	return 0;
//}
//int exchang(int* pa, int* pb)
//{
//	int temp = 0;
//	temp = *pa;
//	*pa = *pb;
//	*pb = temp;
//}
//int main()
//{
//	int a = 10;
//	int b = 20;
//	exchang(&a, &b);
//	printf("%d %d\n", a, b);
//	return 0;
//}
//int math_9(int n)
//{
//	for (int i = 1; i <= n; i++)
//	{
//		for (int j = 1; j <= i; j++)
//		{
//			printf("%d*%d=%d ", i, j, i * j);
//		}
//		printf("\n");
//	}
//}
//int main()
//{
//	int n;
//	while (scanf("%d", &n) != EOF) {
//		math_9(n);
//	}
//	return 0;
//}
int is_leap_year(int a)
{
	if (((a % 4 == 0) && a % 100 != 0) || a % 400 == 0)
	{
		return 1;
	}
}

int main()
{
	int year = 0,mouth = 0;
	while (scanf("%d %d", &year,&mouth) != EOF)
	{
		int set = is_leap_year(year);
		if (mouth == 1 || mouth == 3 || mouth == 5 || mouth == 7 || mouth == 8 || mouth == 10 || mouth == 12)
		{
			printf("31\n");
		}
		else if (mouth == 4 || mouth == 6 || mouth == 9 || mouth == 11)
		{
			printf("30\n");
		}
		else if (mouth == 2)
		{
			if (set == 1)
			{
				printf("29\n");
			}
			else
			{
				printf("28\n");
			}
		}
	}
	return 0;
}