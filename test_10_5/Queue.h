#pragma once

#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <stdbool.h>




typedef int QDataType;

typedef struct QListNode
{
	struct QListNode* next;
	QDataType data;
}QListNode;

typedef struct Queue
{
	QListNode* head;
	QListNode* tail;
}Queue;

//初始化
void QueueInit(Queue* p);

//销毁
void QueueDestroy(Queue* p);

//尾插
void QueuePush(Queue* p,QDataType x);

//打印
void QueuePrint(Queue* p);

//头删
void QueuePop(Queue* p);

//获取头部元素
QDataType QueueFront(Queue* p);

//获取尾部元素
QDataType QueueBack(Queue* p);

//获取元素有效个数
int QueueSize(Queue* p);

//检查是否为空
bool QueueEmpty(Queue* p);