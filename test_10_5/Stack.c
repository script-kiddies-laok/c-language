#define _CRT_SECURE_NO_WARNINGS 1

#include "Stack.h"

void StackInit(Stack* p)
{
	p->data = (STDataType*)malloc(sizeof(STDataType) * 4);
	if (p->data == NULL)
	{
		perror("malloc fail\n");
		exit(-1);
	}
	p->capacity = 4;
	p->top = 0;
}


void StackDestroy(Stack* p)
{
	assert(p);

	free(p->data);
	p->data = NULL;
	p->capacity = p->top = 0;
}


void StackPush(Stack* p, STDataType x)
{
	assert(p);

	if (p->top == p->capacity)
	{
		STDataType*tmp = (STDataType*)realloc(p->data, sizeof(STDataType) * p->capacity * 2);
		p->capacity *= 2;
		if (tmp == NULL)
		{
			perror("realloc fail\n");
			exit(-1);
		}
		p->data = tmp;
		p->top++;
		p->data[p->top - 1] = x;

	}
	else
	{
		p->top++;
		p->data[p->top - 1] = x;
	}
}

void StackPrint(Stack* p)
{
	assert(p);

	for (int i = 0; i < p->top; i++)
	{
		printf("%d ", p->data[i]);
	}
	printf("\n");
}


void StackPop(Stack* p)
{
	assert(p);
	assert(p->top != 0);

	p->top--;
}

STDataType StackTop(Stack* p)
{
	assert(p);
	assert(p->top != 0);

	return p->data[p->top - 1];
}

int StackSize(Stack* p)
{
	assert(p);
	assert(p->top != 0);
	return p->top;
}

bool StackEmpty(Stack* p)
{
	assert(p);
	if (p->top == 0)
		return false;
	else
		return true;
}