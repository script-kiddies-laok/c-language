#define _CRT_SECURE_NO_WARNINGS 1

#include "Stack.h"
#include "Queue.h"


void test()
{
	Stack s;
	StackInit(&s);
	StackPush(&s, 1);
	StackPush(&s, 2);
	StackPush(&s, 3);
	StackPush(&s, 4);
	StackPop(&s);
	StackPrint(&s);

	//STDataType top = StackTop(&s);
	//printf("%d", top);

	int size = StackSize(&s);
	printf("%d\n", size);
	StackDestroy(&s);
}

void test1()
{
	Queue s;
	QueueInit(&s);
	QueuePush(&s, 1);
	QueuePush(&s, 2);
	QueuePush(&s, 3);
	QueuePush(&s, 4);
	QueuePop(&s);

	//QDataType tmp = QueueBack(&s);
	int tmp = QueueEmpty(&s);
	printf("%d\n", tmp);
	QueuePrint(&s);


	QueueDestroy(&s);
}

bool isValid(char* s)
{
	Stack st;
	StackInit(&st);
	while (*s)
	{
		//��������ջ
		if (*s == '{' || *s == '(' || *s == '[')
		{
			StackPush(&st, *s);
			s++;
		}
		else
		{
			//û��ǰ����
			if (st.top==0)
			{
				StackDestroy(&st);
				return false;
			}

			char top = StackTop(&st);
			if ((top == '{' && *s != '}')
				|| (top == '(' && *s != ')') 
				|| (top == '[' && *s != ']'))
			{
				StackDestroy(&st);
				return false;
			}
			else
			{
				StackPop(&st);
				s++;
			}
		}
	}

	bool ret = st.top == 0 ? true : false;
	StackDestroy(&st);
	return ret;
}

void test2()
{
	char arr[] = { '(','(','(','(','(','(','(','(','(','\0'};
	bool ret = isValid(arr);
	printf("%d\n", ret);
}

int main()
{
	//test();

	//test1();

	test2();
	return 0;
}