#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <stdbool.h>

typedef int STDataType;

typedef struct Stack
{
	STDataType* data;
	int top;			//栈顶
	int capacity;		//容量
}Stack;

//初始化
void StackInit(Stack* p);

//销毁栈
void StackDestroy(Stack* p);

//入栈
void StackPush(Stack* p, STDataType x);

//打印
void StackPrint(Stack* p);

//出栈
void StackPop(Stack* p);

//获取栈顶元素
STDataType StackTop(Stack* p);

//获取有效元素个数
int StackSize(Stack* p);

//检查栈是否为空
bool StackEmpty(Stack* p);
