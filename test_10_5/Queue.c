#define _CRT_SECURE_NO_WARNINGS 1

#include "Queue.h"

void QueueInit(Queue* p)
{
	assert(p);

	p->head = p->tail = NULL;
}

void QueueDestroy(Queue* p)
{
	assert(p);

	QListNode* cur = p->head;
	while (cur)
	{
		QListNode* next = cur->next;
		free(cur);
		cur = next;
	}

	p->head = p->tail = NULL;
}

void QueuePush(Queue* p, QDataType x)
{
	assert(p);

	QListNode* newnode = (QListNode*)malloc(sizeof(QListNode));

	if (newnode == NULL)
	{
		perror("malloc fail\n");
		exit(-1);
	}
	newnode->data = x;
	newnode->next = NULL;
	if (p->head == NULL)
	{
		p->head = p->tail = newnode;
	}
	else
	{
		p->tail->next = newnode;
		p->tail = newnode;
	}
}

void QueuePrint(Queue* p)
{
	assert(p);

	while (p->head != NULL)
	{
		printf("%d ", p->head->data);
		p->head = p->head->next;
	}
}

void QueuePop(Queue* p)
{
	assert(p);
	QListNode* next = p->head->next;
	free(p->head);
	p->head = next;
}


QDataType QueueFront(Queue* p)
{
	assert(p);
	return p->head->data;
}

QDataType QueueBack(Queue* p)
{
	assert(p);
	return p->tail->data;
}

int QueueSize(Queue* p)
{
	assert(p);
	int count = 0;
	QListNode* NewHead = p->head;
	while (NewHead)
	{
		count++;
		NewHead = NewHead->next;
	}
	return count;
}

bool QueueEmpty(Queue* p)
{
	if (p->head == NULL)
		return false;
	else
		return true;
}