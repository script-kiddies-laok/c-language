#define _CRT_SECURE_NO_WARNINGS 1

#include "Heap.h"


void Swap(int* a, int* b)
{
	int tmp = *a;
	*a = *b;
	*b = tmp;
}

void AdjustDown(int* a, int parent, int size)
{
	int child = 2 * parent + 1;
	while (child < size)
	{
		if (child + 1 < size && a[child] < a[child + 1])
		{
			child++;			//选出孩子中小的那个				//把有两个小堆的树变成小堆树
		}
		if (a[parent] < a[child])
		{
			Swap(&a[parent], &a[child]);
			parent = child;
			child = 2 * parent + 1;
		}
		else
		{
			break;
		}
	}
}


void HeapCreate(Heap* hp, HPDataType* a, int n)
{
	assert(hp);
	hp->a = (HPDataType*)malloc(sizeof(HPDataType) * n);
	if (hp->a == NULL)
	{
		perror("HeapCreate malloc fail");
		exit(-1);
	}

	hp->size = hp->capacity = n;
	memcpy(hp->a, a, sizeof(HPDataType)*n);

	for (int i = (n - 1 - 1) / 2; i >= 0; i--)
	{
		AdjustDown(hp->a, i, n);
	}
}

void HeapDestroy(Heap* hp)
{
	assert(hp);
	free(hp->a);
	hp->a = NULL;
	hp->size = hp->capacity = 0;
}

void AdjustUp(int* a, int child)
{
	int parent = (child - 1) / 2;
	while (child > 0)
	{
		if (a[parent] > a[child])
		{
			Swap(&a[child], &a[parent]);
			child = parent;
			parent = (child - 1) / 2;
		}
		else
		{
			break;
		}
	}
}

void HeapPush(Heap* hp, HPDataType x)
{
	assert(hp);
	if (hp->capacity == hp->size)
	{
		hp->capacity *= 2;
		HPDataType* tmp = (HPDataType*)realloc(hp->a, sizeof(HPDataType) * hp->capacity);
		if (tmp == NULL)
		{
			perror("HeapPush realloc fail");
			exit(-1);
		}
		hp->a = tmp;
	
	}
	hp->a[hp->size] = x;
	hp->size++;
	AdjustUp(hp->a, hp->size - 1);			//向上调整算法
}


void HeapPop(Heap* hp)
{
	assert(hp);
	Swap(&hp->a[0], &hp->a[hp->size-1]);
	hp->size--;
	//AdjustDown(hp->a, 0,hp->size);
	HeapSoft(hp);
}

HPDataType HeapTop(Heap* hp)
{
	assert(hp);
	assert(hp->size > 0);
	return hp->a[0];
}

int HeapSize(Heap* hp)
{
	assert(hp);
	return hp->size;
}

int HeapEmpty(Heap* hp)
{
	assert(hp);
	return hp->size;
}

void HeapSoft(Heap* hp)
{
	assert(hp);
	for (int i = (hp->size - 1 - 1) / 2; i >= 0; i--)
	{
		AdjustDown(hp->a, i, hp->size);
	}

	int end = hp->size - 1;
	while (end > 0)
	{
		Swap(&hp->a[0], &hp->a[end]);
		AdjustDown(hp->a, 0, end);
		end--;
	}
}