#define _CRT_SECURE_NO_WARNINGS 1

#include "Heap.h"


void test()
{
	Heap hp;
	int a[]= { 9,5,3,8,7,6 };
	HeapCreate(&hp, a, 6);
	HeapPush(&hp, 1);
	HeapPop(&hp);
	HeapSoft(&hp);
	HeapPop(&hp);

	for (int i = 0; i < hp.size; i++)
	{
		printf("%d ", hp.a[i]);
	}
	printf("\n");

	//int tmp = HeapTop(&hp);
	//printf("%d\n", tmp);
	
}

typedef char BTDataType;
typedef struct BinaryTreeNode
{
	struct BinaryTreeNode* leftChild;
	struct BinaryTreeNode* rightChild;
	BTDataType data;
}BTNode;

BTNode* CreateTreeNode(BTDataType x)
{
	BTNode* NewNode = (BTNode*)malloc(sizeof(BTNode));
	if (NewNode == NULL)
	{
		perror("CreateTreeNode malloc fail");
		exit(-1);
	}
	NewNode->data = x;
	NewNode->leftChild = NULL;
	NewNode->rightChild = NULL;
	return NewNode;
}

void PrevOrder(BTNode* root)
{
	if (root == NULL)
	{
		printf("NULL ");
		return;
	}

	printf("%c ", root->data);
	PrevOrder(root->leftChild);
	PrevOrder(root->rightChild);
}

void InOrder(BTNode* root)
{
	if (root == NULL)
	{
		printf("NULL ");
		return;
	}
	InOrder(root->leftChild);
	printf("%c ", root->data);
	InOrder(root->rightChild);
}

void PostOrder(BTNode* root)
{
	if (root == NULL)
	{
		printf("NULL ");
		return;
	}

	PostOrder(root->leftChild);
	PostOrder(root->rightChild);
	printf("%c ", root->data);

}

int BinaryTreeSize(BTNode* root)
{
	return root == 0 ? 0 : BinaryTreeSize(root->leftChild) + BinaryTreeSize(root->rightChild) + 1;
}

int TreeSize(BTNode* root)
{
	if (root == NULL)
	{
		return 0;
	}
	if (root->leftChild == NULL && root->rightChild == NULL)
	{
		return 1;
	}
	else
	{
		return TreeSize(root->leftChild) + TreeSize(root->rightChild);
	}
}

int TreeKLevelSize(BTNode* root,int k)
{
	
}

int main()
{
	BTNode* A = CreateTreeNode('A');
	BTNode* B = CreateTreeNode('B');
	BTNode* C = CreateTreeNode('C');
	BTNode* D = CreateTreeNode('D');
	BTNode* E = CreateTreeNode('E');
	BTNode* F = CreateTreeNode('F');

	A->leftChild = B;
	A->rightChild = C;
	B->leftChild = D;
	B->rightChild = E;
	C->leftChild = F;

	PrevOrder(A);
	printf("\n");

	InOrder(A);
	printf("\n");

	PostOrder(A);
	printf("\n");

	int size = BinaryTreeSize(A);
	printf("size:%d\n",size);

	int treeSize = TreeSize(A);
	printf("treeSize:%d\n", treeSize);

	int kSize = TreeKLevelSize(A,2);
	printf("kSize:%d\n", kSize);
	return 0;
}