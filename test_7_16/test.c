#define _CRT_SECURE_NO_WARNINGS 1

#include "game.h"

void menu()
{
	printf("********************\n");
	printf("*****  1.play  *****\n");
	printf("*****  0.exit  *****\n");
	printf("********************\n");
}

void game()
{
	char mine[ROWS][COLS];
	char show[ROWS][COLS];

	//初始化数组
	InitBoard(mine, ROWS, COLS, '0');
	InitBoard(show, ROWS, COLS, '*');

	//打印数组
	//PrintBoard(mine, ROW, COL);
	PrintBoard(show, ROW, COL);

	//埋雷
	MineBoard(mine);

	//PrintBoard(mine, ROW, COL);

	//排查雷
	SearchMine(mine, show);

	PrintBoard(show, ROW, COL);
	

}

int main()
{
	srand((unsigned)time(NULL));
	int input = 0;
	do
	{
		menu();
		printf("请选择:>");
		scanf("%d", &input);
		switch (input)
		{
		case 1:
			game();
			break;
		case 0:
			printf("退出游戏\n");
			break;
		default:
			printf("输入错误，请重新输入!\n");
			break;
		}
	} while (input);
	return 0;
}