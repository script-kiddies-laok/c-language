#define _CRT_SECURE_NO_WARNINGS 1

#include "game.h"

void InitBoard(char board[ROWS][COLS], int row, int col, char ch)
{
	int i = 0;
	for (i = 0; i < row; i++)
	{
		int j = 0;
		for (j = 0; j < col; j++)
		{
			board[i][j] = ch;
		}
	}
}

void PrintBoard(char board[ROWS][COLS], int row, int col)
{
	int i = 0;
	printf("-------扫雷游戏---------\n");
	for (i = 0; i <= col; i++)
	{
		printf("%d ", i);
	}
	printf("\n");
	for (i = 1; i <= row; i++)
	{
		int j = 1;
		printf("%d ", i);
		for (j = 1; j <= col; j++)
		{
			printf("%c ", board[i][j]);
		}
		printf("\n");
	}
	printf("-------扫雷游戏---------\n");

}


void MineBoard(char mine[ROWS][COLS])
{
	int count = EASY_COUNT;
	while (count)
	{
		int x = rand() % ROW + 1;
		int y = rand() % COL + 1;

		if (mine[x][y] == '0')
		{
			mine[x][y] = '1';
			count--;
		}
	}
}


int AroundMine(char mine[ROWS][COLS], int x, int y)
{
	return 	mine[x - 1][y] +
			mine[x + 1][y] +
			mine[x - 1][y - 1] +
			mine[x + 1][y + 1] +
			mine[x - 1][y + 1] +
			mine[x + 1][y - 1] +
			mine[x][y - 1] +
			mine[x][y + 1] - 8 * '0';

}

//递归
void open(char mine[ROWS][COLS], char show[ROWS][COLS], int x, int y,int row,int col)
{

	int ret = AroundMine(mine, x, y);
	if (ret == 0)
	{
		show[x][y] = ' ';
		if (( x - 1 >0 && y - 1 > 0) && show[x - 1][y - 1] == '*')
		{
			open(mine, show, x - 1, y - 1,row,col);
		}
		if ((x - 1 > 0 && y > 0) && show[x - 1][y] == '*')
		{
			open(mine, show, x - 1, y , row, col);
		}
		if ((x - 1 > 0 && y + 1 <= col) && show[x - 1][y + 1] == '*')
		{
			open(mine, show, x - 1, y + 1, row, col);
		}
		if ((x - 1 > 0 && y - 1 > 0) && show[x][y - 1] == '*')
		{
			open(mine, show, x - 1, y - 1, row, col);
		}
		if ((x > 0 && y + 1 <= col) && show[x ][y + 1] == '*')
		{
			open(mine, show, x , y + 1, row, col);
		}
		
		if( (x + 1 <=row && y - 1 > 0) && show[x + 1][y - 1] == '*')
		{
			open(mine, show, x + 1, y - 1, row, col);
		}
		if ((x + 1 <= row && y > 0) && show[x + 1][y] == '*')
		{
			open(mine, show, x + 1, y , row, col);
		}
		if (( x + 1 <= row &&  y + 1 <= col) && show[x + 1][y + 1] == '*')
		{
			open(mine, show, x + 1, y + 1, row, col);
		}
	}

	else
	{
		show[x][y] = ret + '0';
	}
	
}

int IsWin(char show[ROWS][COLS], int row, int col )		//找到被改变的格子
{
	int count = 0;
	for (int i = 1; i <= row; i++)
	{
		for (int j = 1; j <= col; j++)
		{
			if (show[i][j] != '*')
			{
				count++;
			}
		}
	}
	if (count == ROW * COL - EASY_COUNT)
	{
		return 0;
	}
	return count;
}

void SearchMine(char mine[ROWS][COLS], char show[ROWS][COLS])
{
	int number = ROW * COL - EASY_COUNT;
	while (number)
	{
		int x = 0;
		int y = 0;
		printf("请输入你要排查的坐标:");
		scanf("%d %d", &x, &y);
		if ((x >= 1 && x <= 9) && (y >= 1 && y <= 9))
		{
			if (mine[x][y] == '1')
			{
				
				break;
			}
			else
			{
				int count = AroundMine(mine, x, y);
				open(mine, show, x, y, ROW, COL);	//展开

				PrintBoard(show, ROW, COL);
				
				number = IsWin(show, ROW, COL);
			}
		}
		else
		{
			printf("坐标非法，请重新输入\n");
		}
	}
	if (number > 0)
	{
		printf("你被炸死了\n");
		PrintBoard(mine, ROW, COL);

	}
	else
	{
		printf("恭喜你，你赢了\n");
	}
}
