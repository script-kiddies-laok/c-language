#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <windows.h>
//int main()
//{
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	int k;
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	int left = 0;
//	int right = sz - 1;
//	int mid;
//
//	scanf("%d", &k);
//
//	while (left <= right)
//	{
//		mid = left + (right - left) / 2;
//		if (k > arr[mid])
//			left = mid + 1;
//		else if (k < arr[mid])
//			right = mid - 1;
//		else
//		{
//			printf("找到了，下标是%d\n", mid);
//			break;
//		}
//	}
//	if (left > right)
//		printf("找不到\n");
//
//	return 0;
//}

//#include <string.h>
//#include <windows.h>
//#include <stdlib.h>
//
//int main()
//{
//	char arr1[] = "welcome to bie!!!";
//	char arr2[] = "#################";
//	int sz = strlen(arr1);
//	int left = 0;
//	int right = sz - 1;
//	while (left <= right)
//	{
//		arr2[left] = arr1[left];
//		arr2[right] = arr1[right];
//		printf("%s\n", arr2);
//		//休息1秒
//		Sleep(1000); //单位ms
//		system("cls");	//系统命令	cls清空屏幕
//		left++;
//		right--;
//	}
//	printf("%s\n", arr2);
//	return 0;
//}

//#include <string.h>
//
//int main()
//{
//	int i = 0;
//	char password[20] = { 0 };
//	for (i = 0; i < 3; i++)
//	{
//		printf("请输入密码:");
//		scanf("%s", password);
//		if (strcmp(password, "123456") == 0) //等号不能用来比较字符串
//		{
//			printf("登录成功\n");
//			break;
//		}
//		else
//			printf("密码错误\n");
//	}
//	if (i == 3)
//		printf("三次密码错误，退出程序\n");
//	return 0;
//}

//int main()
//{
//	int a, b, c;
//	scanf("%d%d%d", &a, &b, &c);
//
//	int tmp;
//	if (a < b)
//	{
//		tmp = a;
//		a = b;
//		b = tmp;
//	}
//	if (a < c)
//	{
//		tmp = a;
//		a = c;
//		c = tmp;
//	}
//	if (b < c)
//	{
//		tmp = b;
//		b = c;
//		c = tmp;
//	}
//	printf("%d %d %d", a, b, c);
//	return 0;
//}

//int max(int x, int y)
//{
//	if (x < y)
//	{
//		int tmp = x;
//		x = y;
//		y = tmp;
//	}
//	return  ((x%y == 0) ? y : max(y, x % y));
//}
//
//int main()
//{
//	int x, y;
//	scanf("%d %d", &x, &y);
//
//	int ret = max(x, y);
//	printf("%d\n", ret);
//	return 0;
//}

//#include <math.h>
//
//int main()
//{
//	int i ,j;
//	for ( i = 1; i < 10000; i+=2)
//	{
//		for ( j = 2; j <= i/2; j++)
//		{
//			if (i % j == 0)
//				break;
//		}
//		if (j > i/2)
//			printf("%d ", i);
//	}
//
//	return 0;
//}

//int main()
//{
//	printf("hello bit\n");
//	goto again;
//	printf("你好\n");
//again:
//	printf("hehe\n");
//	return 0;
//}

//int main()
//{
//	char arr1[] = "bit";
//	char arr2[20] = { 0 };
//	strcpy(arr2, arr1);
//	printf("%s\n", arr2);
//	return 0;
//}

//int main()
//{
//	char arr[] = "hello world";
//	memset(arr, '*', 5);
//	printf("%s\n", arr);
//	return 0;
//}

//void swap(int* a, int* b)
//{
//	int tmp = *a;
//	*a = *b;
//	*b = tmp;
//}
//
//int main()
//{
//	int a = 10;
//	int b = 20;
//	swap(&a, &b);
//	printf("%d %d\n", a, b);
//	return 0;
//}

//int is_prime(int n)
//{
//	int i;
//	for ( i = 2; i <= sqrt(n); i++)
//	{
//		if (n % i == 0)
//			break;
//	}
//	if (i >= sqrt(n))
//		return 1;
//	else
//		return 0;
//}
//
//int main()
//{
//	int i;
//	for (i = 101; i <= 200; i+=2)
//	{
//		if (is_prime(i) == 1)
//			printf("%d ", i);
//	}
//	return 0;
//}

//void Add(int* p)
//{
//	(*p)++;
//}
//
//int main()
//{
//	int num = 0;
//	Add(&num);
//	printf("%d\n", num);
//	Add(&num);
//	printf("%d\n", num);
//	Add(&num);
//	printf("%d\n", num);
//	return 0;
//}

//int main()
//{
//	printf("%d", printf("%d", printf("%d", 43)));
//	//printf返回的是字符的个数
//	return 0;
//}

//void print(unsigned int num)
//{
//	/*if (num > 0)
//	{
//		printf("%d ", num % 10);
//		print(num / 10);
//	}*/
//	if (num > 9)
//		print(num / 10);
//	printf("%d ", num%10);
//}
//int main()
//{
//	unsigned int num = 0;
//	scanf("%d", &num);
//	print(num);
//	return 0;
//}


int str_len( char* str)
{
	if (*str != '\0')
		return 1 + str_len(str + 1);
	else
		return 0;
}

int main()
{
	char str[] = "hello bit";
	int ret = str_len(str);

	printf("%d\n", ret);
	return 0;
}