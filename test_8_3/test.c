#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>
#include <string.h>

//int main()
//{
//	//整型
//	char				//存的是ascii值
//	signed char			//有符号的char类型
//	unsigned char		//无符号的char类型
//	short
//	signed short
//	unsigned short		
//	int
//	signed int
//	unsigned int
//	long
//	signed long
//	unsigned long
//	long long
//	signed long long
//	unsigned long long
//	float
//	double
//	long double
//	return 0;
//}


//int main()
//{
//	int a = 20;
//	//00000000000000000000000000010100
//	//0x00000014
//	//14 00 00 00
//	int b = -10;
//	//整数在内存中是以补码存放的
//	//对于正整数来说，原码反码补码都相同
//	//对于负整数来说，补码是要计算的
//	//int类型是4个字节
//	//32个比特位
//	//10000000000000000000000000001010	原码
//	//11111111111111111111111111110101	反码(将原码的符号位不变，其他位按位取反)
//	//11111111111111111111111111110110	补码(反码加一)
//	//用16进制表示就是
//	//f6 ff ff ff
//	return 0;
//}

//
//int main()
//{
//	int a = 0x11223344;
//	char c = (char)a;	//这种方法是错误的，数值在char的范围内是不会发生改变的
//	if (c == 44)
//	{
//		printf("小端存储\n");
//	}
//	else
//		printf("大端存储\n");
//	return 0;
//}


//int main()
//{
//	int a = 0x11223344;
//	//char* c = (char*)&a;	//截取一个字节
//	if (*(char*)&a == 0x44)
//	{
//		printf("小端存储\n");
//	}
//	else
//		printf("大端存储\n");
//	return 0;
//}
//
//int check_sys()
//{
//	int a = 1;
//	return *(char*)&a;
//}
//
//int main()
//{
//	int ret = check_sys();
//	if (ret == 1)
//	{
//		printf("小端存储\n");
//	}
//	else
//		printf("大端存储\n");
//	return 0;
//}

//int main()
//{
//	char a = -1;
//	signed char b = -1;
//	unsigned char c = -1;
//	printf("a=%d b=%d c=%d\n", a, b, c);
//	return 0;
//}

//int main()
//{
//	char a = -128;
//	//10000000000000000000000010000000		-128的原码
//	//11111111111111111111111101111111		反码
//	//10000000000000000000000010000000		补码
//	//char类型只能存放一个字节的
//	//10000000
//	//char是signed char 有符号的数，整型提升补1
//	//11111111111111111111111110000000		这是补码
//	//无符号数的原码，反码，补码都相同，所以直接打印出来
//	//上面的数加上128就是2的32次方，结果就是2的32次方减去128
//	printf("%u\n", a);		//%u打印无符号整型
//	return 0;
//}

//int main()
//{
//	char a = 128;
//	//00000000000000000000000010000000		128的补码
//	//10000000								
//	//11111111111111111111111110000000		有符号数
//	printf("%u\n", a);
//	return 0;
//}


//int main()
//{
//	int i = -20;
//	//100000000000000000000000010100
//	//111111111111111111111111101011
//	//111111111111111111111111101100	-20的补码
//	// 
//	//000000000000000000000000001010	无符号数的10
//	// 
//	//111111111111111111111111110110	相加的结果(补码)
//	// 
//	//111111111111111111111111110101
//	//100000000000000000000000001010	-10
//	unsigned int j = 10;
//	printf("%d\n", i + j);
//
//	return 0;
//}


//int main()
//{
//	unsigned int i = 0;
//	for (i = 9; i >= 0; i--)
//	{
//		printf("%u\n", i);
//	}
//	return 0;
//}

//int main()
//{
//	char arr[1000] = { 0 };
//	int i = 0;
//	for (i = 0; i < 1000; i++)
//	{
//		arr[i] = -1 - i;
//	}
//	
//	printf("%d\n", strlen(arr));
//	return 0;
//}

//unsigned char i = 0;
//
//int main()
//{
//	for (i = 0; i <= 255; i++)
//	{
//		printf("hello world\n");
//	}
//	return 0;
//}


//int main()
//{
//	int n = 9;
//	float* p = (float*)&n;
//	printf("n的值为:%d\n", n);
//	printf("*p的值为:%f\n", *p);
//
//	*p = 9.0;
//	printf("n的值为:%d\n", n);
//	printf("*p的值为:%f\n", *p);
//	return 0;
//}


//int main()
//{
//	//5.5用二进制表示的是101.1
//	//小数部分也是用权重计算的，这个小数点后面的1，实际上是1*2^-1就是0.5
//	//01000000101100000000000000000000000
//	return 0;
//}

//int main()
//{
//	float f = 5.5f;			//5.5后面不加f默认表示double类型，加f表示float类型
//	return 0;
//}


//int main()
//{
//	int n = 9;
//	float* p = (float*)&n;
//	printf("n的值为:%d\n", n);
//	printf("*p的值为:%f\n", *p);
//
//	*p = 9.0;
//	printf("n的值为:%d\n", n);
//	printf("*p的值为:%f\n", *p);
//	return 0;
//}

//void reverse(char* str)
//{
//	int len = strlen(str);
//	char* left = str;
//	char* right = str + len - 1;
//	
//	while (left<right)			//指针是可以比较的，这里是低地址小于高地址
//	{
//		char tmp = *left;
//		*left = *right;
//		*right = tmp;
//		left++;
//		right--;
//	}
//}
//
//int main()
//{
//	char arr[100] = { 0 };
//	//scanf("%s", arr);			//scanf会吃掉用户输入的空格
//	//这里用gets函数
//	gets(arr);					//读取一行，由空格也读
//	reverse(arr);
//	printf("%s\n", arr);
//	return 0;
//}

//int main()
//{
//	char c = 'w';
//	char* ch = &c;
//	printf("%c\n", *ch);
//	return 0;
//}

//   *
//  ***
// *****
//*******
// *****
//  ***
//   *

//int main()
//{
//	int line = 0;
//	scanf("%d", &line);
//	int i = 0;
//	//上部分		假设4行 line=4
//	for (i = 1; i <= line; i++)
//	{
//		//打印空格
//		int j = 0;
//		for (j = 0; j < line - i; j++)
//		{
//			printf(" ");
//		}
//		//打印*
//		for (j = 0; j < 2 * i - 1; j++)
//		{
//			printf("*");
//		}
//		printf("\n");
//	}
//	//下部分
//	for (i = 1; i <= line - 1; i++)
//	{
//		//打印空格
//		int j = 0;
//		for (j = 1; j <= i; j++)
//		{
//			printf(" ");
//		}
//		//打印*
//		for (j = 0; j < 2*(line-i)-1; j++)
//		{
//			printf("*");
//		}
//		printf("\n");
//	}
//	return 0;
//}


int main()
{
	//假设只有4行
	//int n = 4;
	int line = 0;
	scanf("%d", &line);
	//printf("\n");
	int arr[30][30] = { 0 };
	int i = 0;
	
	//对每行的外侧都赋值1
	for (i = 1; i <= line; i++)
	{
		arr[i][1] = arr[i][i] = 1;
	}
	//对里面赋值
	for (i = 2; i <= line; i++)
	{
		int j = 0;
		for (j = 2; j <= i; j++)
		{
			arr[i][j] = arr[i - 1][j] + arr[i - 1][j - 1];
		}
	}

	//打印
	for (i = 1; i <= line; i++)
	{
		int j = 0;
		//打印空格
		for (j = 0; j < line - i; j++)
		{
			printf(" ");
		}
		for (j = 1; j <= i; j++)
		{
			printf("%d ", arr[i][j]);
		}
		printf("\n");
	}
	return 0;
}