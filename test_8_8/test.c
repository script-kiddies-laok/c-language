#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>

//int main()
//{
//	int a[3][4] = { 0 };
//	printf("%d\n", sizeof(a));				//48
//	printf("%d\n", sizeof(a[0][0]));		//4
//	printf("%d\n", sizeof(a[0]));			//16
//	printf("%d\n", sizeof(a[0] + 1));		//4
//	printf("%d\n", sizeof(*(a[0] + 1)));	//4
//	printf("%d\n", sizeof(a + 1));			//4			//二维数组数组名是首元素地址，第一行的地址
//	printf("%d\n", sizeof(*(a + 1)));		//16		//拿到的是第二行，解引用是整个第二行
//	printf("%d\n", sizeof(&a[0] + 1));		//4			//
//	printf("%d\n", sizeof(*(&a[0] + 1)));	//16
//	printf("%d\n", sizeof(*a));				//4			//a是第一行的地址
//	printf("%d\n", sizeof(a[3]));			//16
//	return 0;
//}


//int main()
//{
//	int arr[] = { 1,2,3,4,5 };
//	int* ptr = (int*)(&arr + 1);
//	printf("%d %d\n", *(arr + 1), *(ptr - 1));
//	return 0;
//}

//struct Test
//{
//	int Num;
//	char* pcName;
//	short sDate;
//	char ch[2];
//	short sBa[4];
//}*p;
//
//int main()
//{
//	printf("%p\n", p + 0x1);					//0x00100014
//	printf("%p\n", (unsigned long)p + 0x1);		//0x00100001
//	printf("%p\n", (unsigned int*)p + 0x1);		//0x00100004
//	return 0;
//}


//int main()
//{
//	int a[4] = { 1,2,3,4 };					//01 00 00 00 02 00 00 00 03 00 00 00 04 00 00 00
//	int* ptr1 = (int*)(&a + 1);
//	int* ptr2 = (int*)((int)a + 1);			//小端存储，a是首元素的地址，强制类型成整型，+1就是跳过一个字节
//											//原来a是01 00 00 00 ，现在变成00 00 00 02 ，又是小端存储，所以拿出来是2000000
//	printf("%x,%x", ptr1[-1], *ptr2);		//4,2000000		
//	return 0;
//}

//int main()
//{
//	int a[3][2] = { (0,1),(2,3),(4,5) };
//	int* p = a[0];
//	printf("%d\n", p[0]);
//	return 0;
//}


//int main()
//{
//	int a[5][5];
//	int(*p)[4] = a;
//	printf("%p %d\n", &p[4][2] - &a[4][2], &p[4][2] - &a[4][2]);
//	//FFFFFFFC -4
//	return 0;
//}


//int main()
//{
//	char* a[] = { "work","at","alibaba" };
//	char** p = a;
//	p++;
//	printf("%s\n", *p);
//	return 0;
//}


//int main()
//{
//	char* c[] = { "ENTER","NEW","POINT","FIRST" };
//	char** cp[] = { c + 3,c + 2,c + 1,c };
//	char*** cpp = cp;
//	printf("%s\n", **++cpp);					//POINT
//	printf("%s\n", *-- * ++cpp + 3);			//*++cpp是指向cp第三个元素的，在--是把c+1的值改成c，在*，就是ENTRER，+3就是ER
//	printf("%s\n", *cpp[-2] + 3);				//现在cpp指向的是cp+2，cpp[-2]是*(cpp-2),也就是cp，*，就是FIRST，+3就是ST
//	printf("%s\n", cpp[-1][-1] + 1);			//cpp[-1]=*(cpp-1),*(cpp-1)[-1]=*(*(cpp-1)-1),*(cpp-1)=c+2,*(c+2-1)=*(c+1),NEW,+1就是EW
//	return 0;
//}

//#include <stdio.h>
//#include <string.h>
//
//int main()
//{
//	char* p1 = "abc";
//	char* p2 = "abcdef";
//	printf("%d\n", strlen(p1) - strlen(p2));
//	return 0;
//}
//#include <stdio.h>
//#include <assert.h>
//
////size_t my_strlen(const char* str)
////{
////	assert(str);
////	size_t count = 0;
////	while (*str++)
////	{
////		count++;
////	}
////	return count;
////}
//
////size_t my_strlen(char* str)
////{
////	if (*str++ != '\0')
////		return 1 + my_strlen(str);
////	else
////		return 0;
////}
//
//size_t my_strlen(char* str)
//{
//	assert(str);
//	char* start = str;
//	while (*str != '\0')
//	{
//		str++;
//	}
//	return str - start;
//}
//
//int main()
//{
//	char arr[] = "abcdef";
//	printf("%d\n", my_strlen(arr));
//	return 0;
//}

//#include <stdio.h>
//#include <string.h>
//#include <assert.h>
//
//char* my_strcpy(char* dest, const char* src)
//{
//	assert(dest && src);				//断言，dest/src不能为空指针
//	char* ret = dest;					
//	while (*dest++ = *src++)			//什么时候终止呢
//	{									//当*src等于'\0'时，'\0'的ascii码值为0，表达式为0，条件终止
//		;
//	}
//	//*dest = *src;		
//	return ret;
//}
////strcpy - 返回类型是char*
//int main()
//{
//	char arr1[20] = {0};
//	char arr2[] = "hello world";
//
//	printf("%s\n", my_strcpy(arr1, arr2));				//打印字符串，直接传地址
//	return 0;
//}

////char* a; main(int t, int _, char* a) { return!0 < t ? t < 3 ? main(-79, -13, a + main(-87, 1 - _, main(-86, 0, a + 1) + a)) : 1, t < _ ? main(t + 1, _, a) : 3, main(-94, -27 + t, a) && t == 2 ? _ < 13 ? main(2, _ + 1, "%s %d %d\n") : 9 : 16 : t < 0 ? t < -72 ? main(_, t, "@n'+,#'/*{}w+/w#cdnr/+,{}r/*de}+,/*{*+,/w{%+,/w#q#n+,/#{l+,/n{n+,/+#n+,/#;#q#n+,/+k#;*+,/'r :'d*'3,}{w+K w'K:'+}e#';dq#'l q#'+d'K#!/+k#;q#'r}eKK#}w'r}eKK{nl]'/#;#q#n'){)#}w'){){nl]'/+#n';d}rw' i;# ){nl]!/n{n#'; r{#w'r nc{nl]'/#{l,+'K {rw' iK{;[{nl]'/w#q#n'wk nw' iwk{KK{nl]!/w{%'l##w#' i; :{nl]'/*{q#'ld;r'}{nlwb!/*de}'c ;;{nl'-{}rw]'/+,}##'*}#nc,',#nw]'/+kd'+e}+;#'rdq#w! nr'/ ') }+}{rl#'{n' ')# }'+}##(!!/") : t < -50 ? _ == *a ? putchar(31[a]) : main(-65, _, a + 1) : main((*a == '/') + t, _, a + 1) : 0 < t ? main(2, 2, "%s") : *a == '/' || main(0, main(-61, *a, "!ek;dc i@bK'(q)-[w]*%n+r3#l,{}:\nuwloca-O;m .vpbks,fxntdCeghiry"), a + 1); }

//#include <string.h>
//#include <stdio.h>
//int main()
//{
//	char arr1[20] = "hello \0xxxxxx";
//	char arr2[] = "world";
//	printf("%s\n", strcat(arr1, arr2));
//	return 0;
//}
//#include <assert.h>

//char* my_strcat(char* dest, const char* src)
//{
//	assert(dest && src);
//
//	char* ret = dest;
//	while (*dest)				//找到'\0'
//	{
//		dest++;
//	}
//	while (*dest++ = *src++)	//拷贝
//	{
//		;
//	}
//
//	return ret;
//}
//
//int main()
//{
//	char arr1[20] = "hello ";
//	char arr2[] = "world";
//	printf("%s\n",my_strcat(arr1, arr2));
//	return 0;
//}

//int main()
//{
//	char arr1[20] = "hello ";
//	char arr2[] = "world";
//
//	char* dest = arr1;
//	char* ret = dest;
//
//	char* src = arr2;
//	while (*dest++)
//	{
//		;
//	}
//	while (*dest++ = *src++)
//	{
//		;
//	}
//	printf("%s\n", ret);
//	return 0;
//}


int main()
{

	return 0;
}