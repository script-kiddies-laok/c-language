#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>
#include <string.h>
//int main()
//{
//	int a = 0;
//	printf("%d", ~a);
//	return 0;
//}

//int main()
//{
//	//int a = 13;
//	//a=a | (1<<1);
//
//	int a = 15;
//	a = a & (~(1 << 1));
//	//00000000000000000000000000001111  15
//	//11111111111111111111111111111101
//	//00000000000000000000000000000010  取反
//	printf("%d\n", a);
//	return 0;
//}
//int i;
//int main()
//{
//	i--;
//	if (i > sizeof(i))
//	{
//		printf(">\n");
//	}
//	else
//		printf("<\n");
//	return 0;
//}

//void test1(int arr[])
//	{
//		printf("%d\n", sizeof(arr));
//	}
//
//void test2(char ch[])
//{
//	printf("%d\n", sizeof(ch));
//}
////指针的类型都是4个字节，存放地址
//int main()
//{
//	int arr[10] = { 0 };
//	char ch[10] = { 0 };
//	printf("%d\n", sizeof(arr));
//	printf("%d\n", sizeof(ch));
//	test1(arr);
//	test2(ch);
//	return 0;
//}

//int main()
//{
//	int a = 3;
//	int b = 0;
//	int c = a && b;
// 左边为假，右边不计算
//	printf("%d\n", c);
//	return 0;
//}

//int main()
//{
//	int a = 0;
//	int b = 1;
//	int c = a || b;
//	printf("%d\n", c);
//	return 0;
//}
//
//struct Stu
//{
//	char name[20];
//	int age;
//	char sex[5];
//};
//
//int main()
//{
//	struct Stu s = { "zhangsan",18,"nan"};
//	struct Stu* p=&s;
//	printf("%s\n", s.name);
//	printf("%d\n", p->age);
//	return 0;
//}

//int main()
//{
//	char a = 3;
//	char b = 127;
//	char c = a + b;
//	printf("%d\n", c);
//	return 0;
//}

//int x(int a)
//{
//	int count = 0;
//	for (int i = 1; i <= 31; i++)
//	{
//		if ((a & 1) == 1)
//			count++;
//		a >>= 1;
//	}
//	return count;
//}
//
//int main()
//{
//	int a = 15;
//	int set = x(a);
//	printf("%d\n", set);
//	return 0;
//}


//int fun()
//{
//	static int count = 1;
//	return ++count;
//}
//int main()
//{
//	int answer;
//	answer = fun() - fun() * fun();
//	printf("%d\n", answer);
//	return 0;
//}

//int main()
//{
//	int a = 15;
//	int x = 0;
//	int y = 0;
//	for (int i = 1; i <= 32; i++)
//	{
//		if (a % 2 == 1)
//			x++;
//		else if (a % 2 == 0)
//			y++;
//		a >>= 1;
//	}
//	printf("%d %d\n", x, y);
//	return 0;
//}

//int main()
//{
//	int x = 0, y = 0;
//	scanf("%d %d", &x, &y);
//	int k = x ^ y;
//	int count = 0;
//	for (int i = 1; i <= 32; i++)
//	{
//		if ((k & 1) == 1)
//			count++;
//		k >>= 1;
//	}
//	printf("%d\n", count);
//	return 0;
//}

//int main()
//{
//	int a = 3;
//	int x = 0;
//	int y = 0;
//	for (int i = 1; i <= 32; i++)
//	{
//		if ((a & 1) == 1) {
//			printf("1 ");
//			x++;
//		}
//		else if ((a & 1) == 0) {
//			printf("0 ");
//			y++;
//		}
//		a >>= 1;
//	}
//	printf("\n");
//	printf("奇数=%d 偶数=%d\n", x, y);
//	return 0;
//}

//int main()
//{
//	int a = 3;
//	for (int i = 31; i >= 1; i -= 2)
//	{
//		printf("%d ", (a >> i) & 1);
//	}
//	printf("\n");
//	for (int i = 30; i >= 0; i -= 2)
//	{
//		printf("%d ", (a >> i) & 1);
//	}
//	return 0;
//}
//
//#include <stdio.h>
//
//int main()
//{
//    char ch;
//    while (~scanf("%c", &ch))
//    {
//        getchar();
//        ch += 32;
//        putchar(ch);
//    }
//    return 0;
//}

//#include <stdio.h>
//
//int main()
//{
//    int n = 0;
//    while (~scanf("%d", &n))
//    {
//        printf("%d\n", 1 << n);
//    }
//    return 0;
//}

//int my_strlen(char* arr)
//{
//	char* start = arr;
//	while (*arr != '\0')
//	{
//		arr++;
//	}
//	return arr - start;
//}
////求字符串的长度
//int main()
//{
//	char arr[] = "abcdef";
//	int len = my_strlen(arr);
//	printf("%d\n", len);
//	return 0;
//}

//int main()
//{
//	char* p = "abcdef";
//	printf("%s\n", p);
//	char* arr[] = { "abcdef","hello","bit" };
//	for (int i = 0; i < 3; i++)
//	{
//		printf("%s ", arr[i]);
//	}
//	return 0;
//}

//#include <stdio.h>
//int main()
//{
//    int arr[] = { 1,2,3,4,5 };
//    short* p = (short*)arr;
//    int i = 0;
//    for (i = 0; i < 4; i++)
//    {
//        *(p + i) = 0;
//    }
//
//    for (i = 0; i < 5; i++)
//    {
//        printf("%d ", arr[i]);
//    }
//    return 0;
//}

//struct S
//{
//	char arr[100];
//	int num;
//	double d;
//};
//
//void print(struct S * s)
//{
//	printf("%s %d %lf\n", s->arr, s->num, s->d);
//}
//
//int main()
//{
//	struct S s = { "hello",100,3.14 };
//
//	print(&s);
//	return 0;
//}

//void print(int* arr,int sz)
//{
//	for (int i = 0; i < sz; i++)
//	{
//		printf("%d ", *(arr+i));
//	}
//}
//
//int main()
//{
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,0 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	print(arr,sz);
//	return 0;
//}

//void print(char* ch)
//{
//	int left = 0;
//	int len = strlen(ch)-1;
//	int temp;
//	for (int i = 0; i <= len/2;i++)
//	{
//		temp = *(ch + i);
//		*(ch + i) = *(ch + len-i);
//		*(ch + len - i) = temp;
//	}
//	printf("%s\n", ch);
//}
//
//int main()
//{
//	char ch[100] = {0};
//	gets(ch);
//	print(ch);
//	return 0;
//}

#include <math.h>

//int Num(int i, int count)
//{
//	while (i)
//	{
//		i = i / 10;
//		count++;
//	}
//}
//
//void GeW(int i,int n,int* p)
//{
//	for (int j = 0; j < n; j++)
//	{
//		*(p + j) = i % 10;
//		i /= 10;
//		if (i <= 0)
//			break;
//	}
//}

//int Num(int i,int count)
//{
//	while (i)
//	{
//		i = i / 10;
//		count++;
//	}
//	return count;
//}
//
//int main()
//{
//	int sum = 0;
//	int n=0;
//	int arr[100];
//	int* p = arr;
//	for (int i = 1; i <= 100000; i++)
//	{
//		int count = 0;
//		int c = 0;
//		n = Num(i, count);
//		//GeW(i, n, p);
//
//		for (int j = 0; j < n; j++)
//		{
//			*(p + j) = i % 10;
//			i /= 10;
//			if (i <= 0)
//				break;
//		}
//		for (int j = 0; j < n; j++)
//		{
//			sum += pow(*(p + j), n);
//		}
//		if(i==sum)
//			printf("%d ", sum);
//		sum = 0;
//	}
//	return 0;
//}

int main()
{
	int arr[10];
	int* p = arr;
	for (int i = 1; i <= 99999; i++)
	{
		int tmp = i;
		int count = 0;
		int sum = 0;
		while (tmp)
		{
			tmp /= 10;
			count++;
		}
		tmp = i;
		for (int j = 0; j < count; j++)
		{
			*(p + j) = tmp % 10;
			tmp /= 10;
		}
		for (int j = 0; j < count; j++)
		{
			sum += pow(*(p + j), count);
		}
		if(i==sum)
			printf("%d ", sum);
		sum = 0;
	}
	return 0;
}