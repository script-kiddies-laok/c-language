#define _CRT_SECURE_NO_WARNINGS 1

#include "BinaryTree.h"
#include "Queue.h"

BTNode* CreateTreeNode(BTDataType x)
{
	BTNode* NewNode = (BTNode*)malloc(sizeof(BTNode));
	if (NewNode == NULL)
	{
		perror("CreateTreeNode malloc fail");
		return NULL;
	}

	NewNode->data = x;
	NewNode->leftChild = NULL;
	NewNode->rightChild = NULL;
	
	return NewNode;
}

void PrevOrder(BTNode* root)
{
	if (root == NULL)
	{
		printf("NULL ");
		return;
	}

	printf("%c ", root->data);
	PrevOrder(root->leftChild);
	PrevOrder(root->rightChild);
}

void InOrder(BTNode* root)
{
	if (root == NULL)
	{
		printf("NULL ");
		return;
	}

	InOrder(root->leftChild);
	printf("%c ", root->data);
	InOrder(root->rightChild);
}

void PostOrder(BTNode* root)
{
	if (root == NULL)
	{
		printf("NULL ");
		return;
	}

	PostOrder(root->leftChild);
	PostOrder(root->rightChild);
	printf("%c ", root->data);

}

int TreeSize(BTNode* root)
{
	return root == NULL ? 0 : TreeSize(root->leftChild) + TreeSize(root->rightChild) + 1;
}

int TreeLeafSize(BTNode* root)
{
	if (root == NULL)
		return 0;
	if (root->leftChild == NULL && root->rightChild == NULL)
		return 1;

	return TreeLeafSize(root->leftChild) + TreeLeafSize(root->rightChild);
}

int TreeKLevelSize(BTNode* root, int k)
{
	if (root == NULL)
		return 0;
	if (k == 1)
		return 1;

	return TreeKLevelSize(root->leftChild, k - 1) + TreeKLevelSize(root->rightChild, k - 1);
}

BTNode* TreeFind(BTNode* root, BTDataType x)
{
	if (root == NULL)
		return NULL;
	if (root->data == x)
		return root;

	BTNode* left = TreeFind(root->leftChild, x);
	if (left)
		return left;

	BTNode* right = TreeFind(root->rightChild, x);
	if (right)
		return right;

	return NULL;
		
}

void TreeLevelOrder(BTNode* root)
{
	Queue q;
	QueueInit(&q);
	if (root != NULL)
	{
		QueuePushBack(&q, root);
	}

	while (!QueueEmpty(&q))
	{
		BTNode* front = QueueFront(&q);
		QueuePopFront(&q);

		printf("%c ", front->data);
		if (front->leftChild)
		{
			QueuePushBack(&q, front->leftChild);
		}

		if (front->rightChild)
		{
			QueuePushBack(&q, front->rightChild);
		}
	}

	QueueDestroy(&q);
}


bool TreeComplete(BTNode* root)
{
	Queue q;
	QueueInit(&q);

	if (root != NULL)
	{
		QueuePushBack(&q, root);
	}
	while (!QueueEmpty(&q))
	{
		BTNode* front = QueueFront(&q);
		QueuePopFront(&q);

		if (front==NULL)
			break;

		QueuePushBack(&q, front->leftChild);
		QueuePushBack(&q, front->rightChild);

	}

	while (!QueueEmpty(&q))
	{
		BTNode* front = QueueFront(&q);
		QueuePopFront(&q);
		if (front)
			return false;
	}
	QueueDestroy(&q);
	return true;
}

void TreeDestory(BTNode* root)
{
	if (root == NULL)
		return;
	TreeDestory(root->leftChild);
	TreeDestory(root->rightChild);
	free(root);
}