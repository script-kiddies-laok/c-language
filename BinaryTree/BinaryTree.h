#pragma once

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>


typedef char BTDataType;

typedef struct BinaryTreeNode
{
	struct BinaryTreeNode* leftChild;
	struct BinaryTreeNode* rightChild;
	BTDataType data;
}BTNode;

BTNode* CreateTreeNode(BTDataType x);

void PrevOrder(BTNode* root);

void InOrder(BTNode* root);

void PostOrder(BTNode* root);

int TreeSize(BTNode* root);

int TreeLeafSize(BTNode* root);

int TreeKLevelSize(BTNode* root, int k);

BTNode* TreeFind(BTNode* root, BTDataType x);

void TreeLevelOrder(BTNode* root);

bool TreeComplete(BTNode* root);

void TreeDestory(BTNode* root);