#define _CRT_SECURE_NO_WARNINGS 1

#include "BinaryTree.h"
#include "Queue.h"

void test()
{
	BTNode* A = CreateTreeNode('A');
	BTNode* B = CreateTreeNode('B');
	BTNode* C = CreateTreeNode('C');
	BTNode* D = CreateTreeNode('D');
	BTNode* E = CreateTreeNode('E');
	BTNode* F = CreateTreeNode('F');

	A->leftChild = B;
	A->rightChild = C;
	B->leftChild = D;
	B->rightChild = E;
	C->leftChild = F;

	PrevOrder(A);
	printf("\n");

	InOrder(A);
	printf("\n");

	PostOrder(A);
	printf("\n");

	printf("TreeSize:%d\n", TreeSize(A));
	printf("TreeLeafSize:%d\n", TreeLeafSize(A));
	printf("TreeKLevelSize:%d\n", TreeKLevelSize(A,3));

	BTNode* find = TreeFind(A, 'G');
	//printf("%c\n", find->data);

	TreeLevelOrder(A);
	printf("\n");

	printf("TreeComplete:%d\n", TreeComplete(A));
}

int main()
{
	test();
	return 0;
}