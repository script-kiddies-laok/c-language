#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>
#include <stdlib.h>
#include "Queue.h"

typedef char BTDataType;
typedef struct BinaryTreeNode
{
	struct BinaryTreeNode* leftChild;
	struct BinaryTreeNode* rightChild;
	BTDataType data;
}BTNode;

BTNode* CreateTreeNode(BTDataType x)
{
	BTNode* NewNode = (BTNode*)malloc(sizeof(BTNode));
	if (NewNode == NULL)
	{
		perror("CreateTreeNode malloc fail");
		return NULL;
	}
	NewNode->data = x;
	NewNode->leftChild = NULL;
	NewNode->rightChild = NULL;

	return NewNode;
}

void PrevOrder(BTNode* root)
{
	if (root == NULL)
	{
		printf("NULL ");
		return;
	}

	printf("%c ", root->data);
	PrevOrder(root->leftChild);
	PrevOrder(root->rightChild);

}

void InOrder(BTNode* root)
{
	if (root == NULL)
	{
		printf("NULL ");
		return;
	}

	InOrder(root->leftChild);
	printf("%c ", root->data);
	InOrder(root->rightChild);
}

void PostOrder(BTNode* root)
{
	if (root == NULL)
	{
		printf("NULL ");
		return;
	}

	PostOrder(root->leftChild);
	PostOrder(root->rightChild);
	printf("%c ", root->data);
}

int TreeSize(BTNode* root)
{
	return root == NULL ? 0 : TreeSize(root->leftChild) + TreeSize(root->rightChild) + 1;
}

int TreeLeafSize(BTNode* root)
{
	if (root == NULL)
	{
		return 0;
	}
	if (root->leftChild == NULL && root->rightChild == NULL)
	{
		return 1;
	}
	else
	{
		return TreeLeafSize(root->leftChild) + TreeLeafSize(root->rightChild);
	}
}

int TreeKLevelSize(BTNode* root,int k)
{
	if (root == NULL)
	{
		return 0;
	}

	if (k == 1)
	{
		return 1;
	}

	return TreeKLevelSize(root->leftChild, k - 1) + TreeKLevelSize(root->rightChild, k - 1);
}

BTNode* TreeFind(BTNode* root, BTDataType x)
{
	if (root == NULL)
	{
		return NULL;
	}
	if (root->data == x)
	{
		return root;
	}
	
	BTNode* leftChild = TreeFind(root->leftChild, x);
	if (leftChild)
		return leftChild;
	BTNode* rightChild = TreeFind(root->rightChild, x);
	if (rightChild)
		return rightChild;

	return NULL;
}

void TreeLevelOrder(BTNode* root)
{
	Queue q;
	QueueInit(&q);
	if (root)
	{
		QueuePushBack(&q, root);
	}

	while (!QueueEmpty(&q))
	{
		BTNode* front = QueueFront(&q);
		QueuePopFront(&q);
		printf("%c ", front->data);

		if (front->leftChild)
		{
			QueuePushBack(&q,front->leftChild);
		}
		if (front->rightChild)
		{
			QueuePushBack(&q, front->rightChild);

		}
	}
	QueueDestroy(&q);

}

bool BinaryTreeComplete(BTNode* root)
{
	Queue q;
	QueueInit(&q);
	if (root)
	{
		QueuePushBack(&q, root);
	}

	while (!QueueEmpty(&q))
	{
		BTNode* front = QueueFront(&q);
		QueuePopFront(&q);
		//printf("%c ", front->data);

		if (front->leftChild)
		{
			QueuePushBack(&q, front->leftChild);
		}
		if (front->rightChild)
		{
			QueuePushBack(&q, front->rightChild);

		}
	}
	QueueDestroy(&q);
}
#define MU(x,y) x*y

int main()
{
	printf("%d\n", 6 / MU(2+3,4+5));
	BTNode* A = CreateTreeNode('A');
	BTNode* B = CreateTreeNode('B');
	BTNode* C = CreateTreeNode('C');
	BTNode* D = CreateTreeNode('D');
	BTNode* E = CreateTreeNode('E');
	BTNode* F = CreateTreeNode('F');

	A->leftChild = B;
	A->rightChild = C;
	B->leftChild = D;
	B->rightChild = E;
	C->leftChild = F;

	PrevOrder(A);
	printf("\n");

	InOrder(A);
	printf("\n");

	PostOrder(A);
	printf("\n");

	printf("TreeSize:%d\n", TreeSize(A));

	printf("TreeLeafSize:%d\n", TreeLeafSize(A));

	BTNode* find = TreeFind(A, 'B');
	printf("%c\n", find->data);

	printf("TreeKLevelSize:%d\n", TreeKLevelSize(A,3));

	TreeLevelOrder(A);
	return 0;
}