#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>

struct BinaryTreeNode;

typedef struct BinaryTreeNode* QDataType;

typedef struct QListNode
{
	QDataType data;
	struct QListNode* next;
}QListNode;

typedef struct Queue
{
	QListNode* head;		//队头
	QListNode* tail;		//队尾
}Queue;

//初始化
void QueueInit(Queue* q);

//打印
void QueuePrint(Queue* q);

//队尾入队列
void QueuePushBack(Queue* q, QDataType x);

//队头出队列
void QueuePopFront(Queue* q);

//获取队列有效元素个数
int QueueSize(Queue* q);

//获取队尾元素
QDataType QueueBack(Queue* q);

//获取队头元素
QDataType QueueFront(Queue* q);

//检查队列是否为空
bool QueueEmpty(Queue* q);

//销毁
void QueueDestroy(Queue* q);