#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>

//int main()
//{
//	int arr[2][5] = { 10,9,8,7,6,5,4,3,2,1 };
//	int* ptr1 = (int*)(&arr + 1);				//&arr+1表示跳过整个数组
//	int* ptr2 = (int*)(*(arr + 1));				//arr+1表示跳过第一行
//	printf("%d %d", *(ptr1 - 1), *(ptr2 - 1));
//	return 0;
//}

#include <string.h>

void left_move(char arr[], int k)
{
	char tmp = 0;
	int len = strlen(arr);
	k %= len;
	int i = 0;
	for (i = 0; i < k; i++)
	{
		int j = 0;
		char tmp = arr[j];

		for (j = 0; j < len - 1; j++)
		{
			arr[j] = arr[j + 1];
		}
		arr[j] = tmp;
	}
}

int main()
{
	char arr[] = "abcdef";
	int k = 0;
	scanf("%d", &k);
	left_move(arr, k);

	printf("%s\n", arr);
	return 0;
}


//strlen要传入地址