#define _CRT_SECURE_NO_WARNINGS 1
//
//#include <stdio.h>
//
//int fun()
//{
//	static int count = 1;
//	return ++count;
//}
//
//int main()
//{
//	int answer = 0;
//	answer = fun() - fun() * fun();
//	printf("%d\n", answer);
//	return 0;
//}


//#include <stdio.h>
//
//int main()
//{
//	int i = 1;
//	i = (++i) + (++i) + (++i);
//	printf("%d\n", i);
//	return 0;
//}


//#include <stdio.h>
//
//int main()
//{
//	int a = 0;
//	int b = 0;
//	int i = 1;
//	int count = 0;
//
//	scanf("%d%d",&a,&b);
//
//	for (i = 0; i <= 31; i++)
//	{
//		if (((a >> i) & 1) != ((b >> i) & 1))
//		{
//			count++;
//		}
//	}
//
//	printf("%d\n", count);
//	return 0;
//}


//#include <stdio.h>
//
//int main()
//{
//	int i = 0;
//	int number = 0;
//	scanf("%d", &number);
//
//	for (i = 30; i >=0; i -= 2)
//	{
//		if ((number>>i) % 2 == 0)
//			printf("0 ");
//		else
//			printf("1 ");
//	}
//	printf("\n");
//	for (i = 31; i > 0; i -= 2)
//	{
//		if ((number >> i) % 2 == 0)
//			printf("0 ");
//		else
//			printf("1 ");
//	}
//	return 0;
//}


//#include <stdio.h>
//
//int main()
//{
//	int a = 10;
//	int* p = &a;
//	return 0;
//}
//
//#include <stdio.h>
//
//int main()
//{
//	int a = 10;
//	int* pi = &a;
//	char* pc = &a;
//	short* ps = &a;
//	float* pf = &a;
//	double* pd = &a;
//	long* pl = &a;
//	printf("%u\n", sizeof(pc));
//	return 0;
//}


//#include <stdio.h>
//
//int main()
//{
//	int a = 0x11223344;
//	char* pc = &a;
//	*pc = 0;
//	return 0;
//}


#include <stdio.h>

int main()
{
	int arr[10] = { 0 };
	int* pi = arr;
	char* pc = arr;
	printf("%p\n", pi);
	printf("%p\n", pi+1);
	printf("--------------------\n");
	printf("%p\n", pc);
	printf("%p\n", pc+1);

	return 0;
}