#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>
#include <math.h>
#include <string.h>
#include <assert.h>
//struct tag
//{
//	char name[20];
//};
//
//int main()
//{
//	struct tag s = { "hello" };
//	printf("%s\n", s.name);
//	int i = 1;
//	int* p = &i;
//	printf("%d\n", *p);
//	return 0;
//}


//int main()
//{
//	int arr[10] = { 0 };
//	int i = 0;
//	int* p = arr;
//	for (i = 0; i < 10; i++)
//	{
//		arr[i] = i;
//	}
//
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", *(p + i));
//	}
//	return 0;
//}

//int main()
//{
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	int len = sizeof(arr) / sizeof(arr[0]);
//	int left = 0;
//	int right = len - 1;
//	while (left <= right)
//	{
//		int ret = 0;
//		ret = arr[right];
//		arr[right] = arr[left];
//		arr[left] = ret;
//		left++;
//		right--;
//	}
//
//	for (int i = 0; i < len; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	
//	return 0;
//}



//int main()
//{
//	int num = 0;
//	scanf("%d", &num);
//	int i = 0;
//	int sum = 0;
//	int tmp = 0;
//	for (i = 1; i <= 5; i++)
//	{
//		tmp = tmp * 10 + num;
//		sum += tmp;
//	}
//
//	printf("%d\n",sum);
//	return 0;
//}

//int storage(int i,int arr[])
//{
//	int count = 0;
//	while (i)
//	{
//		arr[count] = i % 10;
//		i /= 10;
//		count++;
//	}
//	return count;
//}
//
//void IfRifht(int n, int count, int arr[])
//{
//	int i = 0;
//	double sum = 0;
//	for (i = 1; i <= count; i++)
//	{
//		if (arr[i-1] == 0)
//			continue;
//		sum += pow(arr[i-1], count);
//	}
//	if (n == sum)
//	{
//		printf("%d ", n);
//	}
//}
//
//int main()
//{
//	int i = 0;
//	for (i = 1; i < 100001; i++)
//	{
//		int arr[10] = { 0 };
//		int ret = storage(i, arr);
//		IfRifht(i,ret, arr);
//	}
//	
//	return 0;
//}


//int main()
//{
//	int i = 0;
//	//上部分
//	for (i = 1; i < 8; i++)
//	{
//		//空格
//		for (int j = 6; j > 0; j--)
//		{
//			
//		}
//		//星星
//		for (int j = 1; j <= 2 * i - 1; j++)
//		{
//			printf("* ");
//		}
//		printf("\n");
//	}
//	return 0;
//}


//int main()
//{
//	int a = 10;
//	int b = 10;
//	int* const p = &a;
//	*p = 20;
//
//	return 0;
//}

//void my_strcpy(char* str1, const char* str2)
//{
//	assert(str1 != NULL && str2 != NULL);
//	while (*str1++ = *str2++)
//		;
//}
//
//int main()
//{
//	char arr1[50] = "hello";
//	char arr2[15] = "bitaaaaa";
//	my_strcpy(arr1, arr2);
//
//	printf("%s\n", arr1);
//	return 0;
//}
//
//int main()
//{
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	int left = 0;
//	int right = sizeof(arr)/sizeof(arr[0])-1;
//	while (left <= right)
//	{
//		while (arr[left] % 2 == 1)
//		{
//			left++;
//		}
//		while (arr[right] % 2 == 0)
//		{
//			right--;
//		}
//		if (left<=right)
//		{
//			int ret = arr[right];
//			arr[right] = arr[left];
//			arr[left] = ret;
//		}
//	}
//	for (int i = 0; i < 10; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	return 0;
//}


//int main()
//{
//	int arr[10] = { 0 };
//	int* p = arr;
//	printf("%d\n", p + 9 - p);		//得到的是数组相差的个数
//	return 0;
//}


//int main()
//{
//	int arr[10] = { 0 };
//	int* p = arr;
//	int i = 0;
//	for (; p < &arr[10]; p++)
//	{
//		*p = i;
//		i++;
//	}
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	return 0;
//}


//int main()
//{
//	int a = 10;
//	int* p = &a;
//	int* * pp = &p;		//可以看成是int*和*pp两个部分
//						//int*表示的是指向类型是整型指针，*pp表示的是pp变量是一个指针                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             
//	printf("%d\n", *(*pp));
//	return 0;
//}

//int main()
//{
//	int a = 10;
//	int b = 12;
//	int c = 14;
//	int* p[3] = { &a,&b,&c };					//存放abc的地址
//
//	printf("%d %d %d\n", *p[0], *p[1], *p[2]);	//打印出abc的值
//	return 0;
//}

//struct tag
//{
//	char name[20];
//	int age;
//	char sex[5];
//};
//
//int main()
//{
//	struct tag s1 = { "zhangsan",19,"man" };
//	//这里的struct tag相当于int/double这样的
//	struct tag* p = &s1;	//结构体指针
//	printf("%d\n", s1.age);
//	printf("%s\n", p->name);
//	printf("%s\n", (*p).sex);
//	return 0;
//}


typedef struct Stu str;
struct Stu
{
	char name[20];
	int age;
	char sex[5];
};
void print(str* s)
{
	printf("%s\n", s->name);
}
int main()
{
	str s1 = {"zhangsan",17,"man"};
	print(&s1);
	//printf("%d\n", s1.age);
	return 0;
}