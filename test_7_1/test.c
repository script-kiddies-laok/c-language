#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>
#include <assert.h>
#include <stdlib.h>

//int main()
//{
//	//指针数组 - 存放指针的数组
//	int* arr[5];
//
//	//数组指针 - 指向数组的指针
//	int data[10] = { 0 };
//	int(*pd)[10] = &data;
//	return 0;
//}

//int main()
//{
//	//一维数组
//	int arr[10];
//	//数组名 首元素的地址
//	//&arr	数组的地址
//	int* p = arr;
//	int(*pa)[10] = &arr;
//
//	//二维数组
//	int data[3][4];
//	data;//首元素的地址			第0行第0个元素的地址
//	int(*pd)[4] = data;
//	int(*p2)[3][4]=&data;//二维数组的地址
//	
//	return 0;
//}

//int Add(int x, int y)
//{
//	return x + y;
//}
//
//void test(char* str)
//{
//
//}
//
//int main()
//{
//	int (*pf)(int, int) = &Add; //pf就是函数指针
//	//int (*pf)(int,int)=Add;	也可以这么写
//
//	//void (*p)(char*) = &test;
//
//	//printf("%p\n", &Add);
//	//printf("%p\n", Add);
//
//	int ret = (*pf)(2, 3);
//
//	printf("%d\n", ret);
//
//	return 0;
//}

//int my_strlen(const char* str)
//{
//	assert(str != NULL);
//	int count = 0;
//	while (*str)
//	{
//		str++;
//		count++;
//	}
//	return count;
//}
//
//int main()
//{
//	char arr[] = "abcdef";
//	//int len = my_strlen(arr);
//	//printf("%d\n", len);
//	
//	int (*pf)(const char*) = my_strlen;
//	int len=(*pf)("bit");
//	printf("%d\n", len);
//
//	len = pf("hello bit");
//	printf("%d\n", len);
//
//	return 0;
//}

//(*(void(*)())0)();  -
//调用0地址处的函数
//对0强制类型转换  - 变成函数的地址

//int main()
//{
//	double j = 0.0;
//	double o = 0.0;
//	for (int i = 1; i < 100; i+=2)
//	{
//		j = j + (1.0/i);
//	}
//	for (int i = 2; i <= 100; i += 2)
//	{
//		o = o + (1.0 / -i);
//	}
//	printf("%f\n", j);
//	printf("%f\n", o);
//	printf("%f\n", o + j);
//	return 0;
//}

//int Add(int x, int y)
//{
//	return x + y;
//}
//
//int Sub(int x, int y)
//{
//	return x - y;
//}
//
//int main()
//{
//	int a = 10;
//	int b = 20;
//	int (*pf1)(int, int) = Add;
//	int (*pf2)(int, int) = Sub;
//
//	int (*pfArr[2])(int, int) = { Add,Sub };
//	return 0;
//}


//计算器 - 加减乘除
//
//void menu()
//{
//	printf("****************************\n");
//	printf("*********1.add	2.sub*******\n");
//	printf("*********3.mul	4.div*******\n");
//	printf("*********0.exit		 *******\n");
//	printf("****************************\n");
//
//}

//int Add(int x, int y)
//{
//	return x + y;
//}
//
//int Sub(int x, int y)
//{
//	return x - y;
//}
//
//int Mul(int x, int y)
//{
//	return x * y;
//}
//
//int Div(int x, int y)
//{
//	return x / y;
//}
//
//int main()
//{
//	int input = 0;
//	int x = 0;
//	int y = 0;
//	int ret = 0;
//	//函数指针数组
//	int (*pfArr[])(int,int) = { 0,Add,Sub,Mul,Div };
//	do
//	{
//		menu();
//		printf("请选择：");
//		scanf("%d", &input);
//		if (0 == input)
//		{
//			printf("退出程序\n");
//			break;
//		}
//		else if (input >= 1 && input <= 4)
//		{
//			printf("请输入两个操作数\n");
//			scanf("%d %d", &x, &y);
//			ret = pfArr[input](x, y);
//			printf("%d\n", ret);
//		}
//		else
//		{
//			printf("选择错误\n");
//		}
//	} while (input);
//	return 0;
//}

//int main()
//{
//	int input = 0;
//	int x = 0;
//	int y = 0;
//	int ret = 0;
//	do
//	{
//		menu();
//		printf("请选择：");
//		scanf("%d", &input);
//		switch (input)
//		{
//		case 1:
//			printf("请输入两个操作数:\n");
//			scanf("%d %d", &x, &y);
//			ret = Add(x, y);
//			printf("ret=%d\n", ret);
//			break;
//		case 2:
//			printf("请输入两个操作数:\n");
//			scanf("%d %d", &x, &y);
//			ret = Sub(x, y);
//			printf("ret=%d\n", ret);
//			break;
//		case 3:
//			printf("请输入两个操作数:\n");
//			scanf("%d %d", &x, &y);
//			ret = Mul(x, y);
//			printf("ret=%d\n", ret);
//			break;
//		case 4:
//			printf("请输入两个操作数:\n");
//			scanf("%d %d", &x, &y);
//			ret = Div(x, y);
//			printf("ret=%d\n", ret);
//			break;
//		case 0:
//			printf("退出\n");
//			break;
//		default:
//			printf("选择错误\n");
//			break;
//		}
//	} while (input);
//	return 0;
//}

void print_arr(int arr[], int sz)
{
	for (int i = 0; i < 10; i++)
	{
		printf("%d ", arr[i]);
	}
}
//
//void bubble_sort(int arr[], int sz)
//{
//	int tmp = 0;
//	for (int i = 0; i < sz - 1; i++)
//	{
//		for (int j = 0; j < sz - 1 - i; j++)
//		{
//			if (arr[j] > arr[j + 1])
//			{
//				tmp = arr[j];
//				arr[j] = arr[j + 1];
//				arr[j + 1] = tmp;
//			}
//		}
//	}
//}
//
//int main()
//{
//	int arr[] = { 1,5,2,4,3,8,7,6,9,0 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	bubble_sort(arr, sz);//升序
//	print_arr(arr, sz);
//	return 0;
//}

int cmp_int(const void* e1,const void* e2)
{
	//if (*(int*)e1 > *(int*)e2)
	//	return 1;
	//else if (*(int*)e1 < *(int*)e2)
	//	return -1;
	//else
	//	return 0;
	return *(int*)e1 - *(int*)e2;
}

void test1()
{
	//qsort	排序整型数组
	int arr[] = { 1,5,2,4,3,8,7,6,9,0 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	qsort(arr, sz, sizeof(arr[0]),cmp_int);
	print_arr(arr, sz);
}

struct Stu
{
	char name[20];
	int age;
};

int cmp_stu_by_age(const void* e1, const void* e2)
{
	return ((struct Stu*)e1)->age-((struct Stu*)e2)->age;
}

int cmp_stu_by_name(const void* e1, const void* e2)
{
	return strcmp(((struct Stu*)e1)->name, ((struct Stu*)e2)->name);
}

void test2()
{
	//qsort	排序结构体
	struct Stu arr[] = { { "zhangsan",20 },{"lisi",31},{"wangwu",15}};
	//按照名字还是年龄
	int sz = sizeof(arr) / sizeof(arr[0]);
	//qsort(arr,sz,sizeof(arr[0]),cmp_stu_by_age);
	//按名字排序
	qsort(arr,sz,sizeof(arr[0]),cmp_stu_by_name);
}
void bubble_sort(void* base,size_t sz,size_t width,int(*cmp)(const void* e1,const void* e2))
{
	int i = 0;
	for (i = 0; i < sz - 1; i++)
	{
		int j = 0;
		for (j = 0; j < sz - 1 - i; j++)
		{
			//相邻两个元素的比较
			//如果补满足顺序就交换
			if (cmp((char*)base+j*width,(char*)base+(j+1)*width) > 0)
			{
				_swap((char*)base + j * width, (char*)base + (j + 1) * width,width);
			}
		}
	}
}

int main()
{
	test2();
	return 0;
}