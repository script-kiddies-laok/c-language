#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>
#include <assert.h>

//void print(int(*p)[5], int x, int y)
//{
//	int i = 0;
//	int j = 0;
//	for (i = 0; i < x; i++)
//	{
//		for (j = 0; j < y; j++)
//		{
//			//printf("%d ", *((*p + i)+j));
//			//printf("%d ", (*p + i)[j]);
//			printf("%d ", (*(p + i))[j]);
//		}
//		printf("\n");
//	}
//}
//
//int main()
//{
//	int arr[3][5] = { {1,2,3,4,5},{2,3,4,5,6},{3,4,5,6,7} };
//	printf("%d\n", *(*arr+1));				//二维数组可以看成是几个一维数组的集合
//	print(arr, 3, 5);
//	return 0;
//}

//int main()
//{
//	int arr[] = { 1,2,3,4,5 };
//	int(*p)[5] = &arr;
//	printf("%d ", (*p)[1]);
//	printf("%d ", *(*p + 1));
//	return 0;
//}


//int main()
//{
//	int arr[5];				//整型数组，每个元素是int类型
//	int* parr1[10];			//指针数组，每个元素是int*
//	int(*parr2)[10];		//数组指针，存放的是有10个元素，每个元素是int类型的数组
//	int(*parr3[10])[5];		//指针数组，每个元素是int(*)[5]类型的,大小为5的数组
//							//parr3先和[10]集合，说明parr3是一个数组
//	return 0;
//}

//void test1(int arr[])			//可以
//{}
//
//void test1(int arr[10])			//可以
//{}
//
//void test1(int* str)			//传入的是首元素的地址，可以这么写
//{}
//
//void test2(int* arr2[10])		//传什么我就用什么接收
//{}
//
//void test2(int** arr2)			//传入的是指针数组类型，每个元素是int*类型的，这就得用到
//{}								//二级指针，一个*表示接受这个数组的首元素地址，再来一个表示接受数组元素的地址
//
//int main()
//{
//	int arr[] = { 1,2,3 };
//	int* arr2[10] = { 0 };
//	test1(arr);
//	test2(arr2);
//	return 0;
//}
//
//void test(int arr[3][5])			//这是可以的
//{
//}
//
//void test(int arr[][])				//列不可以省略
//{
//}
//
//void test(int arr[][5])				//行可以省略
//{
//}
//
//void test(int* arr)					//传入的是第一行的地址，这种肯定不行
//{
//}
//
//void test(int* arr[5])				//这是指针数组，也不对
//{
//}
//
//void test(int(*arr)[5])				//这个可以，[]里面的数字要和传入的列数相同，否则会报警告
//{
//}
//
//void test(int** arr)				//二级指针是用来存放一直指针变量地址的，传入的是一行数组的地址
//{									//这种也不对
//}					
//
//int main()
//{
//	int arr[3][5] = { 0 };
//	test(arr);
//	return 0;
//}

//void test(int** p)
//{
//	;
//}
//
//int main()
//{
//	int arr = 1;
//	int* p[3] = { &arr };
//	test(p);
//	return 0;
//}

//int Add(int x, int y)
//{
//	return x + y;
//}
//
//int my_strlen(const char* str)
//{
//	;
//}
//
//int main()
//{
//	int a = 10;
//	int(*pf)(int, int) = Add;			//这就是函数指针
//	int(*pf1)(int, int) = &Add;			//这两种定义方法都可以
//	int(*ps)(const char*) = &my_strlen;
//	int result = (*pf)(2, 3);
//	printf("%d\n", result);
//	printf("%p\n", Add);
//	printf("%p\n", &Add);
//	return 0;
//}


//int Add(int x, int y)
//{
//	return x + y;
//}
//int main()
//{
//	int(*pf)(int, int) = Add;
//	printf("%d\n", (*pf)(2, 3));
//	printf("%d\n", pf(2, 3));			//这种方法也可以
//	printf("%d\n", (*****pf)(2, 3));	//这个*加不加都可以，但是加*要带括号，()操作符比*的优先级高
//	return 0;
//}

//char* my_strcpy(char* dest, const char* str)
//{
//	assert(dest != NULL && str != NULL);
//	char* ret = dest;
//	while (*str)
//	{
//		*dest++ = *str++;
//	}
//	return ret;
//}
//
//int main()
//{
//	char arr1[20] = { 0 };
//	//char* arr1 = NULL;
//	char arr2[] = "hello world";
//	char* (*pf)(char*, const char*) = my_strcpy;
//	pf(arr1, arr2);
//	
//	printf("%s\n", arr1);
//	return 0;
//}

//typedef void(*pf_y)(int);		//对void(*)(int)类型重定义成pf_y,语法规定这么写
//
//int main()
//{
//	pf_y signal(int, void(*)(int));
//
//	return 0;
//}

//int main()
//{								//void(*)()是一个函数指针类型
//	(*( void (*)() )0)();		//对0强制类型转换成函数指针类型，然后解引用，在调用
//	void (*signal(int, void(*)(int)))(int);
//	//这是一个函数的声明
//	//signal是一个函数，有两个参数，一个是int类型，另一个是函数指针类型
//	//返回值是一个函数指针类型，参数是int，返回值是void类型
//	return 0;
//}

//int Add(int x, int y)
//{
//	return x + y;
//}
//
//int Sub(int x, int y)
//{
//	return x - y;
//}
//
//int main()
//{
//	//int*p[3];								//指针数组
//	//int(*pf1)(int, int) = Add;
//	//int(*pf2)(int, int) = Sub;
//	int(*pf[2])(int, int) = { Add,Sub };	//函数指针数组
//
//	return 0;
//}

//void menu()
//{
//	printf("***1.Add 2.Sub*******\n");
//	printf("***3.Myl 4.Div*******\n");
//	printf("***  0.exit   *******\n");
//}
//
//int Add(int x, int y)
//{
//	return x + y;
//}
//
//int Sub(int x, int y)
//{
//	return x - y;
//}
//int Mul(int x, int y)
//{
//	return x * y;
//}
//int Div(int x, int y)
//{
//	return x / y;
//}
//int main()
//{
//	int input = 0;
//	int x = 0;
//	int y = 0;
//	int ret = 0;
//	int(*pfArr[5])(int, int) = { 0, Add,Sub,Mul,Div };
//	//把这种数组叫做转移表
//	do
//	{
//		menu();
//		printf("请选择:");
//		scanf("%d", &input);
//		if (input == 0)
//		{
//			printf("退出计算器\n");
//			break;
//		}
//		else if (input >= 1 && input <= 4)
//		{
//			printf("请输入两个数:");
//			scanf("%d%d", &x, &y);
//			ret = pfArr[input](x, y);
//			printf("%d\n", ret);
//		}
//		else
//		{
//			printf("输入错误,请重新输入\n");
//		}
//	} while (input);
//	return 0;
//}

int main()
{
	int* p[4];							//指针数组
	int(*p1)[4];						//数组指针
	int(*pf)(int, int);					//函数指针
	int(*pfArr[5])(int, int);			//函数指针数组
	int(*(*pfArr1)[5])(int, int)=&pfArr;//pfArr1先和*结合，表示是一个指针
										//把*pfArr1拿出来剩下的就是函数指针数组类型
	return 0;
}