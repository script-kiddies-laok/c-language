#pragma once

#include <stdio.h>
#include <stdlib.h>


typedef int SLTDataType;

typedef struct SListNode
{
	int data;
	struct SListNode* next;
}SListNode;


//打印
void SListPrint(SListNode* plist);

//尾插
void SListPushBack(SListNode** pplist, SLTDataType x);

//尾删
void SListPopBack(SListNode** pplist);

//头插
void SListPushFront(SListNode** pplist,SLTDataType x);

//头删
void SListPopFront(SListNode** pplist);

//查找
SListNode* SListFind(SListNode* plist, SLTDataType x);

//在pos之后的位置插入
void SListInsertAfter(SListNode* pos, SLTDataType x);

//删除pos之后的值
void SListEraseAfter(SListNode* pos);