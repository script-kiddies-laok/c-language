#define _CRT_SECURE_NO_WARNINGS 1

#include "SListNode.h"

void SListPrint(SListNode* plist)
{
	SListNode* cur = plist;
	while (cur != NULL)
	{
		printf("%d->", cur->data);
		cur = cur->next;
	}
	printf("NULL\n");
}

SListNode* BuySListNode(SLTDataType x)
{
	SListNode* NewNode = (SListNode*)malloc(sizeof(SListNode));
	if (NewNode == NULL)
	{
		perror("BuySListNode malloc err");
		return NULL;
	}
	NewNode->data = x;
	NewNode->next = NULL;

	return NewNode;
}

void SListPushBack(SListNode** pplist, SLTDataType x)
{
	SListNode* NewNode = BuySListNode(x);
	//1.空
	//2.一个或多个
	if (*pplist == NULL)
	{
		*pplist = NewNode;
	}
	else
	{
		//找尾
		SListNode* tail = *pplist;
		while (tail->next != NULL)
		{
			tail = tail->next;
		}
		
		tail->next = NewNode;
	}
}



void SListPopBack(SListNode** pplist)
{
	//1.空
	//2.只有一个
	//3.多个

	if (*pplist == NULL)
	{
		return;
	}
	else if ((*pplist)->next == NULL)
	{
		free(*pplist);
		*pplist = NULL;
	}
	else
	{
		SListNode* prev = NULL;
		SListNode* tail = *pplist;
		while (tail->next != NULL)
		{
			prev = tail;
			tail = tail->next;
		}

		free(tail);
		tail = NULL;

		prev->next = NULL;
	}
}


void SListPushFront(SListNode** pplist, SLTDataType x)
{
	SListNode* NewNode = BuySListNode(x);
	if (*pplist == NULL)
	{
		*pplist = NewNode;
	}
	else
	{
		NewNode->next = *pplist;
		*pplist = NewNode;
	}
}


void SListPopFront(SListNode** pplist)
{
	if (*pplist == NULL)
	{
		exit(-1);
	}
	else
	{
		*pplist = (*pplist)->next;
	}
}

SListNode* SListFind(SListNode* plist, SLTDataType x)
{
	SListNode* tail = plist;

	if (plist == NULL)
	{
		return NULL;
	}
	else if (tail->next == NULL)
	{
		if (tail->data == x)
		{
			return tail;
		}
		return NULL;
	}
	else
	{
		while (tail->next != NULL)
		{
			if (tail->data == x)
			{
				return tail;
			}
			tail = tail->next;
		}
		return NULL;
	}
}


void SListInsertAfter(SListNode* pos, SLTDataType x)
{
	SListNode* NewNode = BuySListNode(x);
	//1.下个为空
	if (pos->next == NULL)
	{
		pos->next = NewNode;
		NewNode->next = NULL;
	}
	else
	{
		NewNode->next = pos->next;
		pos->next = NewNode;
	}
}


void SListEraseAfter(SListNode* pos)
{
	if (pos->next == NULL)
	{
		exit(-1);
	}
	else if((pos->next)->next==NULL)
	{
		pos->next = NULL;
	}
	else
	{
		pos->next = (pos->next)->next;
	}
}