#define _CRT_SECURE_NO_WARNINGS 1

#include "SListNode.h"

void test()
{
	SListNode* plist = NULL;		

	//SListPushBack(&plist, 1);
	//SListPushBack(&plist, 2);
	//SListPopBack(&plist);
	//SListPushBack(&plist, 3);
	////SListPushBack(&plist, 4);
	//SListPushFront(&plist, 0);
	////SListPopBack(&plist);
	//SListPopFront(&plist);

	
	//SListPushBack(&plist, 1);
	//SListPushBack(&plist, 2);

	//SListNode* next = plist->next;
	//SListInsertAfter(next, 101);
	//SListEraseAfter(next);
	//SListPrint(plist);

	/*SListNode* pfind = SListFind(plist, 1);
	if (pfind == NULL)
	{
		printf("û�ҵ�\n");
	}
	else
	{
		printf("�ҵ���\n");
	}*/
}

SListNode* reverseList(SListNode* plist)
{
	if (plist == NULL || plist->next==NULL)
		return plist;
	SListNode* n1 = NULL, * n2 = plist, * n3 = plist->next;

	while (n2)
	{
		n2->next = n1;
		n1 = n2;
		n2 = n3;
		if (n3)
			n3 = n3->next;
	}
	return n2;
}

void test1()
{
	SListNode* plist = NULL;
	SListPushBack(&plist, 1);
	SListPushBack(&plist, 2);
	SListPushBack(&plist, 3);
	SListPushBack(&plist, 4);
	SListPushBack(&plist, 5);

	SListPrint(plist);

	SListNode* test = reverseList(plist);
	SListPrint(test);

}

SListNode* mergeTwoLists(SListNode* l1, SListNode* l2) {
	if (l1 == NULL)
		return l2;
	else if (l2 == NULL)
		return l1;

	SListNode* newNode = (SListNode*)malloc(sizeof(SListNode));
	SListNode* tail = newNode;
	while (l1 || l2)
	{
		if (l1->data >= l2->data)
		{
			tail->next = l2;
			l2 = l2->next;
		}
		else
		{
			tail->next = l1;
			l1 = l1->next;
		}
		tail = tail->next;
	}

	return newNode->next;
}

void test2()
{
	SListNode* plist1 = NULL;
	SListNode* plist2 = NULL;
	SListPushBack(&plist1, 1);
	SListPushBack(&plist1, 2);
	SListPushBack(&plist1, 4);
	SListPushBack(&plist2, 1);
	SListPushBack(&plist2, 3);
	SListPushBack(&plist2, 4);

	SListPrint(plist1);
	SListPrint(plist2);

	
	SListNode* test= mergeTwoLists(plist1, plist2);

	SListPrint(test);

}

int main()
{
	//test();
	//test1();

	//test2();
	return 0;
}