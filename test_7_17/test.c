#define _CRT_SECURE_NO_WARNINGS 1

//#include <stdio.h>

//int main()
//{
//	int n = 7;
//	printf("%d\n", 7 / 2);
//	//除号操作数有一个是浮点数，就执行的是浮点数的除法
//	printf("%.2lf\n", 7 / 2.0);	
//	//printf("%d\n", 7 % 5.0);	不能用取模计算浮点数
//	//printf("%d\n", 7 % 0);
//	return 0;
//}


//#include <stdio.h>
//
//int main()
//{
//	int a = 1;
//	int b = a << 1;
//	printf("%d\n", a);
//	printf("%d\n", b);
//	return 0;
//}


//#include <stdio.h>
//
//int main()
//{
//	int a = -1;
//	a = a << 1;
//	printf("%d\n", a);
//	return 0;
//}



//#include <stdio.h>
//
//int main()
//{
//	int a = 3;
//	int b = 5;
//	int c = a ^ b;
//	//00000000000000000000000000000011
//	//00000000000000000000000000000101
//
//	//00000000000000000000000000000110
//	//按位异或，相同为0，相异为1
//	
//	printf("%d\n", c);
//	return 0;
//}


//#include <stdio.h>
//
//int main()
//{
//	int a = 1;
//	//000000000000000001
//	//111111111111111111
//	//111111111111111110
//	//111111111111111101
//	//100000000000000010
//	int b = -1;
//
//	printf("%d\n", a ^ b);
//	return 0;
//}


//#include <stdio.h>
//
//int main()
//{
//	int a = 3;
//	int b = 5;
//
//	int tmp = a;
//	a = b;
//	b = tmp;
//
//	printf("%d %d\n", a, b);
//	return 0;
//}

//#include <stdio.h>
//
//int main()
//{
//	int a = 3;
//	int b = 5;
//
//	a = a + b;		//8
//	b = a - b;		//3
//	a = a - b;		//5
//
//	printf("%d %d\n", a, b);
//	return 0;
//}

//#include <stdio.h>
//
//int main()
//{
//	int a = 3;
//	int b = 5;
//	//都是正数，就算计最后一个字节，好看出结果
//	//00000011	a
//	//00000101	b
//	
//	a = a ^ b;		//00000110	a^b		a=6	
//	b = a ^ b;		//00000011	a^b		b=3
//	a = a ^ b;		//00000101	a^b		a=5
//
//	printf("%d %d\n", a, b);
//	return 0;
//}

//#include <stdio.h>
//
//int main()
//{
//	int i = 0;
//	int number = 0;
//	int count = 0;
//
//	scanf("%d", &number);
//
//	for (i = 1; i <= 32; i++)
//	{
//		if (((number>>i) & 1) == 1)
//		{
//			count++;
//		}
//	}
//	printf("%d\n", count);
//	return 0;
//}


//#include <stdio.h>
//
//int main()
//{
//	int a = 10;
//	int b = 10;
//	int c = 20;
//	a = b = c + 1;
//
//	printf("%d\n", a);
//	return 0;
//}


//#include <stdio.h>
//
//int main()
//{
//	int a = 1;
//	a += 1;		//a=a+1;
//	a *= 2;		//a=a*2;
//	return 0;
//}


//#include <stdio.h>
//
//int main()
//{
//	int flag = 0;
//	if (!flag)
//	{
//		printf("表示真\n");
//	}
//	else
//	{
//		printf("表示假\n");
//	}
//	return 0;
//}


//#include <stdio.h>
//
//int main()
//{
//	int a = 1;
//	a = -a;
//	printf("%d\n", a);
//
//	a = +a;			//很少用，不会改变负数的值，正数前面默认有+
//	printf("%d\n", a);
//	return 0;
//}


//#include <stdio.h>
//
//int main()
//{
//	int a = 10;		//& 取地址操作符	- 把a的地址拿出来
//	int* p = &a;	//int* 表示整型指针，p是变量
//	*p = 20;		//指针是用来存放地址的，把a的地址放在指针变量p里面
//					//* 解引用操作符-间接访问操作符
//					//*p-表示通过指针变量p里存放的地址找到变量a的值
//	printf("%d\n", a);
//	printf("%d\n", *p);
//	return 0;
//}


//#include <stdio.h>
//
//int main()
//{
//	int a = 10;
//	printf("%d\n", sizeof(a));
//	printf("%d\n", sizeof a);		//这种情况可以省略
//	//printf("%d\n", sizeof int);		//不能省略
//	printf("%d\n", sizeof(int));
//	printf("%d\n", sizeof(&a));
//
//	int arr[10] = { 0 };
//	printf("%d\n", sizeof(arr));	//整个数组的大小
//	printf("%d\n", sizeof(int[10]));//数组的类型
//	return 0;
//}


#include <stdio.h>

int main()
{
	short s = 3;
	int a = 10;
	printf("%d\n", sizeof(s = a + 5));
	printf("%d\n", s);
	return 0;
}