#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>
#include <string.h>
#include <assert.h>

//struct A
//{
//	int _a : 10;		//int类型开辟4个字节空间，分配10个
//	int _b : 20;		//在分配20个
//	int _c : 30;		//不够了在开辟4个字节的空间
//						//加起来就是8个字节
//};
//
//int main()
//{
//	printf("%d\n", sizeof(struct A));
//	return 0;
//}

//struct S
//{
//	char a : 3;
//	char b : 4;
//	char c : 5;
//	char d : 4;
//};
//
//int main()
//{
//	struct S s = { 0 };
//	s.a = 10;
//	s.b = 12;
//	s.c = 3;
//	s.d = 4;
//
//	printf("%d\n", sizeof(struct S));
//	return 0;
//}

////枚举常量
//enum Color
//{
//	RED,			//默认是0开始的，都是常量
//	GREEN,
//	BLUE
//};
//
//enum Sex
//{
//	MALE,
//	FEMALE,
//	SECRET
//};
//
//int main()
//{
//	enum Sex zhangsan = FEMALE;
//	if (zhangsan == FEMALE)
//	{
//		printf("%d\n", 1);
//	}
//
//	printf("%d\n", RED);
//	printf("%d\n", GREEN);
//	printf("%d\n", BLUE);
//
//	return 0;
//}
//
//void menu()
//{
//	printf("**************************\n");
//	printf("*****1.add    2.sub  *****\n");
//	printf("*****3.mul    4.div  *****\n");
//	printf("******    0.exit     *****\n");
//	printf("**************************\n");
//}
//
//enum Calc
//{
//	EXIT,
//	ADD,
//	SUB,
//	MUL,
//	DIV
//};
//
//void Add()
//{
//	int x, y;
//	printf("请输入两个数：");
//	scanf("%d %d", &x, &y);
//	printf("%d\n", x + y);
//}
//
//int main()
//{
//	int input = 0;
//	do
//	{
//		menu();
//		printf("请选择:");
//		scanf("%d", &input);
//		switch (input)
//		{
//		case ADD:
//			Add();
//			break;
//		}
//	} while (input);
//	return 0;
//}


//union Un
//{
//	char c;		//1
//	int i;		//4
//};
//int main()
//{
//	union Un u = { 0 };
//	printf("%d\n", sizeof(union Un));		//最大对齐数的整数倍
//	printf("%p\n", &u);
//	printf("%p\n", &(u.c));					//共用同一块空间
//	printf("%p\n", &(u.i));
//
//	return 0;
//}

//
//
//char* my_strstr(const char* string, const char* str)
//{
//	char* ret = NULL;
//	char* str1 = (char*)str;
//	while (*str&&*string)
//	{
//		if (*str == *string)
//		{
//			str++;
//			string++;
//			if (*str == '\0')
//			{
//				ret = (char*)string;
//			}
//		}
//		else
//			string++; 
//	}
//	if (ret != NULL)
//	{
//		char* Fa = strcat(str1, ret); 
//		return Fa;
//	}
//	return NULL;
//}
//
//int main()
//{
//	char string[] = "hello world";
//	char str[] = "wo";
//	char* ret = my_strstr(string, str);
//
//	printf("%s\n", ret);
//	return 0;
//}



//char* my_strcat(char* destination, const char* source)
//{
//	assert(destination && source);
//	char* ret = destination;
//	while (*ret)
//	{
//		ret++;
//	}
//	while (*ret = *source)
//	{
//		ret++;
//		source++;
//	}
//	return destination;
//}
//
//int main()
//{
//	char string[40] = "hello ";
//	char str[] = "world";
//
//	char* ret = my_strcat(string, str);
//
//	printf("%s\n", ret);
//	return 0;
//}

//int my_strcmp(const char* str1, const char* str2)
//{
//	while (*str1 && *str2)
//	{
//		if (*str1 == *str2)
//		{
//			str1++;
//			str2++;
//		}
//		else
//			return *str1 - *str2;
//	}
//
//	return *str1 - *str2;
//}
//
//int main()
//{
//	char str1[] = "abc";
//	char str2[] = "abd";
//
//	int ret = my_strcmp(str1, str2);
//
//	printf("%d\n", ret);
//	return 0;
//}

union Un
{
	int i;		//4
	char c;		//1
};

int main()
{
	union Un u = {0};
	printf("%d\n", sizeof(union Un));
	return 0;
}