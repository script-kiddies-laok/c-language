#define _CRT_SECURE_NO_WARNINGS 1



//int main()
//{
//	int a = 3;
//	int b = 5;
//	int c = 0;
//	int d = 0;
//	printf("%d\n", a && b);
//	printf("%d\n", a && c);
//	printf("%d\n", a || c);
//	printf("%d\n", c || d);
//	return 0;
//}
//
//#include <stdio.h>
//
//int main()
//{
//	int i = 0, a = 0, b = 2, c = 3, d = 4;
//	i = a++ || ++b || d++;
//
//	printf("i = %d\na = %d\nb = %d\nc = %d\nd = %d\n", i, a, b, c, d);
//	return 0;
//}


//#include <stdio.h>
//
//int main()
//{
//	int a = 3;
//	int b = 5;
//	printf("%d\n", a > b ? a : b);
//	return 0;
//}


//#include <stdio.h>
//
//int main()
//{
//	int a = 3;
//	int b = (a = a + 1, a = a + 2, a = a + 1);
//
//	printf("%d\n", b);
//	return 0;
//}


//#include <stdio.h>
//struct Stu
//{
//	char name[20];
//	int age;
//	char sex[5];
//};
//int main()
//{
//	struct Stu s = { "zhangsan",18,"man" };
//	printf("%s\n",s.name);
//	printf("%d\n", s.age);
//	printf("%s\n", s.sex);
//
//	struct Stu* p = &s;
//	printf("%s\n", (*p).name);
//
//	printf("%s\n", p->name);
//
//	return 0;
//}


#include <stdio.h>

int main()
{
	char a = 3;
	char b = 127;
	char c = 0;
	c = a + b;
	//整型提升补原符号位
	//00000011
	//00000000000000000000000000000011	a
	//01111111
	//00000000000000000000000001111111	b
	//00000000000000000000000010000010	a+b
	//10000010
	//打印出来的是int类型，在发生提升
	//11111111111111111111111110000010	补码
	//11111111111111111111111110000001	反码
	//10000000000000000000000001111110	原码	  -  -126
	printf("%d\n", c);
	printf("%c\n", c);

	return 0;
}