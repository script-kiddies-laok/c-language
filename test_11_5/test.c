#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>
#include <string.h>
#include "Stack.h"

void myPrint(int* a, int n)
{
	for (int i = 0; i < n; i++)
	{
		printf("%d ", a[i]);
	}
	printf("\n");
}

void Swap(int* a, int* b)
{
	int tmp = *a;
	*a = *b;
	*b = tmp;
}

void BubbleSort(int* a, int n)
{
	for (int i = 0; i < n - 1; i++)
	{
		int flag = 0;
		for (int j = 0; j < n - 1 - i; j++)
		{
			if (a[j + 1] < a[j])
			{
				Swap(&a[j + 1], &a[j]);
				flag = 1;
			}
		}
		if (flag == 0)
			break;
	}
}
//三数取中
int GetMinIndex(int* a, int begin, int end)
{
	int keyi = (begin + end) >> 1;
	if (a[begin] > a[keyi])
	{
		if (a[keyi] > a[end])
			return keyi;
		else if (a[end] > a[begin])
			return a[begin];
		else
			return end;
	}
	else  //a[keyi]>a[begin]
	{
		if (a[end] > a[keyi])
			return keyi;
		else if (a[begin] > a[end])
			return begin;
		else
			return end;
	}
			
}

//左右指针法
int PartSort1(int* a, int begin, int end)
{
	int midIndex = GetMinIndex(a, begin, end);
	Swap(&a[begin], &a[midIndex]);
	int keyi = begin;
	while (begin < end)
	{
		//右边往左走找小
		while (begin < end && a[end] > a[keyi])
			end--;
		//左边往右走找大
		while (begin < end && a[begin] < a[keyi])
			begin++;
		Swap(&a[begin], &a[end]);
	}
	Swap(&a[keyi], &a[begin]);

	return begin;
}
//挖坑法
int PartSort2(int* a, int begin, int end)
{
	int midIndex = GetMinIndex(a, begin, end);
	Swap(&a[begin], &a[midIndex]);

	int keyi = a[begin];
	while (begin < end)
	{
		while (begin < end && a[end] > keyi)
			end--;
		a[begin] = a[end];
		while (begin < end && a[begin] < keyi)
			begin++;
		a[end] = a[begin];
	}
	a[begin] = keyi;
	return begin;
}
//前后指针法
int PartSort3(int* a, int begin, int end)
{
	int midIndex = GetMinIndex(a, begin, end);
	Swap(&a[begin], &a[midIndex]);

	int prev = begin, cur = begin + 1;
	int keyi = begin;
	while (cur <= end)
	{
		if (a[cur] < a[keyi] && ++prev != cur)
			Swap(&a[prev], &a[cur]);
		cur++;
	}
	Swap(&a[prev], &a[keyi]);
	return prev;
}

void QuickSort(int* a, int begin,int end)
{
	if (begin >= end)
		return;

	int keyi = PartSort1(a, begin, end);
	//分成两段
	QuickSort(a, begin, keyi - 1);
	QuickSort(a, keyi + 1, end);

}

void QuickSortNonR(int* a, int begin, int end)
{
	Stack st;
	StackInit(&st);
	StackPush(&st, begin);
	StackPush(&st, end);
	while (!StackEmpty(&st))
	{
		int right = StackTop(&st);
		StackPop(&st);
		
		int left = StackTop(&st);
		StackPop(&st);

		int keyi = PartSort1(a, left, right);
		if (left < keyi - 1)
		{
			StackPush(&st, left);
			StackPush(&st, keyi - 1);
		}
		if (keyi + 1 < right)
		{
			StackPush(&st, keyi + 1);
			StackPush(&st, right);
		}
	}
	StackDestroy(&st);
}

void _MergeSort(int* a, int begin, int end, int* tmp)
{
	if (begin >= end)
		return;

	int mid = (begin + end) >> 1;
	_MergeSort(a, begin, mid, tmp);
	_MergeSort(a, mid + 1, end, tmp);

	int left1 = begin, right1 = mid;
	int left2 = mid + 1, right2 = end;
	int i = begin;
	while (left1 <= right1 && left2 <= right2)
	{
		if (a[left1] < a[left2])
			tmp[i++] = a[left1++];
		else
			tmp[i++] = a[left2++];
	}

	while (left1 <= right1)
		tmp[i++] = a[left1++];
	while (left2 <= right2)
		tmp[i++] = a[left2++];

	for (int j = 0; j < end; j++)
	{
		a[j] = tmp[j];
	}
}

void MergeSort(int* a, int n)
{
	int* tmp = (int*)malloc(sizeof(int) * n);
	if (tmp == NULL)
	{
		perror("malloc fail");
		return;
	}

	_MergeSort(a, 0, n - 1, tmp);

	free(tmp);
}

int main()
{
	int a[] = { 2,4,5,1,6,8 };
	//BubbleSort(a, sizeof(a) / sizeof(a[0]));
	//QuickSortNonR(a, 0, sizeof(a) / sizeof(a[0])-1);
	MergeSort(a, sizeof(a) / sizeof(a[0]));
	myPrint(a, sizeof(a) / sizeof(a[0]));
	return 0;
}

