

#include <stdio.h>

//int main()
//{
//	printf("hello world\n");
//	printf("hello world\n");
//	printf("hello world\n");
//	printf("hello world\n");
//	printf("hello world\n");
//	printf("hello world\n");
//	return 0;
//}

//main函数 有两个参数 (int argc, char* argv[])
// int main(void) 表示我不接受参数


//int main()
//{
//	int age = 18;
//	int price = 50;
//	double weight = 55.5;
//	//double 双精度浮点型
//	return 0;
//}
//int			整型
//short		短整型
//char		字符型
//long		长整型
//long long	更长的整形
//float		单精度浮点型
//double		双精度浮点型

//计算一个类型所占的空间大小

//int main()
//{
//	printf("%d\n", sizeof(char));
//	printf("%d\n", sizeof(int));
//	printf("%d\n", sizeof(short));
//	printf("%d\n", sizeof(long));
//	printf("%d\n", sizeof(long long));
//	printf("%d\n", sizeof(float));
//	printf("%d\n", sizeof(double));
//	return 0;
//}
////long的大小>=int

//int main()
//{
//	printf("%c\n", 100);
//	return 0;
//}
////输出的是100所对应的ascii码
//int a = 100;
//
//int test() {
//	int a = 50;
//}
//int main()
//{
//	int a = 10;
//	printf("%d\n", a);
//	return 0;
//}
////定义在函数内部的变量称为局部变量，它的作用域仅限于函数内部， 
////离开该函数后就是无效的，再使用就会报错

int main()
{
	int num1,num2;
	scanf("%d%d", &num1, &num2);
	printf("%d\n", num1 + num2);
	return 0;
}