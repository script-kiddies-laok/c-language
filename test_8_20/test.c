#define _CRT_SECURE_NO_WARNINGS 1

//#include <stdio.h>
//#include <ctype.h>
//int main()
//{
//	char c = '2';
//	int ret = isdigit(c);
//	printf("%d\n", ret);
//	return 0;
//}


#include <stdio.h>
#include <ctype.h>
#include <string.h>
//int main()
//{
//	char ch = 'A';
//	//printf("%c\n", ch + 32);
//	printf("%c\n", tolower(ch));		//���ַ�ת����Сд
//	return 0;
//}
// 
// 
//#include <assert.h>
//
//void* my_memcpy(void* dest, const void* src, size_t count)
//{
//	assert(dest && src);
//	void* ret = dest;
//	while (count--)
//	{
//		*((char*)dest)++ = *((char*)src)++;
//	}
//	return ret;
//}
//
//int main()
//{
//	int arr1[] = { 1,2,3,4,5 };
//	int arr2[10] = { 0 };
//	my_memcpy(arr2, arr1, sizeof(arr1));
//	return 0;
//}


//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
//	memcpy(arr + 2, arr, 16);
//	return 0;
//}



#include <assert.h>

void* my_memcpy(void* dest, const void* src, size_t count)
{
	assert(dest && src);
	void* ret = dest;
	while (count--)
	{
		*((char*)dest)++ = *((char*)src)++;
	}
	return ret;
}

int main()
{
	int arr1[] = { 1,2,3,4,5,6,7,8,9,10 };
	my_memcpy(arr1+2, arr1, 16);
	return 0;
}