#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>

//int missingNumber(int* nums, int numsSize)
//{
//    //int n = numsSize + 1;
//    //int ret = *nums;
//    //int i = 0;
//    //for (i = 0; i < numsSize-1; i++)
//    //{
//    //    ret ^= *(nums+1+i);
//    //}
//    //for (i = 0; i < n; i++)
//    //{
//    //    ret ^= i;
//    //}
//    //return ret;
//    
//
//    int n = numsSize + 1;
//    int ret = 0;
//    for (int i = 0; i < n; i++)
//    {
//        ret += i;
//    }
//    int numbs = 0;
//    for (int j = 0; j < numsSize; j++)
//    {
//        numbs += *(nums + j);
//    }
//    return ret - numbs;
//}
//
//int main()
//{
//    int arr[] = { 3,0,1 };
//    int sz = sizeof(arr) / sizeof(arr[0]);
//    int ret = missingNumber(arr, sz);
//
//    printf("%d\n", ret);
//	return 0;
//}




//int main()
//{
//	int arr[] = { 1,2,5,2 };
//    int ret = 0;
//    for (int i = 0; i < 4; i++)
//    {
//        ret ^= arr[i];               //x^y
//    }
//
//    int n = 0;
//    while ((ret & 1) == 0)
//    {
//        n++;
//        ret >>= 1;
//    }
//	return 0;
//}

//void Func(int N)
//{
//	int count = 0;
//	for (int i = 0; i < N; i++)
//	{
//		for (int j = 0; j < N; j++)
//		{
//			count++;
//		}
//	}
//
//	for (int k = 0; k < N; k++)
//	{
//		count++;
//	}
//
//	int M = 10;
//	while (M)
//	{
//		count++;
//	}
//
//	printf("%d\n", count);
//}
//
//int main()
//{
//	int n = 0;
//	scanf("%d\n", &n);
//	Func(n);
//
//	return 0;
//}


//int main()
//{
//	int n = 0;
//
//	return 0;
//}

//
//void Func(int N)
//{
//	int count = 0;
//	for (int i = 0; i < 2 * N; i++)
//	{
//		count++;
//	}
//
//	int M = 10;
//	while (M--)
//	{
//		count++;
//	}
//
//	printf("%d\n", count);
//}


//void Func(int N,int M)
//{
//	int count = 0;
//	for (int i = 0; i < N; i++)
//	{
//		count++;
//	}
//
//	for (int j = 0; j < M; j++)
//	{
//		count++;
//	}
//
//	printf("%d\n", count);
//}


//void Func()
//{
//	int count = 0;
//	for (int i = 0; i < 100; i++)
//	{
//		count++;
//	}
//	printf("%d\n", count);
//}


//void BubbleSort(int* a, int n)
//{
//	assert(a);
//	for (int i = 0; i < n - 1; i++)
//	{
//		int flag = 0;
//		for (int j = 0; j < n - 1 - i; j++)
//		{
//			if (a[j] > a[j + 1])
//			{
//				int tmp = a[j];
//				a[j] = a[j + 1];
//				a[j + 1] = tmp;
//				flag = 1;
//			}
//		}
//		if (flag == 0)
//			break;
//	}
//}


//void BinarySearch(int* a, int n, int x)
//{
//	assert(a);
//	int start = 0;
//	int end = n - 1;
//	while (start < end)
//	{
//		int mid = start + (end - start) / 2;
//		if (a[mid] < x)
//		{
//			start = mid + 1;
//		}
//		else if (a[mid] > x)
//		{
//			end = mid - 1;
//		}
//		else
//			return mid;
//	}
//	return -1;
//}


int singleNumber(int* nums, int numsSize) {
    int ret = 0;
    for (int i = 0; i < numsSize; i++)
    {
        ret ^= nums[i];
    }

    int count = 0;
    while ((ret & 1) == 0)
    {
        ret >>= 1;
        count++;
    }
    int j = 0;
    for (j = 0; j < numsSize; j++)
    {
        if (((nums[j] >> count) & 1) == 1)
            break;
    }
    return nums[j];
}



int main()
{
    int arr[] = { 30000,500,100,30000,30000,100,100 };
    int sz = sizeof(arr) / sizeof(arr[0]);
    int ret = singleNumber(arr, sz);

    printf("%d\n", ret);
    return 0;
}