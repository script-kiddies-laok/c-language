#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>

//int main()
//{
//	int a, b, c, d, e;
//	scanf("%d%d%d%d%d", &a, &b, &c, &d, &e);
//	printf("%0.1f\n", (a + b + c + d + e) / 5.0);
//	return 0;
//}

//int main()
//{
//	printf("v   v\n");
//	printf(" v v \n");
//	printf("  v  \n");
//	return 0;
//}
//int main()
//{
//    printf("The size of short is %d bytes.\n", sizeof(short));
//    printf("The size of int is %d bytes.\n", sizeof(int));
//    printf("The size of long is %d bytes.\n", sizeof(long));
//    printf("The size of long long is %d bytes.\n", sizeof(long long));
//    printf("The size of float is %d bytes.\n", sizeof(float));
//    printf("The size of double is %d bytes.\n", sizeof(double));
//    printf("The size of char is %d bytes.\n", sizeof(char));
//    return 0;
//}

//int main()
//{
//    printf("     **     \n");
//    printf("     **     \n");
//    printf("************\n");
//    printf("************\n");
//    printf("    *  *    \n");
//    printf("    *  *    \n");
//    return 0;
//}

//int main()
//{
//	int a = 1234;
//	printf("%#o %#X", a,a);
//	return 0;
//}

//#include <stdio.h>
//
//int main()
//{
//    printf("%15d\n", 0XABCDEF);
//    return 0;
//}

//#include <stdio.h>
//
//int main()
//{
//	int i = printf("Hello world!");
//	printf("\n");
//	printf("%d", i);
//	return 0;
//}

//#include <stdio.h>
//
//int main()
//{
//    int a, b, c;
//    scanf("%d%d%d", &a, &b, &c);
//    printf("score1=%d,score2=%d,score3=%d \n", a, b, c);
//    return 0;
//}
//#include <stdio.h>
//
//int main()
//{
//    int a;
//    double b, c, d;
//    scanf("%d", &a);
//    printf("%d\n", a);
//    return 0;
//}

//#include <stdio.h>
//
//int main()
//{
//    int a;
//    float b, c, d;
//    scanf("%d;%f,%f,%f", &a, &b, &c, &d);
//    printf("The each subject score of  No. %d is %0.2f, %0.2f, %0.2f.\n", a, b, c, d);
//    return 0;
//}

//int main()
//{
//	int a;
//	printf("请输入一个整数:");
//	scanf("%d", &a);
//	printf("%d\n", a + 10);
//	return 0;
//}

//int main()
//{
//	puts("hello world");
//	//puts()  当不需要格式化输出的时候可以用puts 带换行
//	printf("hello world!\n");
//	return 0;
//}

//int main()
//{
//	int a, b;
//	puts("hello world");
//	printf("hello world:");	scanf("%d", &a);
//	printf("hello world:"); scanf("%d", &b);
//	printf("%d", a + b);
//	return 0;
//}

//int main()
//{
//	puts("风");
//	puts("风");
//	puts("风");
//	puts("风");
//	return 0;
//}

//int main()
//{
//	int a, b;
//	puts("请输入两个整数：");
//	printf("整数1：");		scanf("%d", &a);
//	printf("整数2：");		scanf("%d", &b);
//	printf("它们的乘积是%d.", a * b);
//	return 0;
//}

//int main()
//{
//	int a, b;
//	puts("输入两个整数：");
//	printf("整数1：");		scanf("%d", &a);
//	printf("整数2：");		scanf("%d", &b);
//	printf("a + b = %d\n", a + b);
//	printf("a - b = %d\n", a - b);
//	printf("a * b = %d\n", a * b);
//	printf("a / b = %d\n", a / b);
//	printf("a %% b = %d\n", a % b);
//	//除   只取整数部分
//	//%		取余数
//	return 0;
//}

//获取整数的最后一位

//int main()
//{
//	int a;
//	scanf("%d", &a);
//	printf("%d", a % 10);
//	return 0;
//}

int main()
{
	int a, b;
	printf("输入两个整数：\n");
	printf("整数a：");
	scanf("%d", &a);
	printf("整数b：");
	scanf("%d", &b);
	printf("a的值是b的%.0f%%\n",((double) a/b) * 100); 
	return 0;
}
