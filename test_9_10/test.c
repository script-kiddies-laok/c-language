#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>
#include <string.h>
#include <assert.h>

//char* my_strstr(const char* string, const char* str)
//{
//	char* ret = NULL;
//	char* str1 = (char*)str;
//	while (*str&&*string)
//	{
//		if (*str == *string)
//		{
//			str++;
//			string++;
//			if (*str == '\0')
//			{
//				ret = (char*)string;
//			}
//		}
//		else
//			string++; 
//	}
//	if (ret != NULL)
//	{
//		char* Fa = strcat(str1, ret); 
//		return Fa;
//	}
//	return NULL;
//}
//
//int main()
//{
//	char string[] = "hello world";
//	char str[40] = "wo";
//	char* ret = my_strstr(string, str);
//
//	printf("%s\n", ret);
//	return 0;
//}

//char* my_strcpy(char* destination, const char* source)
//{
//	assert(*destination && *source);
//	char* ret = destination;
//	while (*source)
//	{
//		*destination++ = *source++;
//	}
//
//	return ret;
//}
//
//int main()
//{
//	char string[] = "hello";
//	char str[40] = "0";
//
//	char* ret = my_strcpy(str,string);
//
//	printf("%s\n", ret);
//	return 0;
//}

//int my_strlen(const char* str)
//{
//	assert(str);
//	int count = 0;
//	while (*str)
//	{
//		str++;
//		count++;
//	}
//	return count;
//}

//int my_strlen(const char* str)
//{
//	assert(str);
//	if (*str != '\0')
//		return 1 + my_strlen(str + 1);
//	else
//		return 0;
//}

//int my_strlen(const char* str)
//{
//	assert(str);
//	const char* start = str;
//	while (*str)
//	{
//		str++;
//	}
//	return str-start;
//}
//
//int main()
//{
//	char str[] = "hello world";
//	int ret = my_strlen(str);
//
//	printf("%d\n", ret);
//	return 0;
//}

//void my_memmove(void* dest, const void* src, int count)
//{
//	assert(dest && src);
//	if(dest>src)
//	while (count--)
//	{
//		*(((char*)dest)+count) = *(((char*)src)+count);
//	}
//	else
//	{
//		while (count--)
//		{
//			*((char*)dest)++ = *((char*)src)++;
//		}
//	}
//}
//
//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
//
//	my_memmove(arr + 2, arr, 16);
//
//	for (int i = 0; i < 10; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	return 0;
//}
//
//void my_memcpy(void* dest, const void* src, int count)
//{
//	assert(dest && src);
//	while (count--)
//	{
//		*((char*)dest)++ = *((char*)src)++;
//	}
//}
//
//int main()
//{
//	int arr1[] = { 1,2,3,4 };
//	int arr2[20] = { 0 };
//
//	my_memcpy(arr2, arr1, sizeof(arr1));
//
//	for (int i = 0; i < 4; i++)
//	{
//		printf("%d ", arr2[i]);
//	}
//	
//	return 0;
//}

//int check_sys()
//{
//	int n = 1;
//	return *(char*)&n;
//}


int check_sys()
{
	union Un
	{
		int i;
		char c;
	}u;
	u.i = 1;
	//返回1，表示小端
	//返回0，表示大端
	return u.c;
}

int main()
{
	int ret = check_sys();

	if (ret == 1)
	{
		printf("小端\n");
	}
	else
		printf("大端\n");
	return 0;
}