#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

typedef int SLDataType;

typedef struct ListNode
{
	SLDataType data;
	struct ListNode* next;
	struct ListNode* prev;
}ListNode;

typedef struct List
{
	ListNode* head;
}List;


//初始化
void ListInit(List* plist);

//打印
void ListPrint(List* plist);

//尾插
void ListPushBack(List* plist, SLDataType x);

//尾删
void ListPopBack(List* plist);

//头插
void ListPushFront(List* plist, SLDataType x);

//头删
void ListPopFront(List* plist);

//查找
ListNode* ListFind(List* plist, SLDataType x);

//在pos前面插入数据
void ListInsert(ListNode* pos, SLDataType x);

//删除pos当前节点
void ListErase(ListNode* pos);

//销毁
void ListDestroy(List* plist);