#define _CRT_SECURE_NO_WARNINGS 1

#include "ListNode.h"

ListNode* BuyListNode(SLDataType x)
{
	ListNode* NewNode = (ListNode*)malloc(sizeof(ListNode));
	if (NewNode == NULL)
	{
		perror("BuyListNode malloc fail");
		return NULL;
	}
	NewNode->data = x;
	NewNode->next = NewNode->prev = NULL;

	return NewNode;
}

void ListInit(List* plist)
{
	plist->head = BuyListNode(0);

	plist->head->next = plist->head;
	plist->head->prev = plist->head;
}

void ListPushBack(List* plist, SLDataType x)
{
	assert(plist);
	ListNode* tail = plist->head->prev;
	ListNode* NewNode = BuyListNode(x);
	
	tail->next = NewNode;
	NewNode->prev = tail;
	NewNode->next = plist->head;
	plist->head->prev = NewNode;
}

void ListPrint(List* plist)
{
	assert(plist);

	ListNode* cur = plist->head->next;
	while (cur != plist->head)
	{
		printf("%d <=> ", cur->data);
		cur = cur->next;
	}
	printf("NULL\n");
}

void ListPopBack(List* plist)
{
	assert(plist);
	ListNode* tail = plist->head->prev;

	tail->prev->next = plist->head;
	plist->head->prev = tail->prev;
	free(tail);
}

void ListPushFront(List* plist, SLDataType x)
{
	assert(plist);
	ListNode* NewNode = BuyListNode(x);
	
	ListNode* head = plist->head, * next = head->next;

	head->next = NewNode;
	NewNode->prev = head;
	NewNode->next = next;
	next->prev = NewNode;
}

void ListPopFront(List* plist)
{
	assert(plist);
	if (plist->head->next == plist->head)
		return;

	ListNode* cur = plist->head->next, * next = cur->next,*head=plist->head;
	head->next = next;
	next->prev = head;
	free(cur);
}


ListNode* ListFind(List* plist, SLDataType x)
{
	assert(plist);

	ListNode* cur = plist->head->next;
	while (cur != plist->head)
	{
		if (cur->data == x)
		{
			return cur;
		}
		cur = cur->next;
	}
	if (cur == plist->head && cur->data == x)
		return cur;
	else
		return NULL;
}

void ListInsert(ListNode* pos, SLDataType x)
{
	assert(pos);
	ListNode* NewNode = BuyListNode(x);
	ListNode* last = pos->prev;
	last->next = NewNode;
	NewNode->prev = last;
	pos->prev = NewNode;
	NewNode->next = pos;
}


void ListErase(ListNode* pos)
{
	assert(pos);

	ListNode* last = pos->prev, * next = pos->next;

	last->next = next;
	next->prev = last;

	free(pos);
}

void ListDestroy(List* plist)
{
	assert(plist);
	ListNode* cur = plist->head->next;

	while (cur != plist->head)
	{
		ListNode* next = cur->next;
		free(cur);
		cur = next;
	}
	free(plist->head);
	plist->head = NULL;
}