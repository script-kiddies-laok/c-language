#define _CRT_SECURE_NO_WARNINGS 1


#include "ListNode.h"

void test()
{
	List s;
	ListInit(&s);
	ListPushBack(&s, 1);
	/*ListPushBack(&s, 2);
	ListPushBack(&s, 3);
	ListPopBack(&s);
	ListPushFront(&s, 15);
	ListPopFront(&s);*/
	ListNode* test = ListFind(&s, 1);
	//ListInsert(test, 15);
	//ListErase(test);
	ListPrint(&s);

	ListDestroy(&s);
}

int main()
{
	test();
	return 0;
}