#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>
#include <string.h>
#include <assert.h>



//void* my_memmove(void* dest, void* src, size_t count)
//{
//	assert(dest && src);
//	if (dest < src)
//	{
//		while (count--)
//		{
//			*((char*)dest)++ = *((char*)src)++;
//		}
//	}
//	else
//	{
//		while (count--)
//		{
//			*((char*)dest + count) = *((char*)src + count);
//
//		}
//	}
//	return dest;
//}
//
//int main()
//{
//	int arr1[] = { 1,2,3,4,5,6,7,8,9,10 };
//	my_memmove(arr1 + 2, arr1, 16);
//
//	int i = 0;
//	for (i = 0; i < (sizeof(arr1) / sizeof(arr1[0])); i++)
//	{
//		printf("%d ", arr1[i]);
//	}
//	return 0;
//}

//int main()
//{
//	int arr1[] = { 1,2,3,4,5 };
//	int arr2[] = { 1,2,3,2,3 };
//	int ret = memcmp(arr1, arr2, 12);
//	printf("%d\n", ret);
//	return 0;
//}


//int main()
//{
//	int arr1[] = { 1,2,3,4,5,6,7 };
//	int arr[20] = { 0 };
//	memcpy(arr, arr1, sizeof(arr1));
//
//	int i = 0;
//	for (i = 0; i < (sizeof(arr1) / sizeof(arr1[0])); i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	return 0;
//}


//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8,9 };
//	memmove(arr + 2, arr, 16);				//把1，2，3，4放到3，4，5，6上
//
//	int i = 0;
//	for (i = 0; i < (sizeof(arr) / sizeof(arr[0])); i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	return 0;
//}


//int main()
//{
//	int arr1[] = { 1,2,3,4,5 };
//	int arr2[] = { 1,2,3,2,3 };
//	int ret1 = memcmp(arr1, arr2, sizeof(arr1));		//arr1大于arr2，返回大于0的数
//	int ret2 = memcmp(arr1, arr2, 12);					//相等返回0
//	int ret3 = memcmp(arr1, arr2 + 1, 4);				//arr1小于arr2，返回小于0的数
//
//	printf("%d %d %d\n", ret1, ret2, ret3);
//	return 0;
//}


//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
//	memset(arr, '1', sizeof(arr));
//	return 0;
//}

////结构体类型声明
//struct tag				//tag结构体的标签			
//{
//	char name[20];
//	int age;
//}student;				//student是结构体变量，也是全局变量				
//
//
//int main()
//{
//	struct tag student = { "zhangsan",18 };
//	return 0;
//}
//
//struct
//{
//	char name[20];
//	int age;
//}s1,s2;
//
//int main()
//{
//	
//	return 0;
//}


//struct
//{
//	char name[20];
//	int age;
//}s1, * ps;
//
//int main()
//{
//	ps = &s1;
//	return 0;
//}


typedef struct Node
{
	int data;
	struct Node* n;
}Node;

int main()
{
	return 0;
}