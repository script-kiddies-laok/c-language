#define _CRT_SECURE_NO_WARNINGS 1


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>

//
//int main()
//{
//	int* p = (int*)malloc(100);
//	if (p == NULL)
//	{
//		perror("malloc");		//打印错误信息
//	}
//	else
//	{
//		int i = 0;
//		for (i = 0; i < 25; i++)
//		{
//			*(p + i) = i;
//		}
//		for (i = 0; i < 25; i++)
//		{
//			printf("%d ", *(p + i));
//		}
//
//		free(p);		//释放内存
//		p = NULL;
//	}
//	return 0;
//}

//
//int main()
//{
//	int* p = calloc(10, sizeof(int));
//
//	if (p == NULL)
//	{
//		perror("calloc");
//		return 0;
//	}
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", *(p + i));
//	}
//
//	free(p);
//	p = NULL;
//	return 0;
//}


//int main()
//{
//	int* p = malloc(10 * sizeof(int));
//	//printf("%p\n", p);
//	if (p == NULL)
//	{
//		perror("malloc");
//		return 0;
//	}
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		p[i] = i+1;
//	}
//	int* ptr = (int*)realloc(p, 20 * sizeof(int));
//	//printf("%p\n", ptr);
//
//	if (ptr != NULL)
//	{
//		p = ptr;
//		ptr = NULL;
//	}
//	else
//	{
//		perror("realloc");
//		return 0;
//	}
//
//	for (i = 10; i < 20; i++)
//	{
//		p[i] = i + 1;
//	}
//
//	for (i = 0; i < 20; i++)
//	{
//		printf("%d ", p[i]);
//	}
//
//	return 0;
//}


//int main()
//{
//	int* p = (int*)malloc(100);
//	*p = 0;
//	return 0;
//}


//int main()
//{
//	int* p = malloc(10 * sizeof(int));
//	if (p == NULL)
//	{
//		perror("malloc");
//		return 0;
//	}
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		p[i] = i + 1;
//	}
//
//	int* ptr = realloc(p, 10 * sizeof(int));
//	if (ptr != NULL)
//	{
//		p = ptr;
//		ptr = NULL;
//	}
//
//	for (i = 0; i <= 10; i++)
//	{
//		p[i] = i;
//	}
//	for (i = 0; i <= 10; i++)
//	{
//		printf("%d ", p[i]);
//	}
//
//	free(p);
//	p = NULL;
//	return 0;
//}

//
//int main()
//{
//	int* p = (int*)malloc(10 * sizeof(int));
//	if (p == NULL)
//	{
//		perror("malloc");
//		return 0;
//	}
//
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		*p = i;
//		p++;
//	}
//
//	free(p);
//	//......
//	free(p);
//	p = NULL;
//	return 0;
//}

//void test()
//{
//	int* p = (int*)malloc(12);
//	if (p == NULL)
//		return;			//void类型，什么都不返回
//	//假设使用空间
//	if (1)				//如果直接返回，p的空间就没有被释放
//		return;
//	free(p);
//	p = NULL;
//}
//
//int main()
//{
//	test();
//	return 0;
//}

//void GetMemory(char** p)
//{
//	*p = (char*)malloc(100);
//}
//
//void test()
//{
//	char* str = NULL;
//	GetMemory(&str);
//	strcpy(str, "hello world");
//	printf(str);
//
//	free(str);
//	str = NULL;
//}
//
//int main()
//{
//	test();
//	return 0;
//}

//void test(int* p)
//{
//	int i = 4;
//	p=&i;
//	return;
//}
//
//int main()
//{
//	int i = 1;
//	int* p = &i;
//	test(p);
//
//	printf("%d\n", *p);
//	return 0;
//}

//
//int* f1()
//{
//	int x = 10;
//	return &x;
//}
//
//int main()
//{
//	int* p = f1();
//	printf("hehe\n");
//	printf("%d\n", *p);
//	return 0;
//}

//char* GetMemory()
//{
//	char p[] = "hello world";		//返回栈空间地址的问题
//	return p;
//}
//
//void test()
//{
//	char* str = NULL;
//	str = GetMemory();
//	printf(str);
//}
//
//int main()
//{
//	test();
//	return 0;
//}

//void GetMemory(char** p, int num)
//{
//	*p = (char*)malloc(num);
//}
//
//void test()
//{
//	char* str = NULL;
//	GetMemory(&str, 100);
//	strcpy(str, "hello world");
//	printf(str);
//}
//
//int main()
//{
//	test();
//	return 0;
//}
//
//void test()
//{
//	char* str = (char*)malloc(100);
//	if (str == NULL)
//	{
//		;
//	}
//	else
//		strcpy(str, "hello");		//不判断，会报警告
//	free(str);
//	if (str != NULL)
//	{
//		strcpy(str, "world");
//		printf(str);
//	}
//}
//
//int main()
//{
//	test();
//	return 0;
//}

int my_atoi(const char* str)
{
	int count = 0;
	double n = 0;
	const char* p = str;
	int i = 0;

	int arr[100] = { 0 };
	while (*str && *str != '.' && *str > 47 && *str < 57)
	{
		count++;
		str++;
	}
	for (i = 0; i < count; i++)
	{
		arr[i] = p[i] - 48;
		n += arr[i] * pow((double)10, (double)count - (double)i - (double)1); 
	}

	
	
	return (int)n;
}

int main()
{
	char str[] = "113.45";
	//char str[] = "h";
	int n = my_atoi(str);

	//printf("%d\n", *str);
	printf("%d\n", n);
	return 0;
}

//char* my_strncat(char* dest, const char* source, size_t num)
//{
//	assert(dest && source);
//	char* ret = dest;
//	while (*dest)
//	{
//		dest++;
//	}
//	while (num--)
//	{
//		*dest++ = *source++;
//		if ((*dest = *source) == 0)
//			return ret;
//	}
//	*dest = '\0';
//	return ret;
//}
//
//int main()
//{
//	char arr1[50] = "hello";
//	char arr2[] = " world";
//	my_strncat(arr1, arr2, 6);
//
//	printf("%s\n", arr1);
//	return 0;
//}

//char* my_strncpy(char* dest, const char* source, size_t num)
//{
//	assert(dest && source);
//	char* ret = dest;
//	while (num--)
//	{
//		if ((*dest++ = *source++) == 0)
//			return ret;
//	}
//	 
//	*dest = '\0';
//	return ret;
//}
//
//int main()
//{
//	char arr1[50] = { 0 };
//	char arr2[] = "hello";
//	my_strncpy(arr1, arr2, 5);
//
//	printf("%s\n", arr1);
//	return 0;
//}