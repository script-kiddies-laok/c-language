#define _CRT_SECURE_NO_WARNINGS 1

#include "Stack.h"
#include "Queue.h"

void test()
{
	Stack s;
	StackInit(&s);
	StackPush(&s, 1);
	StackPush(&s, 2);
	StackPush(&s, 4);
	StackPush(&s, 5);
	StackPrint(&s);

	/*STDataType test = StackTop(&s);
	printf("%d\n", test);*/
	int TopSize = StackSize(&s);
	printf("%d\n", TopSize);

	int iSize = StackEmpty(&s);
	printf("%d\n", iSize);

	//StackDestroy(&s);
}

void TestQueue()
{
	Queue q;
	QueueInit(&q);
	QueuePushBack(&q, 1);
	QueuePushBack(&q, 2);
	QueuePushBack(&q, 3);
	QueuePushBack(&q, 4);
	//QueuePopFront(&q);
	//QueuePopFront(&q);
	//QueuePopFront(&q);
	//QueuePopFront(&q);
	//QueuePopFront(&q);

	//QueuePrint(&q);

	int size = QueueSize(&q);
	printf("%d\n", size);
	QueuePrint(&q);
	QueuePushBack(&q, 4);
	QueuePrint(&q);
	QueueDestroy(&q);
}

bool isValid(char* s) {
	Stack st;
	StackInit(&st);
	while (*s)
	{
		//��������ջ
		if (*s == '{' || *s == '(' || *s == '[')
		{
			StackPush(&st, *s);
			s++;
		}
		else
		{
			//û��ǰ����
			if (st.top == 0)
			{
				StackDestroy(&st);
				return false;
			}

			char top = StackTop(&st);
			if ((top == '{' && *s != '}')
				|| (top == '(' && *s != ')')
				|| (top == '[' && *s != ']'))
			{
				StackDestroy(&st);
				return false;
			}
			else
			{
				StackPop(&st);
				s++;
			}
		}
	}

	bool ret = st.top == 0 ? true : false;
	//StackDestroy(&st);
	return ret;
}

void test1()
{
	char s[] = { '(',')','\0' };
	bool ret = isValid(s);
	printf("%d\n", ret);
}

int main()
{
	//test();

	//TestQueue();

	test1();
	return 0;
}