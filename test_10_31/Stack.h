#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>


typedef char STDataType;

typedef struct Stack
{
	STDataType* data;
	int top;			//栈顶	
	int capacity;		//容量
}Stack;

//初始化
void StackInit(Stack* s);

//打印
void StackPrint(Stack* s);

//入栈
void StackPush(Stack* s, STDataType x);

//出栈
void StackPop(Stack* s);

//获取栈顶元素
STDataType StackTop(Stack* s);

//获取栈的有效个数
int StackSize(Stack* s);

//检查栈是否为空
bool StackEmpty(Stack* s);

//销毁栈
void StackDestroy(Stack* s);