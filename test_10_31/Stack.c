#define _CRT_SECURE_NO_WARNINGS 1

#include "Stack.h"

void StackInit(Stack* s)
{
	assert(s);
	s->data = NULL;
	s->capacity = s->top = 0;
}

void StackPrint(Stack* s)
{
	assert(s);
	int i = 0;
	for (; i < s->top; i++)
	{
		printf("%d->", s->data[i]);
	}
	printf("NULL\n");
}

void StackPush(Stack* s, STDataType x)
{
	assert(s);

	if (s->capacity == s->top)
	{
		STDataType NewCapacity = s->capacity == 0 ? 4 : s->capacity * 2;
		STDataType* tmp = (STDataType*)realloc(s->data, sizeof(STDataType) * NewCapacity);
		if (tmp == NULL)
		{
			perror("realloc fail");
			exit(-1);
		}
		s->data = tmp;
		free(tmp);
		s->data[(s->top)++] = x;
		s->capacity = NewCapacity;
	}
	else
	{
		s->data[(s->top)++] = x;
	}
}


void StackPop(Stack* s)
{
	assert(s);
	assert(s->top > 0);

	s->top--;
}

STDataType StackTop(Stack* s)
{
	assert(s);
	assert(s->top > 0);
	return s->data[s->top-1];
}

int StackSize(Stack* s)
{
	assert(s);

	return s->top;
}

bool StackEmpty(Stack* s)
{
	assert(s);

	if (s->top == 0)
		return false;
	else
		return true;
}

void StackDestroy(Stack* s)
{
	assert(s);
	free(s->data);
	s->data = NULL;
	s->capacity = s->top = 0;
}