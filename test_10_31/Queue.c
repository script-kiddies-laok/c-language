#define _CRT_SECURE_NO_WARNINGS 1

#include "Queue.h"


void QueueInit(Queue* q)
{
	assert(q);
	q->head = NULL;
	q->tail = NULL;
}

QListNode* BuyQListNode(QDataType x)
{
	QListNode* NewNode = (QListNode*)malloc(sizeof(QListNode));
	if (NewNode == NULL)
	{
		perror("BuyQListNode malloc fail");
		return NULL;
	}

	NewNode->data = x;
	NewNode->next = NULL;
	return NewNode;
}

void QueuePushBack(Queue* q, QDataType x)
{
	assert(q);
	QListNode* NewNode = BuyQListNode(x);
	if (q->head == NULL)
	{
		q->head = q->tail = NewNode;
	}
	else
	{
		q->tail->next = NewNode;
		q->tail = NewNode;
	}
}

void QueuePrint(Queue* q)
{
	assert(q);
	QListNode* NewHead = q->head;
	while (NewHead)
	{
		printf("%d->", NewHead->data);
		NewHead = NewHead->next;
	}
	printf("NULL\n");
}

void QueuePopFront(Queue* q)
{
	assert(q);
	if (q->head == NULL)
	{
		q->tail = NULL;
	}
	else
	{
		QListNode* next = q->head->next;
		free(q->head);
		q->head = next;
	}
}


int QueueSize(Queue* q)
{
	assert(q);
	int size = 0;
	QListNode* NewHead = q->head;
	while (NewHead)
	{
		size++;
		NewHead = NewHead->next;
	}
	return size;
}

QDataType QueueBack(Queue* q)
{
	assert(q);

	return q->tail->data;
}

QDataType QueueFront(Queue* q)
{
	assert(q);
	return q->head->data;
}

bool QueueEmpty(Queue* q)
{
	assert(q);
	if (q->head == NULL)
		return true;
	else
		return false;
}

void QueueDestroy(Queue* q)
{
	assert(q);
	QListNode* cur = q->head;
	while (cur)
	{
		QListNode* next = cur->next;
		free(cur);
		cur = next;
	}
	q->head = q->tail = NULL;
}