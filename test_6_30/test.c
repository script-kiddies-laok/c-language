#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>

//int main()
//{
//	int a = 0;
//	int n = 0;
//	int i = 0;
//	int sum = 0;
//	int tmp = 0;
//
//
//	scanf("%d %d", &a, &n);
//	for (i = 0; i < n; i++)
//	{
//		tmp = tmp * 10 + a;
//		sum += tmp;
//	}
//	printf("%d\n", sum);
//
//	return 0;
//}

//int main()
//{
//	int i = 0;
//
//	int line = 0;
//	scanf("%d", &line);
//	for (i = 0; i < line; i++)
//	{
//		//先打印空格
//		int j = 0;
//		for (j = 0; j < line - 1 - i; j++)
//		{
//			printf(" ");
//		}
//		for (j = 0; j < 2 * i + 1; j++)
//		{
//			printf("*");
//		}
//		printf("\n");
//	}
//	for (i = 0; i < line - 1; i++)
//	{
//		for (int j = 0; j <= i; j++)
//		{
//			printf(" ");
//		}
//		for (int j = 0; j < 2 * (line - 1 - i) - 1; j++)
//		{
//			printf("*");
//		}
//		printf("\n");
//	}
//	return 0;
//}

//int main()
//{
//	int n = 0;
//	int x=0;
//	scanf("%d", &n);
//	x = n;
//	int count = 0;
//	while (n)
//	{
//		n = n / 2;
//		count = count + n;
//	}
//	
//	printf("%d\n", x+count);
//	return 0;
//}

//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	printf("%d\n", (2 * n) - 1);
//	return 0;
//}

//int main()
//{
//	int a = 129;
//	char* p = (char*)&a;
//	printf("%d", *p);
//	return 0;
//}

//int main()
//{
//	char a = -1;
//	//-1
//	//10000000000000000000000000000001	原码
//	//11111111111111111111111111111110	反码
//	//11111111111111111111111111111111	补码
//	//11111111	截断
//
//	//整形提升
//	//11111111111111111111111111111111	补码
//	//11111111111111111111111111111110	反码
//	//10000000000000000000000000000001	原码
//	signed char b = -1;
//	unsigned char c = -1;
//	//10000000000000000000000000000001
//	//11111111111111111111111111111110
//	//11111111111111111111111111111111
//	//11111111	截断
//
//	//整形提升
//	//00000000000000000000000011111111		255
//	printf("a=%d,b=%d,c=%d", a,b,c);
//	return 0;
//}

//int main()
//{
//	char a = -128;
//	//10000000000000000000000010000000	原码
//	//11111111111111111111111101111111	反码
//	//11111111111111111111111110000000	补码
//	//10000000	截断
//
//	//整形提升
//	//11111111111111111111111110000000	补码
//	printf("%u\n", a);
//	//%u	认为内存存的就是补码
//
//	return 0;
//}

//int main()
//{
//	int a = -20;
//	//10000000000000000000000000010100
//	//11111111111111111111111111101011
//	//11111111111111111111111111101100
//	unsigned int b = 10;
//	//00000000000000000000000000001010
//	//11111111111111111111111111110110
//	//11111111111111111111111111110101
//	//10000000000000000000000000001010
//	printf("%d\n", a + b);
//	return 0;
//}
//#include <windows.h>
//int main()
//{
//	unsigned int i;
//	for (i = 9; i >= 0; i--)
//	{
//		printf("%u\n", i);
//		Sleep(1000);
//	}
//	//容易死循环
//	return 0;
//}

#include <string.h>

//int main()
//{
//	char a[1000];
//	int i;
//	for (i = 0; i < 1000; i++)
//	{
//		a[i] = -1 - i;
//	}
//	printf("%d\n", strlen(a));
//	return 0;
//}

//unsigned char i = 0;
//
//int main()
//{
//
//	for (i = 0; i < 256; i++)
//	{
//		printf("hello world\n");
//	}
//	return 0;
//}

//int main()
//{
//	unsigned char a = 100;
//	//00000000000000000000000011001000
//	//00000000000000000000000001100100
//	//00000000000000000000000000101100
//	unsigned char b = 200;
//	unsigned char c = 0;
//	c = a + b;
//	//c越界 
//	//
//	printf("%d %d", a + b, c);
//	return 0;
//}

//int main()
//{
//	unsigned int a = 0x1234;
//	return 0;
//}

//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	//打印n行
//	for (int i = 1; i <= n; i++)
//	{
//		//打印i个数字
//		for (int j = 1; j <= i; j++)
//		{
//			printf("1 ");
//			//打印中间数字
//
//		}
//		printf("\n");
//	}
//	return 0;
//}

//int main()
//{
//	int* arr[10] = { 1,2,3,4,5,6,7,8,9,0 };
//	printf("%d", &arr[9]-&arr[0]);
//	return 0;
//}

//int main()
//{
//	char ch = 'a';
//	char* pc = &ch;
//
//	char* p = "abcdef";
//	return 0;
//}

//int main()
//{
//	char str1 = "hello bit.";
//	char str2 = "hello bit.";
//	char* str3 = "hello bit.";
//	char* str4 = "hello bit.";
//
//	if (str1 == str2)
//		printf("1\n");
//	else
//		printf("0\n");
//
//	if (str3 == str4)
//		printf("1\n");
//	else
//		printf("0\n");
//	return 0;
//}

//int main()
//{
//	const char* p = "abcdef";
//	return 0;
//}

//int main()
//{
//	int* arr[10];		//整型指针
//	char** ch[20];		//
//	return 0;
//}

//int main()
//{
//	int a = 10;
//	int* p = &a;
//	char ch = 'w';
//	char* pc = &ch;
//
//	int arr[10] = { 0 };
//	int(*pa)[10]=&arr;//数组的地址
//	//&arr	数组名是整个数组，取出的是数组的地址
//	//&arr arr对比
//
//	char ch[5];
//	char(*p3)[5] = &ch;
//
//	printf("%p %p %p \n", &arr, arr,pa[0]);
//	return 0;
//}

//int main()
//{
//	int arr[10] = { 1,2,3,4,5 };
//	int* p1 = arr;
//	int(*p2)[10] = &arr;
//	printf("%p\n", p1);
//	printf("%p\n", p1+1);
//	//移动4个字节
//
//	printf("%p\n", p2);
//	printf("%p\n", p2+1);
//	//移动整个数组的地址
//
//	return 0;
//}

//int main()
//{
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,0 };
//	int* p = arr;
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	for (int i = 0; i < sz; i++)
//	{
//		printf("%d ", *(p + i));
//	}
//	return 0;
//}

//int main()
//{
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,0 };
//	int(*p)[10] = &arr;
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	for (int i = 0; i < sz; i++)
//	{
//		printf("%d ", (*p)[i]);
//		//printf("%d",p[0][i]);
//		//*((*p)+i)
//	}
//
//	return 0;
//}

void print(int arr[3][5], int a, int b)
{
	for (int i = 0; i < a; i++)
	{
		for (int j = 0; j < b; j++)
		{
			printf("%d ", arr[i][j]);
		}
		printf("\n");
	}
}

//void print2(int(*p)[5],int x,int y)
//{
//	for (int i = 0; i < 3; i++)
//	{
//		for (int j = 0; j < 5; j++)
//		{
//			printf("%d ", (*(p + i))[j]);
//			//*(*(p+i)+j)
//		}
//		printf("\n");
//	}
//}
//
//int main()
//{
//	int arr[3][5] = { {1,2,3,4,5},{2,3,4,5,6},{3,4,5,6,7} };
//	//二维数组传参，数组名也是首元素的地址，二维数组的首元素是第一行
//	//传过去的就是第一行的地址
//
//	print(arr, 3, 5);
//	print2(arr, 3, 5);
//	return 0;
//}

int main()
{
	int arr[5] = { 1,2,3,4,5 };
	int(*p)[5] = &arr;
	for (int i = 0; i < 5; i++)
	{
		printf("%d ", *(*p+i));
	}
	return 0;
}