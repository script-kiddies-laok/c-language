#pragma once

#include <iostream>
#include <assert.h>
using std::cout;
using std::cin;
using std::endl;
class Date
{
public:
	// 构造函数
	Date(int year = 2021, int month = 11, int day = 29);
	
	void Print();
	// 内置类型不需要写析构和拷贝构造

	// 拷贝构造函数
	Date(const Date& d);

	//析构函数
	~Date()
	{
		_year = 0;
		_month = 1;
		_day = 1;
	}

	Date& operator+=(int day);
	Date operator+(int day);

	Date& operator-=(int day);
	Date operator-(int day);

	//前置++
	Date& operator++();
	//后置++
	Date operator++(int);

	Date& operator--();
	Date operator--(int);

	bool operator>(const Date& d);

	bool operator==(const Date& d);

	bool operator>=(const Date& d);

	bool operator<(const Date& d);
	
	bool operator<=(const Date& d);

	bool operator!=(const Date& d);

	int operator-(const Date& d);

private:
	int _year;
	int _month;
	int _day;
};