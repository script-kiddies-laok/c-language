#define _CRT_SECURE_NO_WARNINGS 1

#include "Date.h"

int main()
{
	/*Date d1;
	d1.Print();
	Date d2(d1);
	d2.Print();*/

	//Date d2(d1);
	//d2.Print();

	//Date d1(2021, 2, 8);
	//d1.Print();

	//Date d2(2020, 10, 31);
	//d2 += 100;
	//d2.Print();
	//d1 += 1;
	//d1.Print();

	//Date d2 = d1 + 100;
	//d2.Print();

	//d1 -= 100;
	//d1.Print();

	//d2 = d2 - 100;
	//d2.Print();
	//++d1;
	//d1.Print();

	//Date d2(d1++);
	//d2.Print();

	//d1.Print();

	Date d1(2021, 11, 22);
	Date d2(d1 + 10);
	//int ret = d1 > d2;
	//cout << ret << endl;

	//ret = d1 == d2;
	//cout << ret << endl;

	int ret = d1 - d2;
	cout << ret << endl;
	return 0;
}