#define _CRT_SECURE_NO_WARNINGS 1

#include "Date.h"



inline int GetMonthDay(int year, int month)
{
	static int iDayArray[13] = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
	int day = iDayArray[month];
	if ((month == 2) && ((year % 4 == 0 && year % 100 != 0) || year % 100 == 0))
	{
		day = 29;
	}
	return day;
}

Date::Date(int year, int month, int day)
{
	if (year >= 0
		&& month > 0 && month < 13
		&& day>0 && day <= GetMonthDay(year, month))
	{
		_year = year;
		_month = month;
		_day = day;
	}
	else
	{
		cout << "异常天数" << endl;
		cout << year << "年" << month << "月" << day << "日" << endl;
	}
}

Date::Date(const Date& d)
{
	_year = d._year;
	_month = d._month;
	_day = d._day;
}

void Date::Print()
{
	cout << _year << "年" << _month << "月" << _day << "日" << endl;
}

Date& Date::operator+=(int day)
{
	_day += day;
	while (_day > GetMonthDay(_year, _month))
	{
		_day -= GetMonthDay(_year, _month);
		_month++;
		if (_month > 12)
		{
			_year++;
			_month = 1;
		}
	}
	return *this;
}


Date Date::operator+(int day)
{
	Date ret = *this;
	ret += day;
	return ret;
}

Date& Date::operator-=(int day)
{
	if (day >= _day)
	{
		while (day >= _day)
		{
			day -= _day;
			_month--;
			if (_month == 0)
			{
				_month = 12;
				_year--;
				_day = GetMonthDay(_year, _month);
			}
			_day = GetMonthDay(_year, _month);
		}
		_day -= day;
	}
	else
	{
		_day -= day;
	}
	return *this;
}

Date Date::operator-(int day)
{
	Date ret = *this;
	ret -= day;
	return ret;
}

Date& Date::operator++()
{
	*this += 1;
	return *this;
}

Date Date::operator++(int)
{
	Date ret(*this);
	*this += 1;
	return ret;
}

Date& Date::operator--()
{
	*this -= 1;
	return *this;
}

Date Date::operator--(int)
{
	Date ret(*this);
	*this -= 1;
	return ret;
}

bool Date::operator>(const Date& d)
{
	if (_year >= d._year)
	{
		if (_year == d._year)
		{
			if (_month > d._month)
				return true;
			else if (_month == d._month)
			{
				if (_day > d._day)
					return true;
				else
					return false;
			}
			else
				return false;
		}
		return true;
	}
	else
	{
		return false;
	}
}

bool Date::operator==(const Date& d)
{
	return _year == d._year
		&& _month == d._month
		&& _day == d._day;
}

bool Date::operator>=(const Date& d)
{
	return *this > d || *this == d;
}

bool Date::operator<(const Date& d)
{
	if (*this >= d == true)
		return false;
	else
		return true;
}

bool Date::operator<=(const Date& d)
{
	return *this < d || *this == d;
}


bool Date::operator!=(const Date& d)
{
	return *this == d ? false : true;
}

int Date::operator-(const Date& d)
{
	int sum = 0;
	Date ret(d);
	if (*this > ret)
	{
		while (*this != ret)
		{
			ret++;
			sum ++ ;
		}
		return sum;
	}
	else
	{
		while (*this != ret)
		{
			(*this)++;
			sum++;
		}
		return -sum;
	}
}
