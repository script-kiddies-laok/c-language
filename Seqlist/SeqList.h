#pragma once


#include <stdio.h>
#include <assert.h>
#include <stdlib.h>

typedef int SeqDataType;

typedef struct SeqList
{
	SeqDataType* data;		//用来存放数据
	int size;				//有效个数
	int capacity;			//容量
}SeqList;


//初始化
void SeqListInit(SeqList* ps);

//销毁
void SeqListDestory(SeqList* ps);

//打印
void SeqListPrint(SeqList* ps);

//尾插
void SeqListPushBack(SeqList* ps, SeqDataType x);

//尾删
void SeqListPopBack(SeqList* ps);

//头插
void SeqListPushFront(SeqList* ps, SeqDataType x);

//头删
void SeqListPopFront(SeqList* ps);

//查找
int SeqListFind(SeqList* ps, SeqDataType x);

//在pos位置插入
void SeqListInsert(SeqList* ps, int pos, SeqDataType x);

//删除pos位置的数据
void SeqListErase(SeqList* ps, int pos);

//修改pos位置的数据
void SeqListModify(SeqList* ps, int pos, SeqDataType x);