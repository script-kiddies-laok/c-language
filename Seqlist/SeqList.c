#define _CRT_SECURE_NO_WARNINGS 1

#include "SeqList.h"


void SeqListInit(SeqList* ps)
{
	assert(ps);

	ps->data = NULL;
	ps->size = ps->capacity = 0;
}

void SeqListDestory(SeqList* ps)
{
	assert(ps);

	free(ps->data);
	ps->size = ps->capacity = 0;
}

void SeqListPrint(SeqList* ps)
{
	assert(ps);

	for (int i = 0; i < ps->size; i++)
	{
		printf("%d ", ps->data[i]);
	}

	printf("\n");
}

void SeqCheckCapacity(SeqList* ps)
{
	if (ps->size == ps->capacity)
	{
		int NewCapacity = ps->capacity == 0 ? 4 : ps->capacity * 2;
		SeqDataType* NewData = (SeqDataType*)realloc(ps->data,NewCapacity * sizeof(int));	//���·���
		if (NewData == NULL)
		{
			perror("CheckCapacity malloc");
			exit(-1);
		}

		ps->data = NewData;
		ps->capacity = NewCapacity;
	}
}

void SeqListPushBack(SeqList* ps, SeqDataType x)
{
	//assert(ps);
	/*SeqCheckCapacity(ps);

	ps->data[ps->size] = x;
	ps->size++;*/


	SeqListInsert(ps, ps->size, x);
}


void SeqListPopBack(SeqList* ps)
{
	//assert(ps);
	//assert(ps->size > 0);

	//ps->size--;

	SeqListErase(ps, ps->size - 1);
}


void SeqListPushFront(SeqList* ps, SeqDataType x)
{
	//assert(ps);
	//SeqCheckCapacity(ps);

	//int end = ps->size - 1;
	//for (int i = 0; i < ps->size; i++)
	//{
	//	ps->data[end + 1] = ps->data[end];
	//	end--;
	//}

	//ps->data[0] = x;
	//ps->size++;
	SeqListInsert(ps, 0, x);
}


void SeqListPopFront(SeqList* ps)
{
	//assert(ps);

	//assert(ps->size > 0);
	//SeqCheckCapacity(ps);
	//int start = 0;
	//for (int i = 0; i < ps->size - 1; i++)
	//{
	//	ps->data[start] = ps->data[start + 1];
	//}

	//ps->size--;
	SeqListErase(ps, 0);
}

int SeqListFind(SeqList* ps, SeqDataType x)
{
	assert(ps);

	for (int i = 0; i < ps->size; i++)
	{
		if (ps->data[i] == x)
		{
			return i;
		}
	}

	return -1;
}


void SeqListInsert(SeqList* ps, int pos, SeqDataType x)
{
	assert(ps);
	assert(pos >= 0 && pos <= ps->size);

	SeqCheckCapacity(ps); 

	int end = ps->size - 1;
	while (end >= pos)
	{
		ps->data[end+1] = ps->data[end];
		end--;
	}

	ps->data[pos] = x;
	ps->size++;	
}

void SeqListErase(SeqList* ps, int pos)
{
	assert(ps);
	assert(pos >= 0 && pos <= ps->size);

	int start = pos;
	for (int i = start; i <= ps->size; i++)
	{
		ps->data[start] = ps->data[start + 1];
		start++;
	}

	ps->size--;
}


void SeqListModify(SeqList* ps, int pos, SeqDataType x)
{
	assert(ps);
	assert(pos >= 0 && pos < ps->size);

	ps->data[pos] = x;
}