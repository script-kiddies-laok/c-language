#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>
#include <stdlib.h>

void Swap(int* a, int* b)
{
	int tmp = *a;
	*a = *b;
	*b = tmp;
}

void myPrint(int* a, int n)
{
	for (int i = 0; i < n; i++)
	{
		printf("%d ", a[i]);
	}
	printf("\n");
}

void ShellSoft(int* a, int n)
{
	
	int gap = n;
	while (gap > 1)
	{
		gap = (gap / 3 + 1);
		for (int i = 0; i < n - gap; i++)
		{
			int end = i;
			int tmp = a[end + gap];

			while (end >= 0)
			{
				if (tmp < a[end])
				{
					a[end + gap] = a[end];
					end -= gap;
				}
				else
				{
					break;
				}
			}
			a[end + gap] = tmp;
		}
	}
}

void SelectSoft(int* a, int n)
{
	int left = 0, right = n - 1;
	while (left < right)
	{
		int Min = left;
		int Max = right;
		
		for (int i = left; i < right; i++)
		{
			if (a[i] < a[Min])
			{
				Min = i;
			}
			if (a[i] > a[Max])
			{
				Max = i;
			}
		}
		Swap(&a[left], &a[Min]);
		if (left == Max)
		{
			Max = Min;
		}
		Swap(&a[right], &a[Max]);
		left++;
		right--;
	}
}

void AdjustDown(int* a, int parent, int n)
{
	int child = 2 * parent + 1;
	while (child < n)
	{
		//建大堆
		if (child + 1 < n && a[child + 1] > a[child])
		{
			child++;
		}

		if (a[parent] < a[child])
		{
			Swap(&a[parent], &a[child]);
		}
		parent = child;
		child = 2 * parent + 1;
	}
}

void HeapSort(int* a, int n)
{
	//建堆
	for (int i = (n - 1 - 1) / 2; i >= 0; i--)
	{
		AdjustDown(a, i, n);
	}
	int end = n-1;
	while (end>0)
	{
		Swap(&a[0], &a[end]);
		AdjustDown(a, 0, end);
		end--;
	}
}

void InsertSort(int* a, int n)
{
	for (int i = 0; i < n - 1; i++)
	{
		int end = i;
		int tmp = a[end + 1];
		while (end >= 0)
		{
			if (a[end] > tmp)
			{
				a[end + 1] = a[end];
				end--;
			}
			else
			{
				break;
			}
		}

		a[end + 1] = tmp;
	}
}

void TestShellSort(int* a, int n)
{
	int gap = n;
	while (gap > 1)
	{
		gap = gap / 3 + 1;
		for (int i = 0; i < n - gap; i++)
		{
			int end = i;
			int tmp = a[end + gap];
			while (end >= 0)
			{
				if (a[end] > tmp)
				{
					a[end + gap] = a[end];
					end -= gap;
				}
				else
				{
					break;
				}
			}
			a[end + gap] = tmp;
		}
	}
}

void TestSelectSort(int* a, int n)
{
	int left = 0, right = n - 1;
	while (left < right)
	{
		int min = left;
		int max = right;
		for (int i = left; i < right; i++)
		{
			if (a[i] < a[min])
				min = i;
			if (a[i] > a[max])
				max = i;
		}
		Swap(&a[left], &a[min]);
		if (left == max)
		{
			max = min;
		}
		Swap(&a[right], &a[max]);

		left++;
		right--;
	}
}

void BubbleSort(int* a, int n)
{
	for (int i = 0; i < n - 1; i++)
	{
		int flag = 1;
		for (int j = 0; j < n - 1 - i; j++)
		{
			if (a[j] > a[j + 1])
			{
				Swap(&a[j], &a[j + 1]);
				flag = 0;
			}
		}
		if (flag == 1)
			break;
	}
}

int GetMinIndex(int* a, int left, int right)
{
	int min = (left + right) >> 1;
	//选中间的
	if (a[left] > a[min])
	{
		if (a[min] > a[right])
		{
			return min;
		}
		else if (a[left] < a[right])
		{
			return left;
		}
		else
		{
			return right;
		}
	}
	else
	{	//a[left]<a[min]
		if (a[right] > a[min])
		{
			return left;
		}
		else if (a[left] > a[right])
		{
			return left;
		}
		else
		{
			return min;
		}
	}
}

int PartSort1(int* a, int begin, int end)
{
	int midIndex = GetMinIndex(a, begin, end);
	Swap(&a[begin], &a[midIndex]);

	int keyi = begin;
	while (begin < end)
	{
		while (begin < end && a[end] >= a[keyi])
		{
			end--;
		}
		while (begin < end && a[begin] <= a[keyi])
		{
			begin++;
		}
		Swap(&a[begin], &a[end]);

	}
	//最后剩两个数的时候一定要保证让end先走，否则第一个数和第二个数就想换了
	Swap(&a[keyi], &a[begin]);
	return begin;
}

int PartSort2(int* a, int begin, int end)
{
	int midIndex = GetMinIndex(a, begin, end);
	Swap(&a[begin], &a[midIndex]);

	int keyi = a[begin];
	while (begin < end)
	{
		while (begin < end && a[end] >= keyi)
		{
			end--;
		}
		//放到左边的坑位上
		a[begin] = a[end];
		while (begin < end && a[begin] <= keyi)
		{
			begin++;
		}
		//放到右边的坑位上
		a[end] = a[begin];
	}

	a[begin] = keyi;
	return begin;
}

int PartSort3(int* a, int begin, int end)
{
	int midIndex = GetMinIndex(a, begin, end);
	Swap(&a[begin], &a[midIndex]);

	int prev = begin;
	int cur = begin + 1;
	int keyi = begin;
	while (cur <= end)
	{
		if (a[cur]<a[keyi] && prev++ != cur)
		{
			Swap(&a[prev], &a[cur]);
		}
		cur++;
	}

	Swap(&a[prev], &a[begin]);
	return prev;
}

void QuickSoft(int* a, int begin, int end)
{
	if (begin >= end)
		return;

	int keyi = PartSort3(a, begin, end);

	QuickSoft(a, begin, keyi - 1);
	QuickSoft(a, keyi + 1, end);

}



//int main()
//{
//	int arr[] = { 9,2,1,6,8 };
//	int size = sizeof(arr) / sizeof(arr[0]);
//	//ShellSoft(arr, size);
//	//SelectSoft(arr, size);
//	//HeapSort(arr, size);
//
//	//InsertSort(arr, size);
//	//TestShellSort(arr, size);
//	//TestSelectSort(arr, size);
//	//BubbleSort(arr, size);
//	QuickSoft(arr, 0, size - 1);
//	myPrint(arr, size);
//	return 0;
//}


int main()
{
	int n, min = 0, max = 0;
	/*while (~scanf("%d", &n) && n)
	{
		if (n > 0)
		{
			max = max < n ? max : n;
			if (max == 0)
				max = n;
		}
		else
		{
			min = min > n ? min : n;
			if (min == 0)
				min = n;
		}
	}*/
	int i = 1234567;
	printf("%d\n", i);
	printf("min = %d, max = %d\n", min, max);
	return 0;

}