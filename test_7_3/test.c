#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>
#include <limits.h>
#include <stddef.h>

//int main()
//{
//	//printf("%d-%d", CHAR_MIN, CHAR_MAX);
//
//	int a = -1;
//	//100000000000000000000001
//	//111111111111111111111110
//	//111111111111111111111111
//	//
//	a = a >> 1;
//	printf("%d", a);
//	return 0;
//}

//INT_MAX
//typedef
//size_t

//int count_bits(unsigned int n)
//{
//	int count=0;
//	while (n) {
//		if (n & 1u)
//			count++;
//		n >>= 1;
//	}
//	return count;
//}
//
//int int_bits(void)
//{
//	return count_bits(~0u);
//}
//
//void print_bits(unsigned int nx)
//{
//	int i;
//	for (i = int_bits() - 1; i >= 0; i--)
//		putchar(((nx >> i) & 1u) ? '1' : '0' );
//}
//
//int main()
//{
//	unsigned int nx;
//	scanf("%u", &nx);
//
//	print_bits(nx);
//
//
//	return 0;
//}

//int main()
//{
//	int a = 1234;
//
//	for (int i = 1; i < 10; i++)
//		printf("a>>%d=%d\n", i, a >> i);
//
//
//	return 0;
//}

//unsigned rrotate(unsigned x, int n)
//{
//	x >>= n;
//	return x ;
//}
//
//unsigned lrotate(unsigned x, int n)
//{
//	x <<= n;
//	return x ;
//}
//
//int main()
//{
//	unsigned x;	//被移到的数字
//	int n;		//移动的位数
//	
//	scanf("%d%d", &x,&n);
//
//	printf("%u右移%d:%u\n", x, n, rrotate(x, n));
//	printf("%u左移%d:%u\n", x, n, lrotate(x, n));
//
//	return 0;
//}


//int main()
//{
//	int a;
//	double b;
//	printf("%d",sizeof(a + b));
//	return 0;
//}

//#define sqr(x) ((x) * (x))
//
//int main()
//{
//	int nx = 2 ;
//	double dx = 3.3 ;
//	
//	printf("%d\n", sqr(nx) );
//
//	printf("%lf\n", sqr(dx) );
//
//	return 0;
//}

//#define diff(x,y) ((x)-(y))
//
//int main()
//{
//	int a;
//	int b;
//	scanf("%d%d", &a, &b);
//
//	printf("%d\n", diff(a, b));
//	return 0;
//}


//求最大公约数
//int gcdf(int nx, int ny)
//{
//	return ny == 0 ? nx : gcdf(ny, nx % ny);
//}
//
////交换次序	--	nx>ny
//int gcd(int nx, int ny)
//{
//	return nx > ny ? gcdf(nx, ny) : gcdf(ny, nx);
//}
//
//int main()
//{
//	int nx, ny;
//
//	scanf("%d %d", &nx, &ny);
//
//	printf("最大公约数是%d\n", gcd(nx, ny));
//
//	return 0;
//}

//int fact(int n)
//{
//	int sum = 1;
//	for (int i = 1; i <= n; i++)
//		sum *= i;
//	return sum;
//}
//
//int main()
//{
//	int n;
//
//	scanf("%d", &n);
//
//	printf("%d\n", fact(n));
//	return 0;
//}

//int main()
//{
//	int i, ch;
//	int cnt[10] = { 0 };
//
//	while (1)
//	{
//		ch = getchar();
//		if (ch == EOF)
//			break;
//
//		switch (ch)
//		{
//		case '0':
//			cnt[0]++;
//			break;
//		case '1':
//			cnt[1]++;
//			break;
//		case '2':
//			cnt[2]++;
//			break;
//		case '3':
//			cnt[3]++;
//			break;
//		case '4':
//			cnt[4]++;
//			break;
//		case '5':
//			cnt[5]++;
//			break;
//		case '6':
//			cnt[6]++;
//			break;
//		case '7':
//			cnt[7]++;
//			break;
//		case '8':
//			cnt[8]++;
//			break;
//		case '9':
//			cnt[9]++;
//			break;
//		}
//	}
//
//	for(i=0;i<10;i++){
//		printf("%d\n", cnt[i]);
//	}
//
//	return 0;
//}


//int main()
//{
//	int ch;
//	int count = 0;
//	while ((ch = getchar()) != EOF)
//	{
//		putchar(ch);
//		count++;
//	}
//
//	printf("%d\n", count);
//	return 0;
//}


//int main()
//{
//	char str[] = "ABC\0DEF";
//	printf("%s\n", str);
//	return 0;
//}

//unsigned str_length(const char str[])
//{
//	unsigned len = 0;
//	while (str[len]!='\0')
//		len++;
//	return len;
//}
//
//int main()
//{
//	char str[1000];
//
//	printf("请输入字符串:");
//	scanf("%s", &str);
//
//	printf("字符串%s的长度为%u\n", str, str_length(str));
//
//	return 0;
//}
//#include <string.h>
//int str_char(char str[], char ch,int sz)
//{
//	int len = 0;
//	for (; len <= sz - 1; len++)
//	{
//		if (str[len] == ch)
//			return len;
//	}
//	return -1;
//}
//
//int main()
//{
//	char str[1000];
//	char ch = 'c';
//
//	scanf("%s", &str);
//	int sz = strlen(str);
//	printf("%d\n", sz);
//	printf("%d\n", str_char(str, ch,sz));
//	return 0;
//}




//#include <string.h>
//#define NUM 3
//
//int main(void)
//{
//	int i;
//	char s[NUM][128];
//	for (i = 0; i < NUM; i++)
//	{
//		printf("s[%d]:", i);
//		scanf("%s", s[i]);
//		if (strcmp(s[i], "$$$$$") == 0)//代码显示问题。。。
//			break;
//	}
//	for (i = 0; i < NUM; i++)
//	{
//		if (strcmp(s[i], "$$$$$") == 0)//代码显示问题。。。
//			break;
//		else
//			printf("s[%d] = \"%s\"\n", i, s[i]);
//	}
//
//	return 0;
//}


//#include <string.h>
//
//int str_char(char str[], char ch, int sz)
//{
//	int len = 0;
//	int count=0;
//	for (; len <= sz - 1; len++)
//	{
//		if (str[len] == ch)
//			count++;
//	}
//	if (count)
//		return count;
//	else
//		return -1;
//
//}
//
//int main()
//{
//	char str[1000];
//	char ch = 'c';
//
//	scanf("%s", &str);
//	int sz = strlen(str);
//	printf("%d\n", sz);
//	printf("%d\n", str_char(str, ch, sz));
//	return 0;
//}

//#include <string.h>
//
//void put_string(const char str[],int sz)
//{
//	int i = sz-1;
//
//	while (i>0) {
//		putchar(str[i--]);
//
//	}
//}
//
//int main()
//{
//	char str[1000];
//	
//	scanf("%s", &str);
//
//	int sz = strlen(str);
//	printf("%d\n", sz);
//	put_string(str,sz);
//	putchar('\n');
//	return 0;
//}

//#include <ctype.h>
//
//void str_toupper(char str[])
//{
//	int i = 0;
//	while (str[i]) {
//		str[i] = toupper(str[i]);
//		i++;
//	}
//}
//
//void str_tolower(char str[])
//{
//	int i = 0;
//	while (str[i]) {
//		str[i] = tolower(str[i]);
//		i++;
//	}
//}
//
//int main()
//{
//	char str[1000];
//
//	scanf("%s", str);
//
//	str_toupper(str);
//	printf("%s\n", str);
//
//	str_tolower(str);
//	printf("%s\n", str);
//	return 0;
//}

//void null_string(char str[])
//{
//	str[0] = '\0';
//}
//
//void del_digit(char str[])
//{
//	int i, j=0;
//	for (i = 0; str[i] != '\0'; i++)
//	{
//		if (str[i] < '0' || str[i]>'9')
//			str[j++] = str[i];
//	}
//	str[j] = '\0';
//}
//
//int main()
//{
//	char str[1000];
//
//	scanf("%s", str);
//
//	del_digit(str);
//
//	printf("%s\n", str);
//
//	return 0;
//}

//void sort3(int* a, int* b, int* c)
//{
//	int tmp;
//	if (*a > *b)
//	{
//		tmp = *a;
//		*a = *b;
//		*b = tmp;
//	}
//	if (*b > *c)
//	{
//		tmp = *b;
//		*b = *c;
//		*c = tmp;
//	}
//	if (*a > *c)
//	{
//		tmp = *a;
//		*a = *c;
//		*c = tmp;
//	}
//}
//
//int main()
//{
//	int a, b, c;
//
//	scanf("%d%d%d", &a, &b, &c);
//
//	sort3(&a, &b, &c);
//
//	printf("%d %d %d\n", a, b, c);
//	return 0;
//}

//int main()
//{
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,0 };
//
//	int* ptr = &arr[0];
//	printf("%d\n", ptr[2]);
//	return 0;
//}

//#include <stdlib.h>
//
//int main()
//{
//	int retry=0;
//
//	printf("%d\n", RAND_MAX);
//	printf("%d\n", rand());
//	return 0;
//}

//#include <time.h>
//#include <stdlib.h>
//
//int main()
//{
//	int n = 0;		//用户输入的数字
//	int ans;		//存储随机数
//	const int max_stage = 10;
//	int remain = max_stage;
//
//	srand((unsigned)time(NULL));	//随机种子
//	ans = rand() % 1000;	//0-999的数字，随机
//
//	do
//	{
//		printf("剩余次数%d\n", remain);
//		scanf("%d", &n);
//		remain--;
//
//		
//		if (n > ans)
//			printf("大了\n");
//		else if (n < ans)
//			printf("小了\n");
//	} while (n != ans&&remain>0);
//
//	if (n == ans) {
//		printf("恭喜你\n");
//		printf("剩余次数%d\n", remain);
//	}
//	return 0;
//}

//#include <time.h>
//#include <stdlib.h>
//
//int main()
//{
//	int n = 0;		//用户输入的数字
//	int ans;		//存储随机数
//	const int max_stage = 10;
//	int remain = max_stage;
//
//	srand((unsigned)time(NULL));	//随机种子
//	ans = -999+rand() % 2000;	//0-999的数字，随机
//
//	do
//	{
//		printf("剩余次数%d\n", remain);
//		scanf("%d", &n);
//		remain--;
//
//		
//		if (n > ans)
//			printf("大了\n");
//		else if (n < ans)
//			printf("小了\n");
//	} while (n != ans&&remain>0);
//
//	if (n == ans) {
//		printf("恭喜你\n");
//		printf("剩余次数%d\n", remain);
//	}
//	return 0;
//}

//int main()
//{
//	int a = 10;
//	int* d = &a;
//	d = 10 + *d;
//	printf("%d\n", *d);
//	return 0;
//}

//int main()
//{
//	char ch = 'a';
//	char* cp = &ch;
//	printf("%c\n", *++cp);
//	return 0;
//}

//int main()
//{
//	for (int i = 1; i < 100; i += 2)
//		printf("%d ", i);
//	return 0;
//}

//int main()
//{
//	int i = 1;
//	while (i <= 10)
//	{
//		if (i == 5)
//			continue;
//		printf("%d ", i);
//		i++;
//	}
//	return 0;
//}


//int main()
//{
//	int ch = 0;
//	while ((ch = getchar()) != EOF)
//	{
//		putchar(ch);
//	}
//
//	return 0;
//}

//int main()
//{
//    int i, j;
//    int n;
//    while (scanf("%d", &n) != EOF)
//    {
//        for (i = 0; i < n; i++)
//        {
//            for (j = 0; j <= n - i; j++)
//                printf("* ");
//            printf("\n");
//        }
//    }
//    return 0;
//}

//int main()
//{
//    int i, j;
//    int n;
//    while (scanf("%d", &n) != EOF)
//    {
//        for (i = 0; i < n; i++)
//        {
//            for (j = 0; j <= n - i; j++)
//                printf("* ");
//            printf("\n");
//        }
//        for (i = 0; i <= n; i++)
//        {
//            for (j = 0; j <= i; j++)
//                printf("* ");
//            printf("\n");
//        }
//    }
//    return 0;
//}

//int main()
//{
//	int ch;
//	while ((ch = getchar()) != EOF)
//		putchar(ch);
//	return 0;
//}

//int main()
//{
//	int i = 1;
//	for (; i <= 10;)
//	{
//		printf("%d ", i);
//		i++;
//	}
//	return 0;
//}

//int main()
//{
//	int i, n;
//	int sum = 1;
//
//	scanf("%d", &n);
//
//	for (i = 1; i <= n; i++)
//	{
//		sum *= i;
//	}
//
//	printf("%d\n", sum);
//	return 0;
//}

//int main()
//{
//	int i;
//	int sums = 0;
//	int sum = 1;
//
//	for (i = 1; i <= 10; i++)
//	{
//		sum *= i;
//		sums += sum;
//	}
//
//	printf("%d\n", sums);
//	return 0;
//}

int main()
{
	int arr[10] = { 1,2,3,4,5,6,7,8,9,0 };
	int key = 0;
	int sz = sizeof(arr) / sizeof(arr[0]);
	int left = 0;
	int right = sz - 1;

	scanf("%d", &key);

	int mid;
	
	for (int i = 0; i < sz/2; i++)
	{
		mid = left + (right - left) / 2;
		if (key > arr[mid])
			left = arr[mid];
		else if (key < arr[mid])
			right = arr[mid];

	}

	if (key == arr[mid])
		printf("找到了，下标为[%d]\n", mid);
	else
		printf("没找到\n");

	return 0;
}