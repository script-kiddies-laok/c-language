#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>

//int main()
//{
//	FILE* pf = fopen("test.txt", "w");			//打开文件，不写路径就是当前目录
//	if (pf == NULL)								//w表示写，文件不存在就创建一个新的文件，存在就把它的内容销毁
//	{
//		perror("fopen");
//		return 1;
//	}
//
//	fclose(pf);									//关闭文件
//	pf = NULL;									//释放指针
//	return 0;
//}


//int main()
//{
//	int n = 0;
//	FILE* pf = fopen("test.txt", "r");
//	if (pf == NULL)
//	{
//		perror("fopen");
//		return 1;
//	}
//
//	n = fgetc(pf);				//接受字符
//	printf("%d\n", n);
//
//	n = fgetc(pf);
//	printf("%d\n", n);
//
//	n = fgetc(pf);
//	printf("%d\n", n);
//
//	n = fgetc(pf);
//	printf("%d\n", n);
//
//	n = fgetc(pf);
//	printf("%d\n", n);
//
//	fclose(pf);
//	pf = NULL;
//
//	return 0;
//}


//int main()
//{
//	FILE* fp = fopen("test.txt", "w");
//	if (fp == NULL)
//	{
//		perror("fopen");
//		return 1;
//	}
//
//	char ch = 'a';
//	fputc(ch, fp);				//把ch的内容写入到fp指定的文件中
//
//	fclose(fp);
//	fp = NULL;
//
//	return 0;
//}


//int main()
//{
//	FILE* fp = fopen("test.txt", "r");
//	if (fp == NULL)
//	{
//		perror("fopen");
//		return 1;
//	}
//
//	char str[100] = { 0 };
//	fgets(str, 5, fp);				//读到的字符串要把'\0'也算里，实际上得到的是n-1个字符
//
//	printf("%s\n", str);
//
//	fclose(fp);
//	fp = NULL;
//
//	return 0;
//}


//int main()
//{
//	FILE* fp = fopen("test.txt", "w");
//	if (fp == NULL)
//	{
//		perror("fopen");
//		return 1;
//	}
//
//	fputs("hello world\n", fp);				//默认是不换行的
//	fputs("haha\n", fp);
//
//	fclose(fp);
//	fp = NULL;
//	return 0;
//}


//int main()
//{
//	FILE* fp = fopen("test1.txt", "r");
//	if (fp == NULL)
//	{
//		perror("fopen");
//		return 1;
//	}
//
//	FILE* fpp = fopen("test2.txt", "w");
//	if (fpp == NULL)
//	{
//		perror("fopen tw");
//		return 1;
//	}
//
//	int ch = 0;
//	while ((ch = fgetc(fp))!=EOF)
//	{
//		fputc(ch, fpp);
//	}
//
//	fclose(fp);
//	fp = NULL;
//	fclose(fpp);
//	fpp = NULL;
//
//	return 0;
//}
//

//
//int main()
//{
//	struct Stu s = { 0 };
//	FILE* fp = fopen("test.txt", "r");
//	if (fp == NULL)
//	{
//		perror("fopen");
//		return 1;
//	}
//
//
//	fscanf(fp, "%s %d %s", s.name, &(s.age), s.sex);
//
//	printf("%s %d %s\n", s.name, s.age, s.sex);
//
//	fclose(fp);
//	fp = NULL;
//
//	return 0;
//}
//

//struct Stu
//{
//	char name[20];
//	int age;
//	char sex[5];
//};
//
//int main()
//{
//	struct Stu s = { "zhangsan",18,"man" };
//	FILE* fp = fopen("test.txt", "w");
//	if (fp == NULL)
//	{
//		perror("fopen");
//		return 1;
//	}
//
//	fprintf(fp, "%s %d %s", s.name, s.age, s.sex);
//
//	fclose(fp);
//	fp = NULL;
//
//	return 0;
//}

//struct Stu
//{
//	char name[20];
//	int age;
//	char sex[5];
//};
//
//int main()
//{
//	struct Stu s = { "zhangsan",18,"man" };
//
//	char str[100] = { 0 };
//	sprintf(str, "%s%d%s", s.name, s.age, s.sex);
//
//	printf("%s\n", str);
//	return 0;
//}



struct Stu
{
	char name[20];
	int age;
	char sex[5];
};

int main()
{
	struct Stu s = { "zhangsan",18,"man" };
	struct Stu tmp = { 0 };
	char str[100] = { 0 };
	sprintf(str, "%s%d%s", s.name, s.age, s.sex);

	printf("%s\n", str);
	return 0;
}