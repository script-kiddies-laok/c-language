#define _CRT_SECURE_NO_WARNINGS 1

//#include <stdio.h>

//int main()
//{
//	printf("%d\n", sizeof(char));
//	printf("%d\n", sizeof(short));
//	printf("%d\n", sizeof(int));
//	printf("%d\n", sizeof(long));
//	printf("%d\n", sizeof(long long));
//	printf("%d\n", sizeof(float));
//	printf("%d\n", sizeof(double));
//	printf("%d\n", sizeof(long double));
//
//	return 0;
//}


//#include <stdio.h>
//
//int main()
//{
//	//printf("%c\n", 'a');
//	//printf("%d\n", 111);
//	//printf("%d\n", 124);
//	//printf("%c\n", 'a');
//	//printf("%c\n", 'a');
//	//printf("%c\n", 'a');
//	//printf("%c\n", 'a');
//	//printf("%c\n", 'a');
//
//	printf("%o\n", 11);
//
//	return 0;
//}


//#include <stdio.h>
//
//#define MAX 10					//#define定义的标识符常量MAX
//
//enum Sex
//{
//	MALE,						//括号中的MALE,FEMALE是枚举常量
//	FEMALE
//};
//
//int main()
//{
//	3.14;						//这种是字面常量
//	7;
//	//int MAX = 20;				//#define定义的标识符常量不能重新定义，不能在其他地方更改
//	const int a = 10;
//	//a = 20;					//被const修饰的变量不能被修改，会报错，该变量具有常变量属性
//	printf("a = %d\n", a);
//	printf("MAX = %d\n", MAX);
//
//	return 0;
//}


//#include <stdio.h>
//
//int a = 10;					//变量a为全局变量
//
//int main()
//{
//	int b = 15;				//变量b为局部变量
//
//	printf("a = %d\n", a);	
//
//	int a = 20;
//	printf("a = %d\n", a);
//	return 0;
//}

//#include <stdio.h>
//
//int main()
//{
//	int a = 0;
//	scanf("%d", &a);
//	printf("a = %d\n", a);
//	return 0;
//}

#include <stdio.h>
#include <string.h>

//int main()
//{
//	printf("%d\n", strlen("c:\code\test.c"));
//	return 0;
//}

//int main()
//{
//	printf("%c\n", '\'');
//	printf("%s\n", "\"");
//	return 0;
//}
//int main()
//{
//	printf("c:\code\test.c");
//	return 0;
//}

//int main()
//{
//	char ch1[] = "abcdef";
//	char ch2[] = { 'a','b','c','d','e','f' };
//	char ch3[] = { 'a','b','c','d','e','f',0 };
//	printf("ch1 = %s\n", ch1);
//	printf("ch2 = %s\n", ch2);
//	printf("ch3 = %s\n", ch3);
//	return 0;
//}

//#include <stdio.h>
//
//int main()
//{
//	printf("%c\n", 'a');
//	printf("%s\n", "hello world");
//
//	printf("%c\n", '\'');
//	printf("%s\n", "\"");
//	return 0;
//}


//#include <stdio.h>
//
//int main()
//{
//	printf("%c\n", '\111');
//	printf("%c\n", '\x55');
//	return 0;
//}


//#include <stdio.h>
//
//int main()
//{
//	printf("%d\n", strlen("abcdef"));
//	// \32被解析成一个转义字符
//	printf("%d\n", strlen("c:\test\328\test.c"));
//	return 0;
//}


//#include <stdio.h>
//
//int Add(int x, int y)
//{
//	return x + y;
//}
//
//int main()
//{
//	int a = 10;
//	int b = 20;
//
//	/*
//		这是C语言注释风格
//		这种注释不要套用	
//	*/
//
//	//
//	//	这是C++注释风格
//	//
//	printf("a + b = %d\n", Add(a, b));
//	return 0;
//}


//#include <stdio.h>
//
//int main()
//{
//	int happy = 0;
//	printf("你也想要快乐吗(1 or 0):>");
//	scanf("%d", &happy);
//
//	if (happy == 1)
//	{
//		printf("你的快乐来了\n");
//	}
//	else
//	{
//		printf("没关系，你的快乐需要你自己去寻找\n");
//	}
//	return 0;
//}


//#include <stdio.h>
//
//int main()
//{
//	int i = 0;
//	while (i < 10)
//	{
//		printf("haha\n");
//		i++;
//	}
//	return 0;
//}


//#include <stdio.h>
//
//int Sub(int x, int y)
//{
//	return x - y;
//}
//
//int main()
//{
//	int a = 20;
//	int b = 10;
//	printf("%d\n", Sub(a, b));
//	return 0;
//}


//#include <stdio.h>
//
//int main()
//{
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		printf("arr[%d] = %d\n",i, arr[i]);
//	}
//	return 0;
//}

//#include <stdio.h>
//
//int main()
//{
//	int a = -1;
//	printf("%d\n", a << 2);
//	return 0;
//}

//
//#include <stdio.h>
//
//typedef unsigned int uint_32;
//
//int main()
//{
//	typedef int int_11;
//	int_11 a = 10;
//	uint_32 b = 20;
//	printf("%d\n", a);
//	printf("%u\n", b);
//	return 0;
//}


//#include <stdio.h>
//
//void test()
//{
//	static int a = 1;
//	a++;
//	printf("%d\n", a);
//}
//
//int main()
//{
//	int i = 0;
//	while (i < 10)
//	{
//		test();
//		i++;
//	}
//	return 0;
//}


//#include <stdio.h>
//
//extern int Add(int x,int y);
//
//int main()
//{
//	int a = 10;
//	int b = 20;
//	printf("%d\n", Add(a, b));
//	return 0;
//}



//#include <stdio.h>
//
//#define MAX 10
//
//
//int main()
//{
//	printf("%d\n", MAX);
//	return 0;
//}

//#include <stdio.h>
//
//#define ADD(x,y) (x+y)
//
//int main()
//{
//	int a = 10;
//	int b = 20;
//	printf("%d\n", ADD(a, b));
//	return 0;
//}


//#include <stdio.h>
//
//struct Stu
//{
//	char name[20];
//	int age;
//	char sex[10];
//};
//
//int main()
//{
//	
//	struct Stu s = { "zhangsan",18,"man"};
//	printf("%d\n", s.age);
//	printf("%s\n", s.name);
//
//	struct Stu* p = &s;
//	printf("%d\n", p->age);
//	return 0;
//}

//
//#include <stdio.h>
//
//int max_of(int x, int y)
//{
//	return x > y ? x : y;
//}
//
//int main()
//{
//	
//	int a, b;
//	scanf("%d%d", &a, &b);
//	printf("%d\n", max_of(a, b));
//	return 0;
//}



#include <stdio.h>
#include <math.h>

int main()
{
    double a, b, c;
    scanf("%lf %lf %lf", &a, &b, &c);
    if ((a + b > c) || (b + c > a) || (a + c > b))
    {
        double ret = (a + b + c) / 2.0;
        printf("circumference=%.2lf area=%.2lf", (a + b + c) ,sqrt(ret * (ret - a) * (ret - b) * (ret - c)));
    }
    return 0;
}









//int main()
//{
//	printf("%d\n", sizeof(char*));
//	printf("%d\n", sizeof(short*));
//	printf("%d\n", sizeof(int*));
//	printf("%d\n", sizeof(double*));
//
//	return 0;
//}