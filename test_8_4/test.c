#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>

#include <string.h>

//int main()
//{
//	int arr[30][30] = { 0 };
//	int line = 0;
//	scanf("%d", &line);
//	int i = 0;
//	//对外侧初始化
//	for (i = 1; i <= line; i++)
//	{
//		arr[i][1] = arr[i][i] = 1;
//	}
//
//	//对内部赋值
//	for (i = 2; i <= line; i++)			//这里要从第二行开始
//	{
//		int j = 0;
//		for (j = 2; j <= i; j++)
//		{
//			arr[i][j] = arr[i - 1][j] + arr[i - 1][j - 1];
//		}
//	}
//
//	//打印
//	for (i = 1; i <= line; i++)
//	{
//		int j = 0;
//		//打印空格
//		for (j = 1; j <= line - i; j++)
//		{
//			printf(" ");
//		}
//		//打印数值
//		for (j = 1; j <= i; j++)
//		{
//			printf("%d ", arr[i][j]);
//		}
//
//		printf("\n");
//	}
//	return 0;
//}

//
//int main()
//{
//	int murder[4] = { 0 };
//	int i = 0;
//	for (i = 0; i < 4; i++)
//	{
//		murder[i] = 1;
//		if ((murder[0] != 1) +
//			(murder[2] == 1) +
//			(murder[3] == 1) +
//			(murder[3] != 1) == 3)
//		{
//			break;
//		}
//		murder[i] = 0;
//	}
//	putchar('A' + i);
//	return 0;
//}


//int main()
//{
//	int murder[4] = { 0 };
//	int i = 0;
//	for (i = 0; i < 4; i++)
//	{
//		//假设一名凶手
//		murder[i] = 1;
//		if ((murder[0] != 1) +
//			(murder[2] == 1) +
//			(murder[3] == 1) +
//			(murder[3] != 1) == 3)
//		{
//			break;		//找到了就退出
//		}
//		//没找到
//		murder[i] = 0;
//	}
//	//打印A,B,C,D
//	putchar('A' + i);
//	return 0;
//}


//int CheckData(int* p)
//{
//	int tmp[6] = { 0 };		//用来接收数组p的值
//	int i = 0;
//	for (i = 0; i < 5; i++)
//	{
//		if (tmp[p[i]])
//		{
//			return 0;
//		}					//如果有重复的就返回0，例如p[0]=1,p[1]=1
//		tmp[p[i]] = 1;		//tmp[p[0]]=tmp[1]=0,对tmp[p[0]]重新赋值1
//							//tmp[1]=1
//							//tmp[p[1]]=tmp[1]=1,符合条件，进入if返回0
//	}
//	return 1;
//}
//int main()
//{
//	int p[5] = { 0 };
//	int i = 0;
//	for (p[0] = 1; p[0] <= 5; p[0]++)
//		for (p[1] = 1; p[1] <= 5; p[1]++)
//			for (p[2] = 1; p[2] <= 5; p[2]++)
//				for (p[3] = 1; p[3] <= 5; p[3]++)
//					for (p[4] = 1; p[4] <= 5; p[4]++)
//					{
//						if ((p[1] == 2) + (p[0] == 3) == 1 &&
//							(p[1] == 2) + (p[4] == 4) == 1 &&
//							(p[2] == 1) + (p[3] == 2) == 1 &&
//							(p[2] == 5) + (p[3] == 3) == 1 &&
//							(p[4] == 4) + (p[0] == 1) == 1 && CheckData(p))	//Checkdata检查是否重复
//						{
//							for (i = 0; i < 5; i++)
//							{
//								printf("%d ", p[i]);
//							}
//						}
//					}
//}
//
//typedef struct student
//{
//	char name[20];
//	int age;
//	char sex[5];
//}stu;
//
//int main()
//{
//	stu s = { "zhangsan",18,"man" };
//	printf("%d\n", s.age);
//	return 0;
//}
//
//int main()
//{
//	int a = 10;
//	int const* p = &a;		//const在*p前面，可以修改p指向的值，不可修改*p
//	int b = 20;				//const在p前面，不可以修改p指向的值，可以修改*p
//	//*p = 11;				//不可以修改
//	p = &b;
//
//	int* const pa = &b;	
//	*pa = 10;				//可以修改
//	//pa = &a;				//不可以修改
//
//	return 0;
//}
//
//void move_arr(int arr[], int sz)
//{
//	int left = 0;
//	int right = sz - 1;
//	while (left < right)
//	{
//		while ((left < right) && arr[left] %2 == 1)	//不加这个判断条件会产生越界访问
//		{
//			left++;
//		}
//		while ((left < right) && arr[right] % 2 == 0)
//		{
//			right--;
//		}
//		if (left < right)
//		{
//			int tmp = arr[right];
//			arr[right] = arr[left];
//			arr[left] = tmp;
//		}
//	}
//}
//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	move_arr(arr,sz);
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	return 0;
//}

//int main()
//{
//	char* p = "abcdef";				//把常量字符串的起始地址放在变量p里面
//	printf("%s\n", p);				//常量字符串不可修改
//	return 0;
//}


//int main()
//{
//	char arr1[] = "abcdef";
//	char arr2[] = "abcdef";
//	char* p1 = "abcdef";
//	char* p2 = "abcdef";
//
//	if (arr1 == arr2)
//	{
//		printf("arr1==arr2\n");
//	}
//	else
//	{
//		printf("arr1!=arr2\n");
//	}
//
//	if (p1 == p2)
//	{
//		printf("p1==p2\n");
//	}
//	else
//	{
//		printf("p1!=p2\n");
//	}
//	return 0;
//}


//int main()
//{
//	char* p[10] = { "hello","world","bit"};
//	printf("%s\n", *(p + 1));
//	return 0;
//}

//int main()
//{
//	char* p[3] = { "hello","world" };
//	printf("%s\n", *(p + 0));
//	return 0;
//}


//int main()
//{
//	int a = 10;
//	int b = 20;
//	int c = 30;
//	int* p[3] = { &a,&b,&c };
//	int i = 0;
//	for (i = 0; i < 3; i++)
//	{
//		printf("%d ", *p[i]);
//	}
//	return 0;
//}

//int main()
//{
//	int arr1[] = { 1,2,3 };
//	int arr2[] = { 2,3,4 };
//	int arr3[] = { 3,4,5 };
//	int* p[3] = { arr1,arr2,arr3 };
//
//	int i = 0;
//	for (i = 0; i < 3; i++)
//	{
//		int j = 0;
//		for (j = 0; j < 3; j++)
//		{
//			printf("%d ", p[i][j]);
//		}
//		printf("\n");
//	}
//	return 0;
//}


//int main()
//{
//	int arr[4] = { 1,2,3,4 };
//	int(*p)[4] = &arr;			//p就是数组指针，该指针指向了一个数组，数组有10个元素，每个元素是int类型	
//	int i = 0;
//	printf("%d\n", *(*p));
//	/*for (i = 0; i < 4; i++)
//	{
//		printf("%d ", (*p)[i]);
//	}*/
//	return 0;
//}

//
//int main()
//{
//	int arr[10][10] = { {1,5,7,9},{2,6,8,10},{3,11,13,15},{4,12,14,16} };
//	int number = 0;
//	int row = 3;		//列
//	int col = 0;		//行
//	
//	scanf("%d", &number);
//	while (1)
//	{
//		if (number > arr[col][row])
//		{
//			col++;
//		}
//		if (number < arr[col][row])
//		{
//			row--;
//		}
//		if (number == arr[col][row])
//		{
//			break;
//		}
//	}
//
//	printf("坐标为(%d,%d)", col + 1, row + 1);
//	return 0;
//}

//void rotate(char arr[],int len,int k)
//{
//	int i = 0;
//	int right = len - k;
//	while (i < k)
//	{
//		arr[right++] = arr[i++];
//	}
//	while (k <= len)
//	{
//		arr[k] = arr[k + 1];
//		k++;
//	}
//	
//}
//
//int main()
//{
//	char arr[40] = { 0 };
//	int k = 0;
//	gets(arr);
//	printf("请输入要逆转几个字符:");
//	scanf("%d", &k);
//	int len = strlen(arr);
//	//rotate(arr,len,k);
//
//
//	int i = 0;
//	int right = len - k;
//	while (i < k)
//	{
//		arr[right++] = arr[i++];
//	}
//	while (k <= len)
//	{
//		arr[k] = arr[k+1];
//		k++;
//	}
//
//	printf("%s\n", arr);
//
//	return 0;
//}


//int main()
//{
//	char arr[] = "ABCD";
//	int k = 3;
//	int len = strlen(arr);
//	k %= len;							//对输入的k截取
//	for (int i = 0; i < k; i++)			//循环一次左旋一个字符
//	{
//		int tmp = arr[0];				//把第一个字符存起来
//		int j = 0;
//		for (j = 0; j < len - 1; j++)	//数组整体向左平移一个单位
//		{
//			arr[j] = arr[j + 1];
//		}
//		arr[j] = tmp;					//把存起来的字符放到最后一位
//	}
//	printf("%s\n", arr);
//	return 0;
//}


//int main()
//{
//	char arr[] = "ABCDEF";
//	int k = 0;
//	int len = strlen(arr);
//	scanf("%d", &k);
//	k %= len;
//
//	int i = 0;
//	int j = 0;
//	for (i = 0; i < k; i++)
//	{
//		int tmp = arr[0];
//		for (j = 0; j < len - 1; j++)
//		{
//			arr[j] = arr[j + 1];
//		}
//		arr[j] = tmp;
//	}
//
//	printf("%s\n", arr);
//	return 0;
//}

//#include <stdlib.h>

//int main()
//{
//	char arr[40] = { 0 };
//	gets_s(arr,5);
//	printf("%s\n", arr);
//	return 0;
//}

//int main()
//{
//	int arr1[] = { 1,2,3 };
//	int arr2[] = { 2,3,4 };
//	int arr3[] = { 3,4,5 };
//
//	int* p[3] = { arr1,arr2,arr3 };
//
//	int i = 0;
//	for (i = 0; i < 3; i++)
//	{
//		int j = 0;
//		for (j = 0; j < 3; j++)
//		{
//			printf("%d ", p[i][j]);
//		}
//		printf("\n");
//	}
//	return 0;
//}

//void print(int(*p)[5], int x, int y)
//{
//	int i = 0;
//	for (i = 0; i < x; i++)
//	{
//		int j = 0;
//		for(j=0;j<y;j++)
//		{
//			//printf("%d ", (*p + i)[j]);
//			printf("%d ",(*(p+i))[j]);
//		}
//		printf("\n");
//	}
//}
//
//int main()
//{
//	int arr[3][5] = { {1,2,3,4,5},{2,3,4,5,6},{3,4,5,6,7} };
//	print(arr, 3, 5);
//	return 0;
//}

int main()
{
	int arr[] = { 10,20 };
	int(*p)[2] = &arr;
	printf("%d\n", *(*p));				//数组指针是一个指针，用来存放数组地址的指针，第一次解引用是找到数组
	printf("%d\n", *p[0]);				//首元素的地址，在解引用是找到该地址存放的值
	int a = 10;							//*p就相当于数组名
	int b = 20;
	int* pi[2] = { &a,&b };
	printf("%d\n", *(*pi));				//指针数组是一个数组，解引用一次表示找到这个数组的首元素地址
										//在解引用找到这个地址存放的值
	int arr2[] = { 1,2,3 };
	printf("%d\n", *arr2);
	return 0;
}