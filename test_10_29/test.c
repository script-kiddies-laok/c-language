#define _CRT_SECURE_NO_WARNINGS 1

#include "SListNode.h"

//void test()
//{
//	SListNode* s = NULL;
//	SListPushBack(&s, 1);
//	SListPushBack(&s, 2);
//	//SListPushBack(&s, 4);
//	//SListPushBack(&s, 5);
//	//SListPopBack(&s);
//	//SListPushFront(&s, 11);
//	//SListPopFront(&s);
//	SListNode* find = SListFind(s, 1);
//	//SListInsertAfter(find, 15);
//	//SListPrint(s);
//	SListEraseAfter(find);
//	SListPrint(s);
//}
//
//int main()
//{
//	//test();
//	//int s = 0.12f;					//实际的类型取决于变量的类型
//	//printf("%f\n", s);		
//	//printf("%f\n", 0.123f);
//	//float af;
//	//af = 1234567898.1234;				//有效数字为7位
//	//af = 12.123456789123456789123456;	//有效数字为7位
//	//af = 123465789.123456789123456789;
//	//double aff;
//	//aff = 12.123456789123456789123456;
//	//printf("%.20f\n", aff);
//	//printf("%.20f\n", af);
//
//
//	char s = '!';
//	return 0;
//}

//
//void Swap(int* a, int* b)
//{
//    int ret = *a;
//    *a = *b;
//    *b = ret;
//}
//
//
//void RotateStr(int* nums, int start, int end)
//{
//    while (start < end)
//    {
//        Swap(&nums[start], &nums[end]);
//        start++;
//        end--;
//    }
//}

//void rotate(int* nums, int numsSize, int k) {
//
//    k %= numsSize;
//    //整体旋转
//    RotateStr(nums, 0, numsSize - 1);
//    //旋转前3个
//    RotateStr(nums, 0, k - 1);
//    //旋转后4个
//    RotateStr(nums, k, numsSize - 1);
//}
//
////int main()
////{
////    int nums[] = { 1,2,3,4,5,6,7 };
////    rotate(nums, 7, 3);
////    return 0;
////}
//
//int* addToArrayForm(int* num, int numSize, int k)
//{
//    int kSize = 0, ki = k;
//    while (ki)
//    {
//        kSize++;
//        ki /= 10;
//    }
//    int len = numSize > kSize ? numSize + 1 : kSize + 1;
//    int* arr = (int*)malloc(sizeof(int) * len);
//    int i = numSize - 1;
//    int sum = 0;
//    while (i >= 0 || k != 0)
//    {
//        sum = num[i] + k % 10;
//        k /= 10;
//        if (sum >= 10)
//        {
//            k++;
//            *arr = (sum - 10);
//            arr++;
//        }
//        else
//        {
//            *arr = sum;
//            arr++;
//
//        }
//        i--;
//    }
//    return arr;
//}

SListNode* insertionSortList(SListNode* head) {
    if (head == NULL || head->next == NULL)
        return head;
    SListNode* softHead = head, * cur = head->next;
    softHead->next = NULL;    //只取一个数据,当做已经排好序的

    while (cur)
    {
        SListNode* next = cur->next;
        SListNode* p = NULL, * c = softHead;

        while (c)
        {
            if (cur->data < c->data)
            {
                break;
            }
            else
            {
                p = c;
                c = c->next;
            }
        }

        if (p == NULL)
        {
            cur->next = c;
            softHead = cur;
        }
        else
        {
            p->next = cur;
            cur->next = c;
        }

        cur = next;
    }
    return softHead;
}

void test()
{
    SListNode* s = NULL;
    SListPushBack(&s, 4);
    SListPushBack(&s, 2);
    SListPushBack(&s, 1);
    SListPushBack(&s, 3);
    //SListPrint(s);
    SListNode* test = insertionSortList(s);
    SListPrint(test);
}

int main()
{
    //int num[] = { 1,2,0,0 };
    //int k = 34;
    //int* arr = addToArrayForm(num, 4, k);

    test();
    return 0;
}