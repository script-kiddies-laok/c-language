#define _CRT_SECURE_NO_WARNINGS 1

#include "SListNode.h"


void SListPrint(SListNode* s)
{
	//assert(s);
	SListNode* cur = s;
	while (cur != NULL)
	{
		printf("%d->", cur->data);
		cur = cur->next;
	}
	printf("NULL\n");
}

SListNode* BuyListNode(SLDataType x)
{
	SListNode* new = (SListNode*)malloc(sizeof(SListNode));
	if (new == NULL)
	{
		perror("malloc fail");
		exit(-1);
	}

	new->data = x;
	new->next = NULL;
	return new;
}

void SListPushBack(SListNode** s, SLDataType x)
{
	SListNode* new = BuyListNode(x);

	if (*s == NULL)
	{
		*s = new;
	}
	else
	{
		SListNode* tail = *s;
		while (tail->next!=NULL)
		{
			tail = tail->next;
		}
		tail->next = new;
	}
}

void SListPopBack(SListNode** s)
{
	//没有节点
	if (*s == NULL)
	{
		exit(-1);
	}
	//只有一个节点
	else if ((*s)->next == NULL)
	{
		free(*s);
		*s = NULL;
	}
	//多个节点
	else
	{
		SListNode* tail = *s;
		SListNode* prev = NULL;
		while (tail->next != NULL)
		{
			prev = tail;
			tail = tail->next;
		}

		free(tail);
		tail = NULL;

		prev->next = NULL;
	}
}

void SListPushFront(SListNode** s, SLDataType x)
{
	SListNode* NewNode = BuyListNode(x);
	if (*s == NULL)
	{
		*s = NewNode;
	}
	else
	{
		NewNode->next = *s;
		*s = NewNode;
	}
}

void SListPopFront(SListNode** s)
{
	//没有节点
	if (*s == NULL)
	{
		return;
	}
	//只有一个节点
	else if ((*s)->next == NULL)
	{
		free(*s);
		*s = NULL;
	}
	//多个节点
	else
	{
		SListNode* next = (*s)->next;
		free(*s);
		*s = next;
	}
}

SListNode* SListFind(SListNode* s, SLDataType x)
{
	SListNode* cur = s;
	while (cur != NULL)
	{
		//怎么找到这个节点
		if (cur->data == x)
		{
			return cur;
		}
		else
		{
			cur = cur->next;
		}
	}
	return NULL;
}

void SListInsertAfter(SListNode* pos, SLDataType x)
{
	SListNode* NewNode = BuyListNode(x);
	//pos下一个为空
	if (pos->next == NULL)
	{
		pos->next = NewNode;
	}
	else
	{
		SListNode* next = pos->next;
		pos->next = NewNode;
		NewNode->next = next;
	}
}

void SListEraseAfter(SListNode* pos)
{
	assert(pos);
	if (pos->next == NULL)
	{
		return;
	}
	else
	{
		SListNode* next = pos->next;
		pos->next = next->next;
		free(next);
	}
}