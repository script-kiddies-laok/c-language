#pragma once

#include <stdio.h>
#include <assert.h>
#include <stdlib.h>


typedef int SLDataType;

typedef struct SListNode
{
	struct SListNode* next;
	SLDataType data;
}SListNode;

//打印
void SListPrint(SListNode* s);

//尾插
void SListPushBack(SListNode** s,SLDataType x);

//尾删
void SListPopBack(SListNode** s);

//头插
void SListPushFront(SListNode** s, SLDataType x);

//头删
void SListPopFront(SListNode** s);

//查找
SListNode* SListFind(SListNode* s, SLDataType x);

//在pos位置插入数据
void SListInsertAfter(SListNode* pos, SLDataType x);

//删除pos位置之后的数据
void SListEraseAfter(SListNode* pos);