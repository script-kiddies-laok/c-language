#define _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
using namespace std;


//void Swap(int& a, int& b)
//{
//	int tmp = a;
//	a = b;
//	b = tmp;
//}
//
//int main()
//{
//	int a = 10, b = 20;
//	Swap(a, b);
//	cout << "a = " << a << "\n" << "b = " << b << endl;
//	//这里C++在同时输出多个参数的时候有点麻烦，可以用printf
//	return 0;
//}
//#define Add(x,y) ((x) + (y))		//不要分号
//int main()
//{
//	int* a = NULL;
//	int*& ra = a;
//	cout << Add(1, 2) << endl;
//	return 0;
//}


//int main()
//{
//	int a = 10;
//	auto b = &a;		//把地址传给它
//	auto* c = &a;
//	int& y = a;
//	auto d = y;
//	auto& e = a;
//	cout << typeid(b).name() << endl;
//	cout << typeid(c).name() << endl;
//	cout << typeid(y).name() << endl;
//	cout << typeid(d).name() << endl;
//	cout << typeid(e).name() << endl;
//
//	return 0;
//}

//int main()
//{
//	//范围for
//	int array[] = { 1,2,3,4,5 };
//	for (int i = 0; i < sizeof(array) / sizeof(array[0]); i++)
//		cout << array[i] << " ";
//	cout << endl;
//
//
//	for (auto& e : array)
//	{
//		e *= 2;
//	}
//
//	//自动遍历
//	for (auto h : array)
//		cout << h << " ";
//	cout << endl;
//	return 0;
//}
//
//void f(int i)
//{
//	cout << "f(int)" << endl;
//}
//
//void f(int* p)
//{
//	cout << "f(int*)" << endl;
//}
//
//int main()
//{
//	int* p1 = NULL;
//	int* p2 = nullptr;	//在C++中用这个	
//
//	f(0);
//	f(NULL);		//在C++中NULL被宏定义成0
//	f(nullptr);
//	return 0;
//}

//int& Add(int a, int b)
//{
//	int c = a + b;
//	return c;
//}
////实际中，函数在调用结束后，返回变量就不存在了，所以不能用引用返回
//int main()
//{
//	int& ret = Add(1, 2);		//返回的是c的临时变量的引用
//	Add(2, 4);					//ret接受的是c的临时变量，临时变量改变ret也就改变了
//	cout << ret << endl;		//但是这里的ret是越界的，函数调用结束，c销毁了，ret仍然访问
//	return 0;					//如果在Add(2,4)下面在随便调用一个在栈中创建销毁的函数
//}								//那么ret的值就会变成随机值

//
//int main()
//{
//	int a = 0;
//	int& ra = a;
//	int* pa = &a;
//	//int& rr = NULL;
//	return 0;
//}

//inline int Add(int a, int b)
//{
//	return a + b;
//}
//
//int main()
//{
//	int ret = Add(1, 2);
//	cout << ret << endl;
//	return 0;
//}

//int main()
//{
//	int a = 10;								
//	auto b = a;								//编译器会自动推导出b的类型						
//	cout << typeid(b).name() << endl;		//先记住怎么用就可以，输出变量的类型
//
//	return 0;
//}

//int main()
//{
//	int a = 10;
//	auto& ra = a;			//使用auto声明引用类型时必须加上&
//	auto pa = &a;
//	auto* ppa = &a;			//使用auto声明指针类型时加不加*都可以
//	cout << typeid(ra).name() << endl;
//	cout << typeid(pa).name() << endl;
//	cout << typeid(ppa).name() << endl;
//	return 0;
//}

//int main()
//{
//	int a = 10;
//	double b = 3.14;
//	auto c = a, d = b;		//使用auto声明多个变量必须是类型一样的
//	return 0;
//}
//
//int main()
//{
//	int arr[] = { 1,2,3,4,5 };
//	for (auto e : arr)			//依次遍历，把值赋给e，直到结束
//		cout << e << " ";
//	cout << endl;
//
//	for (auto& e : arr)			//不加&只改变e的值不改变arr里面的值
//		e *= 2;
//
//	for (auto a : arr)			//auto后面的变量是可以随便给的
//		cout << a << " ";
//	cout << endl;
//
//	return 0;
//}

//
//void f(int i)
//{
//	cout << "f(int)" << endl;
//}
//
//void f(int* p)
//{
//	cout << "f(int*)" << endl;
//}
//
//int main()
//{
//	f(0);
//	f(NULL);
//	f(nullptr);
//	return 0;
//}

//int main()
//{
//	long a = 10;
//	cout << sizeof(a) << endl;
//	return 0;
//}

class Stack
{
public:
	void StackInit()
	{
		;
	}
	void StackPush()
	{
		;
	}
public:
	int a = 0;
	int capacity = 0;
};

int main()
{
	Stack st;
	st.StackInit();
	return 0;
}