#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>

//void menu()
//{
//	printf("****1.Add 2.Sub****\n");
//	printf("****3.Mul 4.Div****\n");
//	printf("****   0.exit  ****\n");
//
//}
//
//int Add(int x, int y)
//{
//	return x + y;
//}
//
//int Sub(int x, int y)
//{
//	return x - y;
//}
//
//int Mul(int x, int y)
//{
//	return x * y;
//}
//
//int Div(int x, int y)
//{
//	return x / y;
//}
//
//void cale(int(*pf)(int, int))
//{
//	int x = 0;
//	int y = 0;
//	int ret = 0;
//	
//	printf("请输入两个数:");
//	scanf("%d %d", &x, &y);
//	ret = pf(x, y);					//通过调用这个地址调用指向的函数
//	printf("%d\n", ret);
//}
//
//int main()
//{
//	int input = 0;
//	do
//	{
//		menu();
//		printf("请选择:");
//		scanf("%d", &input);
//		switch (input)
//		{
//		case 1:
//			cale(Add);				//把地址传个cale函数
//			break;
//		case 2:
//			cale(Sub);
//			break;
//		case 3:
//			cale(Mul);
//			break;
//		case 4:
//			cale(Div);
//			break;
//		case 0:
//			printf("退出计算器!\n");
//			break;
//		default:
//			printf("输入错误请重新输入!\n");
//			break;
//		}
//	} while (input);
//	return 0;
//}

//void*的指针是无具体类型的指针
//可以接受任意数据类型的地址
//不能直接进行+-整数的操作
//不能直接解引用操作
//
//void bubble_sort(int arr[], int sz)
//{
//
//}
//int main()
//{
//	int arr[] = { 9,8,7,6,5,4,3,2,1 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	bubble_sort(arr,sz);
//	//float arr2[] = { 2.0,3.0,1.0,0 };
//	return 0;
//}
//#include <stdio.h>
//#include <stdlib.h>
//#include <string.h>
//
//void print(int arr[], int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//}
//
//typedef struct S
//{
//	char name[20];
//	int age;
//}stu;
//
//int int_cmp(const void* e1, const void* e2)
//{
//	return *(int*)e2 - *(int*)e1;				//默认是升序,返回值大于0，表示第一个数大于第二数，进行排序
//}
//
//int float_cmp(const void* e1, const void* e2)
//{
//	return (int)(*(float*)e1 - *(float*)e2);	//要进行强制类型转换，要不会发生警告，这里只要返回值大于0，等于0或者小于0，值的大小多少都没关系
//}
//
//int struct_str_cmp(const void* e1, const void* e2)
//{
//	return strcmp(((stu*)e1)->name, ((stu*)e2)->name);
//}
//
//int struct_int_cmp(const void* e1, const void* e2)
//{
//	return ((stu*)e1)->age - ((stu*)e2)->age;
//}
//
//void test1()
//{
//	int arr[] = { 9,2,7,6,5,4,3,2,1 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	qsort(arr, sz, sizeof(arr[0]), int_cmp);
//	print(arr, sz);
//}
//
//void test2()
//{
//	float arr1[] = { 2.0,3.0,1.0,0 };
//	int sz = sizeof(arr1) / sizeof(arr1[0]);
//	qsort(arr1, sz, sizeof(arr1[0]), float_cmp);
//	for (int i = 0; i < sz; i++)
//	{
//		printf("%.2f ", arr1[i]);
//	}
//}
//
//void test3()
//{
//	stu student[] = { {"zhangsan",18},{"lisi",27},{"wangwu",19}};
//	int sz = sizeof(student) / sizeof(student[0]);
//	qsort(student, sz, sizeof(student[0]), struct_str_cmp);
//	qsort(student, sz, sizeof(student[0]), struct_int_cmp);
//
//}
//
//int main()
//{
//	test1();
//	//test2();
//	//test3();
//	return 0;
//}

//int int_cmp(const void* e1, const void* e2)
//{
//	return *(int*)e1 - *(int*)e2;
//}
//
//
//int float_cmp(const void* e1, const void* e2)
//{
//	return (int)(*(float*)e1 - *(float*)e2);
//}
//
//void _Swap(char* buf1, char* buf2, int width)
//{
//	int i = 0;
//	for (i = 0; i < width; i++)
//	{
//		char tmp = *buf2;
//		*buf2 = *buf1;
//		*buf1 = tmp;
//		buf1++;
//		buf2++;
//	}
//}
//
//void bubble_sort(void* base, int sz, int width, int(*cmp)(const void* e1, const void* e2))
//{
//	int i = 0;
//	int j = 0;
//	for (i = 0; i < sz; i++)
//	{
//		for (j = 0; j < sz - 1 - i; j++)
//		{
//			if (cmp((char*)base + j * width, (char*)base + (j + 1) * width) > 0)
//			{
//				_Swap((char*)base + j * width, (char*)base + (j + 1) * width, width);
//			}
//		}
//	}
//}
//
//int main()
//{
//	int arr[] = { 9,8,7,6,5,4,3,2,1 };
//	float arr2[] = { 2.0,1.0,4.0,0 };
//	int sz = sizeof(arr2) / sizeof(arr2[0]);
//	bubble_sort(arr2, sz, sizeof(arr2[0]), int_cmp);
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%.2f ", arr2[i]);
//	}
//	return 0;
//}


//int main()
//{
//	int arr[] = { 1,2,3,4,5,6 };
//	int* p = arr;
//	printf("%d", *p + 1);
//	return 0;
//}

int main()
{
	//int a[] = { 1,2,3,4 };
	//printf("%d\n", sizeof(a));			//16
	//printf("%d\n", sizeof(a + 0));		//4
	//printf("%d\n", sizeof(*a));			//4
	//printf("%d\n", sizeof(a + 1));		//4
	//printf("%d\n", sizeof(a[1]));			//4
	//printf("%d\n", sizeof(&a));			//4
	//printf("%d\n", sizeof(*&a));			//16
	//printf("%d\n", sizeof(&a + 1));		//4
	//printf("%d\n", sizeof(&a[0]));		//4
	//printf("%d\n", sizeof(&a[0] + 1));	//4
	//数组名是首元素的地址
	//&数组名是整个数组的地址
	//sizeof(数组名)计算的是整个数组的大小
	char arr[] = { 'a','b','c','d','e','f' };
	/*printf("%d\n", sizeof(arr));
	printf("%d\n", sizeof(arr + 0));
	printf("%d\n", sizeof(*arr));
	printf("%d\n", sizeof(arr[1]));
	printf("%d\n", sizeof(&arr));
	printf("%d\n", sizeof(&arr + 1));
	printf("%d\n", sizeof(&arr[0] + 1));*/
	printf("%d\n", strlen(arr));
	printf("%d\n", strlen(arr + 0));
	//printf("%d\n", strlen(*arr));
	//printf("%d\n", strlen(arr[1]));
	printf("%d\n", strlen(&arr));
	printf("%d\n", strlen(&arr + 1));
	printf("%d\n", strlen(&arr[0] + 1));
	return 0;
}