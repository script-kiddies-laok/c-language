#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>
#include <string.h>
#include <assert.h>
//int main()
//{
//	for (int i = 1; i < 100; i++)
//	{
//		printf("%d ", i);
//	}
//	return 0;
//}

//void my_strcpy(char* dest, char* src)
//{
//	while (*src != '\0')
//	{
//		*dest = *src;
//		src++;
//		dest++;
//	}
//	*dest = *src;
//}

//void my_strcpy(char* dest,const char* src)
//{
//	assert(dest != NULL);
//	assert(src != NULL);
//	while (*dest++ = *src++)
//	{
//		;
//	}
//}
//
//int main()
//{
//	char arr1[] = "****************";
//	char arr2[] = "bit";
//	//strcpy(arr1, arr2);
//	my_strcpy(arr1, arr2);
//	printf("%s\n", arr1);
//	return 0;
//}

//int main()
//{
//	const int n = 10;
//	const int* p = &n;
//	//*p = 20;
//	printf("%d\n", n);
//	return 0;
//}

//int main()
//{
//	int i = 0;
//	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
//
//	for (i = 0; i <= 12; i++)
//	{
//		arr[i] = 0;
//		printf("hello bit\n");
//	}
//	return 0;
//}

//int main()
//{
//	printf("%d", 0 % 2);
//	return 0;
//}

//int my_strlen(const char* str)
//{
//	assert(str != NULL);
//	int count = 0;
//	while (*str != '\0')
//	{
//		count++;
//		str++;
//	}
//	return count;
//}
//
//int main()
//{
//	char arr[] = "hello bit";
//	
//	printf("%d\n", my_strlen(arr));
//	return 0;
//}

//char* my_strcpy(char* dest, const char* str)
//{
//	char* arr = dest;
//	assert(dest != NULL && str != NULL);
//	while (*dest++ = *str++)
//		;
//	return arr;
//
//}
//
//int main()
//{
//	char arr1[] = "***********";
//	char arr2[] = "hello";
//
//	printf("%s\n", my_strcpy(arr1, arr2));
//	return 0;
//}

void test(int arr[], int sz)
{
	int left = 0;
	int right = sz - 1;
	while (left <= right)
	{
		while (arr[left] % 2 == 1)
		{
			left++;
		}

		while (arr[right] % 2 == 0)
		{
			right--;
		}
		
		if ((arr[left] % 2 == 0) && (arr[right] % 2 == 1))
		{
			int tmp = arr[left];
			arr[left] = arr[right];
			arr[right] = tmp;
		}

		right--;
		left++;
	}
}

int main()
{
	int arr[11] = { 2,5,3,4,5,6,7,8,9,10,1 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	test(arr,sz);

	for (int i = 0; i < sz; i++)
		printf("%d ", arr[i]);
	return 0;
}