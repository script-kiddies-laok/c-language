#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define ROW 3
#define COL 3

void InitBoard(char board[ROW][COL], int row, int col);
void DisPlayBoard(char board[ROW][COL],int row,int col);

void PlayerBoard(char board[ROW][COL], int row, int col);
void ComputerBoard(char board[ROW][COL], int row, int col);

char IsWin(char board[ROW][COL], int row, int col);