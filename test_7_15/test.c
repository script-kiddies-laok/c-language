#define _CRT_SECURE_NO_WARNINGS 1

//#include <stdio.h>
//
//int main()
//{
//	int arr1[3][3] = { 0 };							//三行三列的数组
//	int arr2[][3] = { {1,2,3},{3,4,5},{5,6,7} };	//三行三列的数组	//行数可以省略
//	int arr3[3][5] = { 1,2,3,4,5,6,7,8,9,10 };		//先把行填满，在进行下一行，剩下的元素，默认是0
//	//int arr4[2][] = { {1,2,3},{4,5,6} };			这种是错误的，不能省略列数
//	return 0;
//}


//#include <stdio.h>
//
//int main()
//{
//	int arr[3][5] = { 0 };
//	int i = 0;
//	for (i = 0; i < 3; i++)
//	{
//		int j = 0;
//		for (j = 0; j < 5; j++)
//		{
//			arr[i][j] = i * j;			//对数组赋值
//		}
//	}
//
//	for (i = 0; i < 3; i++)
//	{
//		int j = 0;
//		for (j = 0; j < 5; j++)
//		{
//			printf("%d ",arr[i][j]);
//		}
//		printf("\n");
//	}
//	return 0;
//}

//#include <stdio.h>
//
//int main()
//{
//	int arr[3][5] = { 0 };
//	int i = 0;
//	for (i = 0; i < 3; i++)
//	{
//		int j = 0;
//		for (j = 0; j < 5; j++)
//		{
//			printf("arr[%d][%d] = %p\n", i, j, &arr[i][j]);
//		}
//	}
//	return 0;
//}


//#include <stdio.h>
//
//int main()
//{
//	int arr[3][4] = { 1,2,3,4,5,6,7,8,9,10 };
//	int* p = &arr[0][0];
//	int i = 0;
//	for (i = 0; i < 12; i++)
//	{
//		printf("%d ", *(p + i));
//	}
//	return 0;
//}


//#include <stdio.h>
//
//void bubble_sort(int arr[], int sz)
//{
//	int i = 0;
//	int flag = 0;
//	for (i = 0; i < sz - 1; i++)
//	{
//		int j = 0;
//		for (j = 0; j < sz - 1 - i; j++)
//		{
//			if (arr[j] < arr[j+1])
//			{
//				int tmp = arr[j];
//				arr[j] = arr[j+1];
//				arr[j+1] = tmp;
//				flag = 1;
//			}
//		}
//		if (flag == 0)
//			break;
//	}	
//}
//
//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//
//	bubble_sort(arr, sz);
//
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	return 0;
//}


//#include <stdio.h>
//
//int main()
//{
//	int arr[10] = { 1,2,3,4,5 };
//	int* p = arr;
//	
//	printf("%p\n", arr);
//	printf("%p\n", &arr[0]);
//	printf("%d\n", *(arr+1));
//	printf("%d\n", *(p + 1));
//	printf("%d\n", p[1]);
//
//	printf("%d\n", sizeof(arr));
//	int* pn = &arr;
//	//arr数组名是首元素的地址
//	//有两个例外
//	//1.sizeof(arr)里的arr表示的是整个数组
//	//2.&arr表示的是整个数组的地址
//	return 0;
//}

//#include <stdlib.h>
//#include <stdio.h>
//#include <time.h>
//#include <string.h>
//
//void menu()
//{
//	printf("################\n");
//	printf("#####猜数字#####\n");
//	printf("################\n");
//
//}
//
//void game()
//{
//	int ret = 0, guest = 0;
//	ret = rand() % 100;
//
//	while (1)
//	{
//		printf("猜数字");
//		printf("%d\n", ret);
//		scanf("%d", &guest);
//		if (ret < guest)
//		{
//			printf("猜大了\n");
//		}
//		else if (ret > guest)
//		{
//			printf("猜小了\n");
//		}
//		else
//		{
//			printf("猜对了\n");
//			break;
//		}
//	}
//}
//
//
//int main()
//{
//	int input;
//	srand((unsigned int)time(NULL));
//	do
//	{
//		menu();
//		printf("请选择1/0>:\n");
//		scanf("%d", &input);
//		switch (input)
//		{
//		case 1:
//			game();
//			break;
//		case 0:
//			printf("退出");
//			break;
//		default:
//			printf("错误");
//			break;
//		}
//	} while (input);
//
//	return 0;
//}

//#include "game.h"
//
//void menu()
//{
//	printf("********************\n");
//	printf("***1.play 0.exit****\n");
//	printf("********************\n");
//
//}
//
//void game()
//{
//	char board[ROW][COL] = { 0 };		//棋盘的大小，3X3
//	char ret = 0;
//
//	InitBoard(board, ROW, COL);			//初始化棋盘
//	DisPlayBoard(board,ROW,COL);		//打印棋盘	5行11列
//	while(1)
//	{
//		printf("玩家下棋:\n");
//		PlayerBoard(board, ROW, COL);	//玩家下棋
//		DisPlayBoard(board, ROW, COL);
//		ret = IsWin(board, ROW, COL);	//判断谁赢
//		if (ret != 'C')					//判断棋盘满没满
//		{
//			break;
//		}
//		printf("\n");
//
//		printf("电脑下棋:\n");
//		ComputerBoard(board, ROW, COL);		//电脑下棋
//		DisPlayBoard(board, ROW, COL);
//		if (ret != 'C')
//		{
//			break;
//		}
//	}
//	if (ret == '#')
//	{
//		printf("玩家赢了\n");
//	}
//	else if (ret == '*')
//		printf("电脑赢了\n");
//	else
//		printf("平局\n");
//}
//
//int main()
//{
//	int input = 0;
//	srand((unsigned)time(NULL));		//给rand设置一个由时间戳随机的种子
//	do {
//		menu();
//		printf("请选择:");
//		scanf("%d", &input);
//		switch (input)
//		{
//		case 1:
//			game();
//			break;
//		case 0:
//			printf("退出游戏\n");
//			break;
//		default:
//			printf("输入错误，请重新输入\n");
//			break;
//		}
//	} while (input);
//
//	return 0;
//}


//#include <stdio.h>
//
//
//
//void init(int arr[], int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		arr[i] = 0;
//	}
//}
//
//void print(int arr[], int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//}
//
//void reserve(int arr[], int sz)
//{
//	int left = 0;
//	int right = sz - 1;
//	while (left <= right)
//	{
//		int tmp = arr[left];
//		arr[left] = arr[right];
//		arr[right] = tmp;
//		left++;
//		right--;
//	}
//}
//
//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8,9,10,11 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	print(arr, sz);
//	putchar('\n');
//
//	reserve(arr, sz);
//	print(arr, sz);
//
//	init(arr, sz);
//	putchar('\n');
//
//	print(arr, sz);
//
//
//	return 0;
//}


//#include <stdio.h>
//
//void exchange(int arr1[], int arr2[],int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		int tmp = arr2[i];
//		arr2[i] = arr1[i];
//		arr1[i] = tmp;
//	}
//}
//
//void print(int arr[],int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//}
//
//int main()
//{
//	int arr1[] = { 1,2,3,4,5 };
//	int arr2[] = { 6,7,8,9,10 };
//	int sz = sizeof(arr1) / sizeof(arr1[0]);
//
//	exchange(arr1, arr2, sz);
//
//	print(arr1, sz);
//
//	putchar('\n');
//	print(arr2, sz);
//	return 0;
//}

//
//#include <stdio.h>
//
//int Fei(int n)
//{
//	int a = 1;
//	int b = 1;
//	int c = 1;
//	for (; n > 2; n--)
//	{
//		c = a + b;
//		a = b;
//		b = c;
//	}
//	return c;
//}
//
//int main()
//{
//	int n = 0;
//	int fei = 0;
//	scanf("%d", &n);
//	fei = Fei(n);
//	printf(" %d ", fei);
//	return 0;
//}

#include <stdio.h>

int main()
{
	
	return 0;
}