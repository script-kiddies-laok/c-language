#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

//int main()
//{
//	int a = 1;
//	char ch = (char)a;
//	printf("%d\n", ch);
//	return 0;
//}


//int test()
//{
//	int i = 1;
//	return *(char*)&i;
//}
//
//int main()
//{
//	int ret = test();
//	printf("%d\n", ret);
//	return 0;
//}

//
//void test(int arr[])
//{
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	printf("%d\n", sz);
//}
//
//int main()
//{
//	int arr[10] = { 0 };
//	test(arr);
//
//	return 0;
//}


//int main()
//{
//	char arr[] = "hello bit";
//	char* pc = arr;
//	printf("%s\n", pc);
//	return 0;
//}

struct Str
{
	char name[20];
	int age;
};

int int_cmp(const char* e1, const char* e2)
{
	return *(int*)e1 - *(int*)e2;
}

int str_cmp(const char* e1, const char* e2)
{
	return strcmp(((struct Str*)e1)->name, ((struct Str*)e2)->name);
}

int str_cmp_int(const char* e1, const char* e2)
{
	return ((struct Str*)e1)->age - ((struct Str*)e2)->age;
}

int fl_cmp(const char* e1, const char* e2)
{
	return ((int)(*(double*)e1 - *(double*)e2));
}

void swap(char* e1, char* e2, int width)
{
	int i = 0;
	for (i = 0; i < width; i++)
	{
		char tmp = *e1;
		*e1 = *e2;
		*e2 = tmp;
		e1++;
		e2++;
	}
}

//void test1()
//
//int main()
//{
//	test1();
//	return 0;
//}









//void bubble_sort(void* base,int sz,int width,int (*cmp)(const void* e1,const void* e2))
//{
//	int i = 0;
//	for (i = 0; i < sz - 1; i++)
//	{
//		int j = 0;
//		for (j = 0; j < sz - 1 - i; j++)
//		{
//			if (cmp((char*)base + j * width, (char*)base + (j + 1) * width) > 0)
//			{
//				swap((char*)base + j * width, (char*)base + (j + 1) * width,width);
//			}
//		}
//	}
//}
//
//void test1()
//{
//	int arr[10] = { 9,8,7,6,5,4,3,2,1,0 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	bubble_sort(arr, sz, sizeof(arr[0]), int_cmp);
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	
//}
//
//
//
//void test2()
//{
//	struct Str stu[3] = { {"zhangsan",68},{"lisi",22},{"wangwu",35} };
//	int sz = sizeof(stu) / sizeof(stu[0]);
//	//bubble_sort(stu, sz, sizeof(stu[0]), str_cmp);
//	bubble_sort(stu, sz, sizeof(stu[0]), str_cmp_int);
//}
//
//void test3()
//{
//	double f[] = { 9.0,8.0,7.0,6.0,5.0 };
//	int sz = sizeof(f) / sizeof(f[0]);
//	bubble_sort(f, sz, sizeof(f[0]), fl_cmp);
//
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%.2lf ", f[i]);
//	}
//	printf("\n");
//	qsort(f, sz, sizeof(f[0]), fl_cmp);
//	for (i = 0; i < sz; i++)
//	{
//		printf("%.2lf ", f[i]);
//	}
//}
//
//int main()
//{
//	//test1();	整型
//	//test2();	结构体
//	test3();	
//	return 0;
//}